<script type="text/javascript" src="js/vSelector.js"></script>
<link rel="stylesheet" type="text/css" href="css/vSelector.css" />
<script type="text/javascript">
$(function() {
	$("#btnFaq").click(function () {
		$("#faqDialog").append($("<iframe width='100%' height='100%' />").attr("src", "faq.html")).dialog({
			width: 800,
			height: 600
		});
	});
});
</script>

<div id="faqDialog" style="margin:0;padding:0;background:#000;"></div>
<div id='vSelector'>
	<span class="subSubPanel" style="cursor:pointer; display:block; text-align:center; font-size:0.7em">
		<span id="currentUser"></span> <a id="aLogout" onclick="logout();">(Log out)</a>
		<span style="cursor:pointer; display:block; text-align:center; font-size:0.9em">
			<script language="Javascript">insertMail("Click here to provide feedback.", "apps","inilex.com?subject=PROTECT Feedback");</script>
		</span>
		<img src="./images/btn_faq.png" id="btnFaq" width="80px" class="actionImage"/>
	</span>
	<span class="subSubPanel" style="cursor:pointer; display:block; text-align:center">
		<img src='images/page_Previous.png' id='vSelPrev' onclick='vehicles.showVehicle(-1);' class='pageNavIcon'/>
		<span style='font-weight: bold;' id='span_vName' onclick='vehicles.getCurrentVehicle().locations.list[0].plot();'></span>
		<img src='images/page_Next.png' id='vSelNext' onclick='vehicles.showVehicle(1);' class='pageNavIcon'/>
		<br/>
		<div id="vehicleInfo"></div>
		<img src="./images/btn_history.png" id="btnHistory" width="80px" onclick="toggleHistoryPanel();" class="actionImage"/>
		<img src="./images/btn_settings.png" id="btnSettings" width="80px" onclick="toggleCtrlPanel();" class="actionImage"/>
		<img src="./images/btn_account.png" id="btnAccount" width="80px" onclick="toggleAcctPanel();" class="actionImage"/>
		<img src="./images/menuIndicator.png" id="menuIndicator" width="80px" class="menuIndicator"/>
	</span>
</div>

<!--#include file="ctrlPanel.inc" -->
<!--#include file="historyPanel.inc" -->
<!--#include file="acctPanel-1.0.1.inc" -->