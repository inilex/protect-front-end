<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript" src="js/creditCard.js"></script>
<script type="text/javascript" src="js/acctPanel.js"></script>
<script type="text/javascript" src="js/timezoneConstants.js"></script>

<link rel="stylesheet" type="text/css" href="css/acctPanel.css" />

<div id="acctPanel" class="ctrlPanel">
	<div id="aSettings" class="subPanel">
		<div id="acctPanelLoading">
				<img src='images/loading.gif' id='loading' style='margin-top:5px; margin-bottom:5px'/>
		</div>
		<div id="bSettings" style="display:none">
			<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="hideAcctPanel();"/>
			<div class="subSubPanel">
				<!--#include file="acctPanelMenu-1.0.1.inc"-->
			</div>
			<div id="acctPromo1"></div>
		</div>
	</div>
	
	<!--#include file="inc/acctPanelBody.inc"-->
	
</div>

<script>
	// TRIP TABLE
	$('#maintIntervalsTable').dataTable({
	  "sScrollY":"200px",
	  "bPaginate":false,
	  "aaSorting":[[0,'asc']],
	  "sDom": '<"#mFilterContainer"<"mfilteroffset"<"clear">f>>lrtip'  
	});

	$("#mFilterContainer").addClass("tableFilterContainer");
	
</script>