//INSURER ----------------------------------------------------------
function Insurer(i) {

	this.init = init;
	this.populate = populate;
	
	this.init(i)
	
	function init(i) {	
			this.populate(i);
	}
	
	function populate(i) {

/*      
    "InsuranceCard": {
        "__type": "InsuranceCard:#SkyLinkWCFService.DataTypes",
        "AgentName": "",
        "AgentPhoneNumber": "",
        "Dealership": {
			.
			.
			.
        },
        "InsuranceCarrier": "",
        "OwnerInformation": {
			.
			.
			.
			
        },
        "PolicyNumber": "",
        "Vehicle": {
        	.
        	.
        	.
        	
        }
    },
    */
		
		this.name = i.AgentName
		this.phone = i.AgentPhoneNumber;
		this.carrier = i.InsuranceCarrier;
		this.policyNumber = i.PolicyNumber;
	}	
}
