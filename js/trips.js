//TRIP ----------------------------------------------------------
function Trip(parent,id, t) {
	//Public Functions
	this.init = init;
	this.plot = plot;

	//Properties
	this.parent = parent;
	this.id = id;

	this.maxSpeed = (t.MaxSpeed != null ? convertFromMeters(t.MaxSpeed*1000, "mph", 0) : 0);
	this.distance = convertFromMeters(t.Distance, "mph", 2);
	this.duration = t.Duration;
	this.start = new Location(this,0,t.TripStart, "green");
	this.end = new Location(this,0,t.TripEnd, "red");


	function plot() {
		this.start.plot(true);
		this.end.plot(true);
		
		var bounds = new google.maps.LatLngBounds();
		bounds.extend(this.start.myLatlng);
		bounds.extend(this.end.myLatlng);
		g_map.map.fitBounds(bounds);
	}
}

/**************************************
*
* SAMPLE OVERLAY
*
***************************************/
function USGSOverlay(bounds, image, map) {
  // Now initialize all properties.
  this.bounds_ = bounds;
  this.image_ = image;
  this.map_ = map;

  // We define a property to hold the image's
  // div. We'll actually create this div
  // upon receipt of the add() method so we'll
  // leave it null for now.
  this.div_ = null;

  // Explicitly call setMap() on this overlay
  this.setMap(map);
}

USGSOverlay.prototype = new google.maps.OverlayView();

USGSOverlay.prototype.onAdd = function() {

  // Note: an overlay's receipt of onAdd() indicates that
  // the map's panes are now available for attaching
  // the overlay to the map via the DOM.

  // Create the DIV and set some basic attributes.
  var div = document.createElement('DIV');
  div.style.border = "none";
  div.style.borderWidth = "5px";
  div.style.position = "absolute";
  div.innerHTML = '<div id="vlistContainer3">'+vehicles.list[0].html()+'</div>';


  // Set the overlay's div_ property to this DIV
  this.div_ = div;
 
  // We add an overlay to a map via one of the map's panes.
  // We'll add this overlay to the overlayImage pane.
  var panes = this.getPanes();
  panes.overlayLayer.appendChild(div);
}

USGSOverlay.prototype.draw = function() {

  // Size and position the overlay. We use a southwest and northeast
  // position of the overlay to peg it to the correct position and size.
  // We need to retrieve the projection from this overlay to do this.
  var overlayProjection = this.getProjection();

  // Retrieve the southwest and northeast coordinates of this overlay
  // in latlngs and convert them to pixels coordinates.
  // We'll use these coordinates to resize the DIV.
  var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
  var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

  // Resize the image's DIV to fit the indicated dimensions.
  var div = this.div_;
  div.style.left = sw.x + 'px';
  div.style.top = ne.y + 'px';
//  div.style.width = (ne.x - sw.x) + 'px';
//  div.style.height = (sw.y - ne.y) + 'px';
}




//TRIPS ----------------------------------------------------------
function Trips(parent) {
    this.parent = parent;

	// Public Functions
	this.init = init;
	this.add = add;
	this.addList = addList;
	this.insert = insert;
	this.shift = shift;
	this.getTripsSummaries = getTripsSummaries;
	this.unplot = unplot;

	this.init();

	
	function init() {
		if (this.list!=undefined) {
			this.unplot();
		}
		this.list = [];	
	}

	function addList(tlist) {
		for (var i in tlist.d) {
			t = tlist.d[i];
			this.add(i, t);
		}

//		this.parent.setIgnStatus(this.list[0].IgnitionOn);
	}

	function add(tripNumber, t) {
		this.list[tripNumber] = new Trip(this, tripNumber, t);
	}

	function insert(tlist) {
		this.shift(tlist.d.length);
		this.addList(tlist);
	}

	function shift(numItems) {
		n = this.list.length;
		for (var i=n-numItems-1; i>=numItems-1; i--)
		{
			this.list[i+1] = this.list[i];
		}
	}

	function getTripsSummaries(startDate, endDate, numEvents) {
		if (!startDate) { 
			var startDate=0; 
		} else { 
			startDate = new Date(startDate); 
			startDate.setHours(startDate.getHours()+7);
			startDate = startDate.getTime();
		}
		
		if (!endDate) { 
			var endDate=new Date(); 
		} else {
			endDate = new Date(endDate);
			endDate.setDate(endDate.getDate()+1);
		}
		
		//Server isn't smart enough to compensate yet, so we will calculate timezone offset for it.
		endDate.setHours(endDate.getHours()+7);
		endDate = endDate.getTime(); 			
		
		if (!numEvents) { var numEvents=10; }
		var _this = this;
		// Request filters by UTC, but response comes back in local timezone.
		// TODO: Confirm response is in local timeszone.
		postRequest(services.GetTripsSummaries,'{"vin":"' + this.parent.vin + '","startDate":"\/Date('+startDate+'-0000)\/", "endDate":"\/Date('+endDate+'-0700)\/","maxReports":100}',
				function(json, textStatus, jq)
				{
			// Received locations for a vehicle, so repopulate the list.
			_this.init();
			_this.addList(eval('('+jq.responseText+')'));

			populateTrips();
		});
	}

	function unplot(removeAll) {
		//Remove anything that is plotted EXCEPT for the most recent location.
		for (var i in this.list) {
			if ((i>0) || (removeAll)) {
				this.list[i].start.unplot(removeAll);
				this.list[i].end.unplot(removeAll);
			}
		}
	}	
}
