function toggleAcctPanel() {
	if ($("#acctPanel").css("width")>"0px") {
		if ($("#addVehicle").css("display") == "block") {
			$("#addVehicle").fadeOut(125, function () { 
									$("#addVehicle").hide(); 
									$("#aSettings").fadeIn(125, function() {
															$("#acctPanel").css("width","360px"); 
														});
								});			


		} else if ($("#account").css("display") == "block") {
			$("#account").fadeOut(125, function () { $("#account").hide(); $("#aSettings").fadeIn(125, function() {$("#acctPanel").css("width","360px"); }); });
		} else if ($("#maintIntervals").css("display") == "block") {
			$("#maintIntervals").fadeOut(125, function () { $("#maintIntervals").hide(); $("#aSettings").fadeIn(125, function() {$("#acctPanel").css("width","360px"); }); });
		} else if ($("#editMaintIntervals").css("display") == "block") {
			$("#editMmaintIntervals").fadeOut(125, function () { $("#editMaintIntervals").hide(); $("#aSettings").fadeIn(125, function() {$("#acctPanel").css("width","360px"); }); });
		} else {
			hideAcctPanel();
		}
	} else {
		hideHistoryPanel();
		hideCtrlPanel();
		showAcctPanel();
	}
}

function showAcctPanel() {
	initAcctPanel();
	
	//	$("#btnAccount").attr("src", "./images/btn_back.png");
	adjustAcctPanelPosition();	
	$("#acctPanel").show();
	$("#aSettings").show();
	$("#acctPanel").animate({width:"360px", height:$("#aSettings").height(), opacity:"0.85"},250);

	$("#menuIndicator").css("left","200px").show();
}

function isAcctPanelOpen() {
	return ($("#acctPanel").css("width")>"0px");
}

function adjustAcctPanelPosition() {
	$("#acctPanel").css("top",($("#vSelector").height()+5)+"px");
	$("#menuIndicator").css("top",($("#vSelector").height()-20)+"px");
}

function refreshAcctPanel() {
	if (isAcctPanelOpen()) {
		adjustAcctPanelPosition();
		initAcctPanel();
	}
}

function hideAcctPanel() {
//		$("#btnAccount").attr("src", "./images/btn_ctrlPanelOpen.png");
		$("#acctPanel").animate({width:"0px",height:"0px",opacity:"0.0"},250,"linear" ,function() { 
			$("#acctPanel").hide();		
			$("#addVehicle").hide();
			$("#account").hide();
			$("#maintIntervals").hide();
			$("#editMaintIntervals").hide();
 		});
		$("#menuIndicator").hide();
}

function initAcctPanel() {
	// Only get cellphone providers if we haven't already loaded them.
	if ($("#cellProvider").html() == "") {
		getCellPhoneProviders(function () {
			// Need to do this after we have cellphone providers, otherwise the 
			// value in the dropdown will not be correctly populated.
			populateAcctPanel();
		});
	} else {
		populateAcctPanel();
	}
}	

function populateAcctPanel() {
	// Only populate the screen if we have actually populated our global user object,
	// and we have loaded cellphone provider values.
	if ((g_accountUser.firstName != undefined) && ($("#cellProvider").html() != "")) {
		$("#customerError").html("");

		$("#firstName").val(g_accountUser.firstName);
		$("#lastName").val(g_accountUser.lastName);

		$("#streetAddress1").val(g_accountUser.streetAddress1);
		$("#streetAddress2").val(g_accountUser.streetAddress2);
		$("#suite").val(g_accountUser.suite);
		$("#city").val(g_accountUser.city);
		$("#state").val(g_accountUser.state);
		$("#postalCode").val(g_accountUser.postalCode);

		$("#primaryPhone").val(g_accountUser.primaryPhone);
		$("#cellPhone").val(g_accountUser.cellPhone);
		$("#cellProvider").val(g_accountUser.cellProvider);

		$("#emailAddress").val(g_accountUser.emailAddress);
		$("#emailConfirm").val(g_accountUser.emailConfirm);

		$("#timeZone").val(timezones.filter(function (tz) { return tz.value == g_accountUser.timeZone; })[0].name);
	}
}

function acctPanelSubmit() {
//	if (($("#newPassword").val() != "") && (($("#oldPassword").val() == "") || ($("#oldPassword").val() == $("#oldPassword").val())) {
//		alert("Bad password - please check old and new passwords and ensure they are different.");
//	} else {
		getCurrentValues();
		g_accountUser.updateUserInfo(function() {});
//	}
  toggleAcctPanel();		
}

function getCurrentValues() {
	g_accountUser.firstName = $("#firstName").val();
	g_accountUser.lastName = $("#lastName").val();

	g_accountUser.streetAddress1 = $("#streetAddress1").val();
	g_accountUser.streetAddress2 = $("#streetAddress2").val();
	g_accountUser.suite = $("#suite").val();
	g_accountUser.city = $("#city").val();
	g_accountUser.state = $("#state").val();
	g_accountUser.postalCode = $("#postalCode").val();
	
	g_accountUser.primaryPhone = $("#primaryPhone").val();
	g_accountUser.cellPhone = $("#cellPhone").val();
	g_accountUser.cellProvider = $("#cellProvider").val();
	
	g_accountUser.emailAddress = $("#emailAddress").val();
	g_accountUser.emailConfirm = $("#emailConfirm").val();

	g_accountUser.timeZone = timezones.filter(function (tz) { return tz.name == $("#timeZone").val(); })[0].value;
}

function getCellPhoneProviders(success) {
	postRequest(services.GetCellPhoneProviders, null,
			function(json, textStatus, jq) {
		populateCellPhoneProviders(json.d);
		success();
	});
}

function populateCellPhoneProviders(response) {
	var providerList = "";
	for (p in response) {
		providerList = providerList + '<option value="'+ response[p] +'">'+response[p]+'</option>';
	}
	$("#cellProvider").html(providerList);
}	

function printWalletCards() {
	var v = vehicles.getCurrentVehicle();

	// Save existing values of those elements that cause a cyclical reference
	var parent = v.parent;
	var locations = v.locations;
	var events = v.events;
	var geofences = v.geofences;
	var trips = v.trips;
	var plans = v.plans;
	
	// Now null them
	v.parent = null;
	v.locations = null;
	v.events = null;
	v.geofences = null;
	v.trips = null;
	v.plans = null;
	
	document.cookie = "vehicle="+escape(JSON.stringify(v));
	window.open('./walletcard.html');

	// Restore values of those elements that cause a cyclical reference
	v.parent = parent;
	v.locations = locations;
	v.events = events;
	v.geofences = geofences;
	v.trips = trips;
	v.plans = plans;
}

function printCertificate() {
	var v = vehicles.getCurrentVehicle();

	// Save existing values of those elements that cause a cyclical reference
	var parent = v.parent;
	var locations = v.locations;
	var events = v.events;
	var geofences = v.geofences;
	var trips = v.trips;
	var plans = v.plans;

	// Now null them
	v.parent = null;
	v.locations = null;
	v.events = null;
	v.geofences = null;
	v.trips = null;
	v.plans = null;
	
	document.cookie = "vehicle="+escape(JSON.stringify(v));
	window.open('./certificate.html');

	// Restore values of those elements that cause a cyclical reference
	v.parent = parent;
	v.locations = locations;
	v.events = events;
	v.geofences = geofences;
	v.trips = trips;
	v.plans = plans;
}

function addVehicle() {
	// TODO: Consider building add vehicle into PROTECT.  For now we will just use the existing
	// ASPX page since we need to continue supporting our SVR only users.
	//$('#aSettings').fadeOut(125, function() {$('#addVehicle').fadeIn(125, function() {$('#acctPanel').css('width','430px'); });})"
	
	window.open('https://www.mysky-link.com/AddVehicle.aspx', "_self");	
}

function updatePaymentInfo()
{
	initCart(1);	// 1=Update Payment Info
}

//MAINT SETTINGS
function initMaintSettings() {
	// Get maintenance intervals defined on the account
	if (g_accountUser.maintIntervals==null) {
		g_accountUser.maintIntervals = new MaintIntervals(g_accountUser, function() { populateAcctMaintIntervals(); });
	} else {
		populateAcctMaintIntervals(); 
	}
}

function populateAcctMaintIntervals() {
	$("#acctPanel").css("width","600px");
	$('#aSettings').fadeOut(125, function() {$('#maintIntervals').fadeIn(125);})
	
	var maintArray = [];
	if (g_accountUser.maintIntervals.list.length > 0) {
		var maintList = g_accountUser.maintIntervals.list;
		for ( var i = 0; i < maintList.length; i++) {
			var m = maintList[i];
			var a = [ m.name, m.description];
			maintArray.push(a);
		}
	}
	$("#maintIntervalsTable").dataTable().fnClearTable();
	$("#maintIntervalsTable").dataTable().fnAddData(maintArray);

	// We repopulated our rows, so add click handlers to them.
	$("#maintIntervalsTable tbody tr").click(
			function(e) {
				if ($(this).hasClass("row_selected")) {
					$(this).removeClass("row_selected");
				} else {
					$("#maintIntervalsTable").dataTable().$("tr.row_selected")
					.removeClass("row_selected");
					$(this).addClass("row_selected");
				}
			});

	$("#maintIntervalsTable tbody tr").dblclick(
			function(e) {
				if ($(this).hasClass("row_selected")) {
					$(this).removeClass("row_selected");
				} else {
					$("#maintIntervalsTable").dataTable().$("tr.row_selected")
					.removeClass("row_selected");
					$(this).addClass("row_selected");
				}
				editMaintInterval(g_accountUser.maintIntervals.list[$("#maintIntervalsTable").dataTable().fnGetPosition(this)]);
			});

	// Because we hid our table, we need to go through a resize to make sure
	// everything (especially our buttons!) are sized and work properly.
	resizeTable($('#maintIntervalsTable'));
}

function editMaintInterval(interval) {
	$("#maintIntervalTitle").html(interval.name);
	
	$("#maintID").val(interval.id);
	$("#maintIntervalID").val(interval.intervalID);
	$("#maintIndex").val(interval.index);
	
	$("#maintName").val(interval.name);
	$("#maintDescription").val(interval.description);
	$("#maintDays").val(interval.interval.days);
	$("#maintDistance").val(interval.interval.distance);
	$("#maintHours").val(interval.interval.hours);
	$("#maintDaysThreshold1").val(interval.threshold1.days);
	$("#maintDaysThreshold2").val(interval.threshold2.days);
	$("#maintDaysThreshold3").val(interval.threshold3.days);
	$("#maintDistanceThreshold1").val(interval.threshold1.distance);
	$("#maintDistanceThreshold2").val(interval.threshold2.distance);
	$("#maintDistanceThreshold3").val(interval.threshold3.distance);
	$("#maintHoursThreshold1").val(interval.threshold1.hours);
	$("#maintHoursThreshold2").val(interval.threshold2.hours);
	$("#maintHoursThreshold3").val(interval.threshold3.hours);
	
	$('#maintIntervals').fadeOut(125, function() {$('#editMaintIntervals').fadeIn(125);})
}

function editNewMaintInterval() {
	$("#maintIntervalTitle").html("New Maintenance Interval");
	
	$("#maintID").val(-1);
	$("#maintIntervalID").val(-1);
	$("#maintIndex").val(g_accountUser.maintIntervals.list.length);

	$("#maintName").val("");
	$("#maintDescription").val("");
	$("#maintDays").val("");
	$("#maintDistance").val("");
	$("#maintHours").val("");
	$("#maintDaysThreshold1").val("");
	$("#maintDaysThreshold2").val("");
	$("#maintDaysThreshold3").val("");
	$("#maintDistanceThreshold1").val("");
	$("#maintDistanceThreshold2").val("");
	$("#maintDistanceThreshold3").val("");
	$("#maintHoursThreshold1").val("");
	$("#maintHoursThreshold2").val("");
	$("#maintHoursThreshold3").val("");
	
	$('#maintIntervals').fadeOut(125, function() {$('#editMaintIntervals').fadeIn(125);})
	
}

function submitMaintInterval() {
	if (($("#maintDays").val() > 0) && (isNaN($("#maintDaysThreshold1").val()))) {
		$("#maintDaysThreshold1").val($("#maintDays").val());
	} 
	
	if (($("#maintDistance").val() > 0) && (isNaN($("#maintDistanceThreshold1").val()))) {
		$("#maintDistanceThreshold1").val($("#maintDistance").val());
	} 

	if (($("#maintHours").val() > 0) && isNaN(($("#maintHoursThreshold1").val()))) {
		$("#maintHoursThreshold1").val($("#maintHours").val());
	} 
	
	var interval = getMaintIntervalValues();
	var errString = validateMaintInterval(interval);
	
	if (errString == "") {

		// If maintID has a non-negative value, then we must have been editing an existing one, so update it...
		if 	($("#maintID").val() > -1) {
			updateMaintInterval(interval);
		} else {
			// ...otherwise, add a new one.
			addMaintInterval();
		}

		// Then refresh our table with the new values...
		populateAcctMaintIntervals(interval);

		// ...and redisplay it
		$('#editMaintIntervals').fadeOut(125, function() {$('#maintIntervals').fadeIn(125);}) 
	} else {
		//TODO: warn user of errors
		$("#maintError").html(errString);
	}

}

function getMaintIntervalValues() {
	return {
		"ID" : $("#maintIntervalID").val(),
		"AccountID" : g_accountUser.id,
		"Index" : $("#maintIndex").val(),
		"Name" : $("#maintName").val(),
		"Description" : $("#maintDescription").val(),
		"Status" : 1,
		"Interval" : {	"Days" : ($("#maintDays").val()!="" ? $("#maintDays").val() : null),
						"Distance" : ($("#maintDistance").val()!="" ? $("#maintDistance").val() : null),
						"Hours" : ($("#maintHours").val()!="" ? $("#maintHours").val() : null)
					 },
		"Threshold1" : {	"Days" : ($("#maintDaysThreshold1").val()!="" ? $("#maintDaysThreshold1").val() : null),
							"Distance" : ($("#maintDistanceThreshold1").val()!="" ? $("#maintDistanceThreshold1").val() : null),
							"Hours" : ($("#maintHoursThreshold1").val()!="" ? $("#maintHoursThreshold1").val() : null)
					   },
		"Threshold2" : {	"Days" : ($("#maintDaysThreshold2").val()!="" ? $("#maintDaysThreshold2").val() : null),
							"Distance" : ($("#maintDistanceThreshold2").val()!="" ? $("#maintDistanceThreshold2").val() : null),
							"Hours" : ($("#maintHoursThreshold2").val()!="" ? $("#maintHoursThreshold2").val() : null)
					   },
		"Threshold3" : {	"Days" : ($("#maintDaysThreshold3").val()!="" ? $("#maintDaysThreshold3").val() : null),
							"Distance" : ($("#maintDistanceThreshold3").val()!="" ? $("#maintDistanceThreshold3").val() : null),
							"Hours" : ($("#maintHoursThreshold3").val()!="" ? $("#maintHoursThreshold3").val() : null)
					   }
	};
}

function validateMaintInterval(interval) {
	var retVal = "";
	
	// Check name is not empty.
	if (interval.Name == "") {
		retVal = retVal + "Please provide a name.";
	}
	
	// Check that there is at least one interval defined.
	if ((interval.Interval.Days == null) && (interval.Interval.Distance == null) && (interval.Interval.Hours == null)) {
		retVal = retVal + "Please enter a day, distance or hour-based interval value.<br/>";
	} 
	
	// Check for only numeric values in interval fields (where non-null)
	// and force a Threshold3 if one is not specified.
	if (interval.Interval.Days != null) {
		if (isNaN(interval.Interval.Days)) {
			retVal = retVal + "<b># Days:</b> Please enter only numeric interval values.<br/>";
		} else {
			// Value is numeric, so force Threshold3 if one is not specified.
			if (interval.Threshold3.Days == null) {
				interval.Threshold3.Days = interval.Interval.Days;
			}
		}
	}

	if (interval.Interval.Distance != null) {
		if (isNaN(interval.Interval.Distance)) {
			retVal = retVal + "<b>Distance:</b> Please enter only numeric interval values.<br/>";
		} else {
			// Value is numeric, so force Threshold3 if one is not specified.
			if (interval.Threshold3.Distance == null) {
				interval.Threshold3.Distance = interval.Interval.Distance;
			}
		}
	}

	if (interval.Interval.Hours != null) {
	if (isNaN(interval.Interval.Hours)) {
		retVal = retVal + "<b># Hours:</b> Please enter only numeric interval values.<br/>";
	} else {
		// Value is numeric, so force Threshold3 if one is not specified.
		if (interval.Threshold3.Hours== null) {
			interval.Threshold3.Hours = interval.Interval.Hours;
		}
	}
}
	
	// Check for only numeric values in threshold fields (where non-null).
	if (((interval.Threshold1.Days != null) && (isNaN(interval.Threshold1.Days))) ||
		((interval.Threshold2.Days != null) && (isNaN(interval.Threshold2.Days))) ||
		((interval.Threshold3.Days != null) && (isNaN(interval.Threshold3.Days)))) {
		retVal = retVal + "<b>Days Threshold:</b> Please enter only numeric interval values.<br/>";
	}
	
	if (((interval.Threshold1.Distance != null) && (isNaN(interval.Threshold1.Distance))) ||
		((interval.Threshold2.Distance != null) && (isNaN(interval.Threshold2.Distance))) ||
		((interval.Threshold3.Distance != null) && (isNaN(interval.Threshold3.Distance)))) {
			retVal = retVal + "<b>Distance Threshold:</b> Please enter only numeric interval values.<br/>";
		}

	if (((interval.Threshold1.Hours != null) && (isNaN(interval.Threshold1.Hours))) ||
		((interval.Threshold2.Hours != null) && (isNaN(interval.Threshold2.Hours))) ||
		((interval.Threshold3.Hours != null) && (isNaN(interval.Threshold3.Hours)))) {
			retVal = retVal + "<b>Hours Threshold:</b> Please enter only numeric interval values.<br/>";
		}

	// Check for threshold values where there is no interval defined, and warn user accordingly.
	if (((interval.Threshold1.Days>0) || (interval.Threshold2.Days>0) || (interval.Threshold3.Days>0)) &&
			   (interval.Interval.Days== null)) {
			var response = confirm("Since no day-based interval has been defined, the day-based thresholds will have no effect.\n\nClick OK to clear the threshold values you entered or Cancel to continue editing.");		
			if (response == true) {
				$("#maintDaysThreshold1").val("");
				$("#maintDaysThreshold2").val("");
				$("#maintDaysThreshold3").val("");
				interval.Threshold1.Days = null;
				interval.Threshold2.Days = null;
				interval.Threshold3.Days = null;
			} else {
				retVal = retVal + "Enter a day-based interval, or clear the day-based thresholds.<br/>";
			}
		}

	if (((interval.Threshold1.Distance>0) || (interval.Threshold2.Distance>0) || (interval.Threshold3.Distance>0)) &&
			   (interval.Interval.Distance== null)) {
			var response = confirm("Since no distance-based interval has been defined, the distance-based thresholds will have no effect.\n\nClick OK to clear the threshold values you entered or Cancel to continue editing.");		
			if (response == true) {
				$("#maintDistanceThreshold1").val("");
				$("#maintDistanceThreshold2").val("");
				$("#maintDistanceThreshold3").val("");
				interval.Threshold1.Distance = null;
				interval.Threshold2.Distance = null;
				interval.Threshold3.Distance = null;
			} else {
				retVal = retVal + "Enter a distance-based interval, or clear the distance-based thresholds.<br/>";
			}
		}

	if (((interval.Threshold1.Hours>0) || (interval.Threshold2.Hours>0) || (interval.Threshold3.Hours>0)) &&
		   (interval.Interval.Hours == null)) {
		var response = confirm("Since no hour-based interval has been defined, the hour-based thresholds will have no effect.\n\nClick OK to clear the threshold values you entered or Cancel to continue editing.");		
		if (response == true) {
			$("#maintHoursThreshold1").val("");
			$("#maintHoursThreshold2").val("");
			$("#maintHoursThreshold3").val("");
			interval.Threshold1.Hours = null;
			interval.Threshold2.Hours = null;
			interval.Threshold3.Hours = null;
		} else {
			retVal = retVal + "Enter an hour-based interval, or clear the hour-based thresholds.<br/>";
		}
	}
	
	return retVal;
}

function updateMaintInterval(interval) {
	// Populate our existing interval with new values
	var intervalID = $("#maintID").val();
	g_accountUser.maintIntervals.list[intervalID].update(interval);
	g_accountUser.maintIntervals.update();	
}

function addMaintInterval() {
	// Populate our new interval with default values and add to the DB.
	g_accountUser.maintIntervals.insert(interval);
	g_accountUser.maintIntervals.add();
}

function deleteMaintInterval(interval) {
	var intervalIDs = [interval.intervalID];
	g_accountUser.maintIntervals.remove(intervalIDs);
	
	// Then refresh our table with the new values...
	populateAcctMaintIntervals();
	
	// ...and redisplay it
	$('#editMaintIntervals').fadeOut(125, function() {$('#maintIntervals').fadeIn(125);})
}