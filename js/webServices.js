//Web Services ----------------------------------------------------------
var services = {
		GetVehicles           				: {number:100, name:"GetVehicles"},
		GetLastLocation       				: {number:101, name:"GetLastLocation"},
		GetLastNLocations     				: {number:102, name:"GetLastNLocations"},
		RequestLocationUpdate 				: {number:103, name:"RequestLocationUpdate"},
		GetLocationsAfterId   				: {number:104, name:"GetLocationsAfterId"},
		GetLocationsAfterDate 				: {number:105, name:"GetLocationsAfterDate"},
		IsQuickFenceSet       				: {number:106, name:"IsQuickFenceSet"},
		SetQuickFence         				: {number:107, name:"SetQuickFence"},
		GetDashboard          				: {number:108, name:"GetDashboard"},
		GetTripsSummaries   				: {number:110, name:"GetTripsSummaries"},
		GetGeofences		   				: {number:111, name:"GetGeofences"},
		StoreGeofence		   				: {number:112, name:"StoreGeofence"},
		SetGeofence		   					: {number:113, name:"SetGeofence"},
		GetVehiclesByVINSuffix				: {number:200, name:"GetVehiclesByVINSuffix"},
		GetEvents               			: {number:201, name:"GetEvents"},
		GetCustomerInfo         			: {number:300, name:"GetCustomerInfo"},
		UpdateCurrentSkyLinkUser			: {number:301, name:"UpdateCurrentSkyLinkUser"},
		GetCellPhoneProviders   			: {number:302, name:"GetCellPhoneProviders"},
		GetCCInfo							: {number:303, name:"GetCCInfo"},
		UpdateCCInfo						: {number:304, name:"UpdateCCInfo"},
		SetSpeedAlert		    			: {number:400, name:"SetSpeedAlert"},
		GetProductOfferings					: {number:500, name:"GetProductOfferings"},
		SubmitCart							: {number:501, name:"SubmitCart"},
		SubmitOrder							: {number:502, name:"SubmitOrder"},
		SetBatteryAlertState				: {number:600, name:"SetBatteryAlertState"},	
		IsBatteryAlertEnabled				: {number:601, name:"IsBatteryAlertEnabled"},
		GetMaintenanceIntervals				: {number:700, name:"GetMaintenanceIntervals"},
		UpdateMaintenanceIntervals			: {number:701, name:"UpdateMaintenanceIntervals"},
		DeleteMaintenanceIntervals			: {number:702, name:"DeleteMaintenanceIntervals"},
		AddMaintenanceIntervals				: {number:703, name:"AddMaintenanceIntervals"},
		GetVehicleMaintenanceIntervals		: {number:704, name:"GetVehicleMaintenanceIntervals"},
		UpdateVehicleMaintenanceIntervals	: {number:705, name:"UpdateVehicleMaintenanceIntervals"},
		DeleteVehicleMaintenanceIntervals	: {number:706, name:"DeleteVehicleMaintenanceIntervals"},
		AddVehicleMaintenanceIntervals		: {number:707, name:"AddVehicleMaintenanceIntervals"},
		LogVehicleMaintenanceIntervals		: {number:708, name:"LogVehicleMaintenanceIntervals"},	
		GetCouponOfferings					: {number:1000,name:"GetCouponOfferings"}
}

function postRequest(service, data, success, error)
{
	if (data==null) data='""';
	
	var vin = null;
	var vehicleNumber = null;
	
	if (data!="") {
		vin=(eval('('+data+')').vin);
		vehicleNumber=vehicles.getID(vin);
	}

	if (error==undefined) {
		error = function(json, textStatus, jq) {
			// If our fake call to GetVehiclesByVINSuffix returns an error, assume we need to login.
			if ((~json.responseText.indexOf("Object reference not set to an instance of an object.")) && (service == services.GetVehiclesByVINSuffix) && (~data.indexOf("999999"))) {
				window.location.replace('main.aspx');
			} else {
				webServiceErrorHandler(service.name, textStatus.toUpperCase(), (json.responseText!="" && json.responseText!= null ? eval("(" + json.responseText + ")") : null));
			}
		}
	}

	var svcUrl = "./Services/SkyLinkService.svc/json/";
		
	$.ajax(
			{
				type: "POST",
				url: svcUrl + service.name + '?q='+new Date().getTime(),
				dataType: "json",
				data: data,	
				contentType: "application/json; charset=utf-8",
				success: success,
				failure: function(json)
				{
				},
				error: function(json, textStatus, jq) {
					error(json, textStatus, jq);
				},
				complete: function(json)
				{
				}
			});
}

//This method shows friendly error messages where appropriate, and generic error messages
//where a friendly one is not available.  Errors will generally be caused by invalid web
//service responses, so the method signature is tailored as such although , it can be used
//for other purposes.
function webServiceErrorHandler(service, status, response) {
	switch (service) {
	case "GetCouponOfferings" : 
		switch (response.Message) {
		case "UnknownCode" : 
			showError("Coupon code is not recognized","Please check the code you entered and try again.");
			break;
		case "Expired" : 
			showError("This coupon has expired.", "Please check the code you entered and try again.");
			break;
		case "AlreadyUsed" : 
			showError("This coupon code is not valid.", "Please check the code you entered and try again.");
			break;
		case "NotProvisioned" : 
			showError("This coupon code is invalid.", "Please check the code you entered and try again.");
			break;
		}
		break;
	default:
		if ((response != null) && (response != "") && (response != undefined)) {
			showError("Invalid response from web service<br/>"+service, status+"<br/>"+response.Message);
		} else {
			showError("Invalid response from web service<br/>"+service, status);
		}
	}
}
