//CUSTOMER ----------------------------------------------------------
function Customer() {

	this.id = 0;
	
	this.firstName = "";
	this.lastName = "";
	
	this.streetAddress1 = "";
	this.streetAddress2 = "";
	this.suite = "";
	this.city = "";
	this.state = "";
	this.postalCode = "";
	
	this.primaryPhone = "";
	this.cellPhone = "";
	this.cellProvider = "";
	
	this.canAddVehicles = false;
	this.canManageUsers = false;
	this.canRenewSubscription = false;
	
	this.alternateEmail = "";
	this.emailAddress = "";
	
	//TODO: Web service does not provide timeZone - need to modify WCF 
	//      method to provide it, and set if when changed.
	// this.timmeZone = "";
	
	this.emailNotifications = 0;
	this.smsNotifications = 0;
	
	//TODO: Should not receive secret question answer or PIN in plain text
	this.secretQuestionChoice = 0;
	this.secretQuestionAnswer = "";
	this.PIN = 0;
	this.newPassword = "";
	this.oldPassword = "";

	this.getCustomerInfo = getCustomerInfo;
	this.populate = populate;
	this.updateCustomerInfo = updateCustomerInfo;
	this.serialize = serialize;
	
	function getCustomerInfo(success) {
		_this = this;
		postRequest(services.GetCustomerInfo, null,
				function(json, textStatus, jq) {
			_this.populate(json.d);
			success();
		});
	}
	
	function populate(response) {

		/*
		"{"d":{"__type":"SkyLinkUserInformation:http:\/\/www.mysky-link.com\/skylink"," +
				""AlternateEmail":""," +
				""CanAddVehicles":true," +
				""CanManageUsers":true," +
				""CanRenewSubscription":true," +
				""CellPhone":"602-614-2034"," +
				""CellProvider":"AT&T Wireless"," +
				""City":"Chandler"," +
				""EmailAddress":"godonnell@inilex.com"," +
				""EmailNotifications":63," +
				""FirstName":"Phil"," +
				""Id":3119," +
				""LastName":"DeCarlo"," +
				""PIN":5565," +
				""PostalCode":"85224"," +
				""PrimaryPhone":"480-889-5676"," +
				""SecretQuestionAnswer":null," +
				""SecretQuestionChoice":5," +
				""SmsNotifications":15," +
				""State":"AZ"," +
				""StreetAddress1":"460 S Benson Lane"," +
				""StreetAddress2":"b"," +
				""Suite":null}}"
		*/

		this.id = response.Id;
		
		this.firstName = response.FirstName;
		this.lastName = response.LastName;
		
		this.streetAddress1 = response.StreetAddress1;
		this.streetAddress2 = response.StreetAddress2;
		this.suite = response.Suite;
		this.city = response.City;
		this.state = response.State;
		this.postalCode = response.PostalCode;
		
		this.primaryPhone = response.PrimaryPhone;
		this.cellPhone = response.CellPhone;
		this.cellProvider = response.CellProvider;
		
		this.canAddVehicles = response.CanAddVehicles;
		this.canManageUsers = response.CanManageUsers;
		this.canRenewSubscription = response.CanRenewSubscription;
		
		this.alternateEmail = response.AlternateEmail;
		this.emailAddress = response.EmailAddress;
		
		// this.timeZone = response.timeZone;
		
		this.emailNotifications = response.EmailNotifications;
		this.smsNotifications = response.SmsNotifications;
		
		//TODO: Should not receive secret question answer or PIN in plain text
		this.secretQuestionChoice = response.SecretQuestionChoice;
		this.secretQuestionAnswer = response.SecretQuestionAnswer;
		this.PIN = response.PIN;
	}	
	
	function updateCustomerInfo() {
		postRequest(services.UpdateCurrentSkyLinkUser, this.serialize(),
				function(json, textStatus, jq) {
			_this.populate(json.d);
			success();
		});	
	}

	function serialize() {
		return ('{"skyLinkUserInformation":'+
				 '{"FirstName":"'+this.firstName+'",'+
				 '"LastName":"'+this.lastName+'",'+
				 '"StreetAddress1":"'+this.streetAddress1+'",'+
				 '"StreetAddress2":"'+this.streetAddress2+'",'+
				 '"Suite":"'+this.suite+'",'+
				 '"City":"'+this.city+'",'+
				 '"State":"'+this.state+'",'+
				 '"PostalCode":"'+this.postalCode+'",'+
				 '"PrimaryPhone":"'+this.primaryPhone+'",'+
				 '"CellPhone":"'+this.cellPhone+'",'+
				 '"CellProvider":"'+this.cellProvider+'",'+
				 '"EmailAddress":"'+this.emailAddress+'",'+
				 '"AlternateEmail":"'+this.alternateEmail+'",'+
				 '"CanAddVehicles":'+this.canAddVehicles+','+
				 '"CanManageUsers":'+this.canManageUsers+','+
				 '"CanRenewSubscription":'+this.canRenewSubscription+','+
				 '"SmsNotifications":'+this.smsNotifications+','+
				 '"EmailNotifications":'+this.emailNotifications+','+
				 '"SecretQuestionAnswer":"'+this.secretQuestionAnswer+'",'+
				 '"SecretQuestionChoice":'+this.secretQuestionChoice+','+
				 '"PIN":'+this.PIN+','+
				 '"Id":'+this.id+
				 (this.newPassword != "" ? ',"newpassword":"'+this.newPassword+'","oldpassword":"'+this.oldPassword+'"' : '')+
				'}}');
	}

}

