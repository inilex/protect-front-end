g_infowindow = null;
g_infowindowVisible = false;
g_infowindowVehicle = null;

//MAP ----------------------------------------------------------
function Map() {
	//Public Functions
	this.display = display;
	this.plot = plot;
	this.showOrHide = showOrHide;
	this.showInfoWindow = showInfoWindow;
	this.showMarker = showMarker;

	//Properties
	this.map = null;
	this.defaultZoom = null;
	this.wasZoom = 12;

	function display(loc) {
		var vehicleNumber = loc.parentVehicle.vehicleNumber;
		if (!isVisible("map")) {
			show("map");
			
			// We hid all vehicles when showing the map canvas,
			// so show the vehicle we're interested in.
			show("vehicle_"+vehicleNumber);
			vehicles.showVehicle(vehicleNumber);
			
			// Setup the loc button to now refresh (update) the loc.
			document.getElementById("locationContainer_"+vehicleNumber).onclick=function() { vehicles.list[vehicleNumber].updateloc(); };
			this.plot(loc);
		}
	}

	function plot(loc) {
		// Only show loc if the map is actually visible
		if (isVisible("map")) {  

			var zoomFactor = (this.map==null ? 12 : this.map.getZoom()); //16; 							// Default zoom factor
			var mapType = google.maps.MapTypeId.ROADMAP; 	// Default map type

			// Dealer users will want to be see a hybrid view since they are looking for vehicles that are off the road.
			switch (g_userType.behaviors.defaultMapView) {
				case "hybrid" :
					mapType = google.maps.MapTypeId.HYBRID;
					break;
			}

			// If we are plotting the current vehicle, but not a "trip", we want to center the map
			if ((loc.parentVehicle.vehicleNumber == vehicles.currentVehicleNumber()) && !(loc.parent instanceof Trip)) {
				//But if we don't yet have a valid location location...
				if (loc.address == 'LOADING...') {
					// ...we will zoom out...
					this.wasZoom = this.map.getZoom();
					this.defaultZoom = true;
					this.map.setZoom(3);
				} else if (this.defaultZoom) {
					///...if we were zoomed out for lack of an address, restore the previous zoom factor...
					this.defaultZoom = false;
					if (this.map == null) {
						zoomFactor = this.wasZoom;
					} else {
						this.map.setZoom(this.wasZoom);
					}
					// Now that it is updated, force redisplay of the info window.
					g_infowindowVehicle = null;
				}
				//...now, center the map.
				var myOptions = {
						zoom: zoomFactor,
						center: loc.myLatlng,
						panControl : false,
						mapTypeId:  mapType
				};					
				

				if (this.map === null) {
					// Only create map first time through.
					this.map = new google.maps.Map(document.getElementById("map"), myOptions);
				} else {
					// Subsequently, just change the existing map's options.
					this.map.setOptions(myOptions);
				}
			}

			this.showOrHide("map_controls");

			if (g_userType==userTypes.Dealer) {		
				// Dealer users will want to be zoomed into the max since they are looking for vehicles in a relatively small area
				var maxZoomService = new google.maps.MaxZoomService;
				maxZoomService.getMaxZoomAtLatLng(myLatlng, function(response) {
					if (response.status == google.maps.MaxZoomStatus.OK) {
						this.map.setZoom(response.zoom);
					}
				});
			} else {
				// To add the marker to the map, call setMap();
				this.showMarker(loc);

				//If this vehicle is the one with focus (i.e. is the current vehicle), display callout
				if (loc.parentVehicle.vehicleNumber == vehicles.currentVehicleNumber()) {
					this.showInfoWindow(loc);
				}

				var _this = this;
				if (loc.listener == null) {
					// Only set listener on most recent location (i.e. the car icon)
					if (loc.index==0) {
						loc.listener = google.maps.event.addListener(loc.marker, 'click', function () { 
										g_map.showInfoWindow(loc);
						});
					}
				} else if (loc.index.locationNumber>0) {
					// Remove listener from markers that are not the most recent location.
					google.maps.event.removeListener(loc.listener);
					loc.listener = null;
				}

				hide("vlistContainer");
			}
		}
	}

	function showOrHide(id) {
		// This method shows or hides the map controls based on the size of the screen 
		// (and, of course, whether or not the map is created and visible).
		if ((id=="map_controls") && (isVisible("map")) && this.map != null) {
			var visibility = ((isVisible("vehicleList")) && ($(window).height() < 400) ? false : true);
			var myOptions = {
					mapTypeControl:visibility,
					mapTypeControlOptions:{
				position:google.maps.ControlPosition.LEFT_CENTER
			},
			zoomControl:visibility,
			zoomControlOptions:{
				position:google.maps.ControlPosition.RIGHT_CENTER
			},
			streetViewControl:visibility,
			streetViewControlOptions:{
				position:google.maps.ControlPosition.LEFT_CENTER
			}
			}

			this.map.setOptions(myOptions);
		}
	}

	function showInfoWindow(loc) {
		var html = '<div id="vlistContainer2">'+loc.parentVehicle.html()+'</div>';
		if (g_infowindow == null) {
			g_infowindow = new google.maps.InfoWindow({
				content: html
			});
		} else {
			// To prevent flashing, only redisplay if window is currently not visible, or vehicle has changed.
//			if ((g_infowindowVehicle != loc.parentVehicle.vehicleNumber) || (g_infowindow.map==null)) {
				g_infowindow.setContent(html);
//			}
		}
		
		// To prevent flashing, only redisplay if window is currently not visible, or vehicle has changed.
		if ((g_infowindow.map==null) || (g_infowindowVehicle != loc.parentVehicle.vehicleNumber)) {
			g_infowindow.open(this.map, loc.marker);
		}

		// Update current vehicle and vSelector.
		//TODO: build class for vSelector - or add currentVehicle to vehicles class and update there.
		vehicles.setCurrentVehicle(loc.parentVehicle.vehicleNumber);
		document.getElementById("span_vName").innerHTML=vehicles.getCurrentVehicle().name;

		g_infowindowVisible = true;
		g_infowindowVehicle = loc.parentVehicle.vehicleNumber;
	}
	
	function showMarker(loc) {
		loc.marker.setMap(this.map);
	}
}
