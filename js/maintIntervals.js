//MAINTENANCE INTERVAL ----------------------------------------------------------
function MaintInterval(i,parent, inserted) {
	this.parent = parent;
	this.changed = false;
	this.inserted = false;
	this.id= this.parent.list.length;
	
	this.init = init;
	this.populate = populate;
	this.update = update;
	
	this.init(i);
	
	function init(interval) {
		this.populate(interval);
		this.inserted = inserted;
	}
	
	function populate(interval) {
		this.accountID = interval.AccountID;
		this.intervalID = interval.ID;
		this.index = this.parent.list.length;

		this.name = interval.Name;
		this.description = interval.Description;
		this.interval = {"days":interval.Interval.Days, "distance":Math.round(interval.Interval.Distance*metersToMiles), "hours":interval.Interval.Hours};
		this.threshold1 = {"days":interval.Threshold1.Days, "distance":Math.round(interval.Threshold1.Distance*metersToMiles), "hours":interval.Threshold1.Hours};
		this.threshold2 = {"days":interval.Threshold2.Days, "distance":Math.round(interval.Threshold2.Distance*metersToMiles), "hours":interval.Threshold2.Hours};
		this.threshold3 = {"days":interval.Threshold3.Days, "distance":Math.round(interval.Threshold3.Distance*metersToMiles), "hours":interval.Threshold3.Hours};

		this.status = interval.Status;
	}

	function update(modInterval) {
		// Save existing values
		var _this = this;
		
		//Populate new values
		this.populate(modInterval);	
		
		// Populate control values that we saved earlier
		this.parent = _this.parent;
		this.id = _this.id;
		this.intervalID = _this.intervalID;
		this.status = _this.status;
		
		//Flag as modified
		this.changed = true;
	}
}

//MAINTENANCE INTERVALS ----------------------------------------------------------
function MaintIntervals(parent, success) {
	// Public properties
	this.parent = parent;
	this.list = [];

	// Public methods
	this.init = init;
	this.getMaintIntervals = getMaintIntervals;
	this.addList = addList;
	this.insert = insert;
	this.add = add;
	this.update = update;
	this.remove = remove;
	this.hide = hide;

	this.init(success);

	function init()
	{
		this.getMaintIntervals(success);
	}

	function getMaintIntervals() {
		var _this = this;
		postRequest(services.GetMaintenanceIntervals, '',
				function(json, textStatus, jq) {
			_this.addList(json.d);
			success();
		});
	}

	function addList(ilist) {
		for (var j=0; j<ilist.length; j++) {
			var i = ilist[j];
			this.list[this.list.length] = new MaintInterval(i,this);
		}
	}

	function insert(interval) {
		this.list[this.list.length] = new MaintInterval(interval,this,true);
	}
	
	function add() {
		var insertedIntervals = "";
		var insertRequired = false;
		
 		for (var j=0; j<this.list.length; j++) {
			var i = this.list[j];
			if (i.inserted) {
				insertRequired = true;
				insertedIntervals = insertedIntervals + insertedIntervals + (insertedIntervals!="" ? "," : "") +
						'{' +
							'"ID": '				+ i.intervalID +',' +
							'"AccountID": ' 		+ i.accountID +',' +
							'"Index":' 				+ i.index +',' +
							'"Name":"' 				+ i.name +'",' +
							'"Description":"' 		+ i.description +'",' +
							'"Status":'				+ i.status +',' +
							'"Interval": {' +
									'"Days":'		+ i.interval.days +',' +
									'"Distance":'	+ i.interval.distance +',' +
									'"Hours":'		+ i.interval.hours +
							'},' +
							'"Threshold1": {' +
								'"Days":'			+ i.threshold1.days +',' +
								'"Distance":'		+ i.threshold1.distance +',' +
								'"Hours":'			+i.threshold1.hours +
							'},' +
							'"Threshold2": {' +
								'"Days":'			+ i.threshold2.days +',' +
								'"Distance":'		+ i.threshold2.distance +',' +
								'"Hours":'			+ i.threshold2.hours +
							'},' +
							'"Threshold3": {' +
								'"Days":'			+ i.threshold3.days +',' +
								'"Distance":'		+ i.threshold3.distance +',' +
								'"Hours":'			+ i.threshold3.hours +
							'}' +					
						'}';					
			}
		}

 		if (insertRequired) {
 			var _this = this;
 			postRequest(services.AddMaintenanceIntervals, '{"intervals":[' + insertedIntervals + ']}',
				function(json, textStatus, jq) {
 					// Insert returns a new list of current maintenanIntervals, so rebuild our list from scratch.
 					_this.list = [];
 					_this.addList(json.d);
 			});				
 		}
	}
	
	function update() {
		var updatedIntervals = "";
		var updateRequired = false;
		
 		for (var j=0; j<this.list.length; j++) {
			var i = this.list[j];
			if (i.changed) {
				updateRequired = true;
				updatedIntervals = updatedIntervals + (updatedIntervals!="" ? "," : "") +
						'{' +
							'"ID":'					+ i.intervalID +',' +
							'"AccountID": ' 		+ i.accountID +',' +
							'"Index":' 				+ i.index +',' +
							'"Name":"' 				+ i.name +'",' +
							'"Description":"' 		+ i.description +'",' +
							'"Status":'				+ i.status +',' +
							'"Interval": {' +
									'"Days":'		+ i.interval.days +',' +
									'"Distance":'	+ parseFloat(i.interval.distance)/metersToMiles +',' +
									'"Hours":'		+ i.interval.hours +
							'},' +
							'"Threshold1": {' +
								'"Days":'			+ i.threshold1.days +',' +
								'"Distance":'		+ parseFloat(i.threshold1.distance)/metersToMiles +',' +
								'"Hours":'			+i.threshold1.hours +
							'},' +
							'"Threshold2": {' +
								'"Days":'			+ i.threshold2.days +',' +
								'"Distance":'		+ parseFloat(i.threshold2.distance)/metersToMiles +',' +
								'"Hours":'			+ i.threshold2.hours +
							'},' +
							'"Threshold3": {' +
								'"Days":'			+ i.threshold3.days +',' +
								'"Distance":'		+ parseFloat(i.threshold3.distance)/metersToMiles +',' +
								'"Hours":'			+ i.threshold3.hours +
							'}' +					
						'}';					
			}
		}

 		if (updateRequired) {
 			var _this = this;
 			postRequest(services.UpdateMaintenanceIntervals, '{"intervals":[' + updatedIntervals + ']}',
				function(json, textStatus, jq) {
		 			for (var j=0; j<_this.list.length; j++) {
 		 				var i = _this.list[j];
 		 				i.changed = false;
 		 			}				
 			});				
 		}
	}

	function remove(intervalIDs) {
		for (var j=0; j<this.list.length; j++) {
			var i = this.list[j];
			
			if (intervalIDs.indexOf(i.intervalID)>-1) {
				this.list.splice(j,1);
			}
		}
		
		postRequest(services.DeleteMaintenanceIntervals, '{"intervalIDs":' + JSON.stringify(intervalIDs) + '}',
				function(json, textStatus, jq) {
 			});				
	}
}
