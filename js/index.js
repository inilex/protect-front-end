//GLOBAL CONSTANTS ----------------------------------------------------------
var autoLocationCheckPeriod = 15 * 60 * 1000; // Check for updated locations
												// for each vehicle every 10
												// seconds
var locationUpdatePeriod = 2500; // Frequency with which to check for updated
									// locations following a request from user
var locationUpdateIterations = 15; // Number of times to check for location
									// update to arrive before timing out

var autoFenceCheckPeriod = 15 * 60 * 1000; // Update geofence status for each
											// vehicle every 10 seconds
var fenceStatusUpdatePeriod = 2500; // Frequency with which to check fence
									// status toggle took effect following a
									// request from user
var fenceStatusUpdateIterations = 15; // Number of times to check for fence
										// status toggle to take effect before
										// timing out

var batteryStatusUpdatePeriod = 2500; // Frequency with which to check battery
										// alert toggle took effect following a
										// request from user
var batteryStatusUpdateIterations = 15; // Number of times to check for battery
										// alert toggle to take effect before
										// timing out

var ScreenLayouts = {
	landscape : 0,
	portait : 1
}; // Valid screen layout configurations
var minLandscapeWidth = 400; // Width below which layout is considered
								// portrait

var metersToMiles = 0.000621371192; // Constant to convert meters to miles

var minSpeedThreshold = 45; // Minimum speed threshold that may be set
var maxSpeedThreshold = 100; // Maximum speed threshold that may be set
var speedThresholdStep = 5; // Increment for speed threshold slider

var minIdleTime = 5; 		// Minimum idle time that may be set
var maxIdleTime = 60; 		// Maximum idle time that may be set
var idleTimeStep = 5; 		// Increment for idle time slider

var minRadius = 0.1; // Minimum geofence radius that may be set
var maxRadius = 100; // Maximum geofence radius that may be set
var radiusStep = 0.1; // Increment for geofence radius slider

// GLOBAL ENUMS-----------------------------------------------

/************************************************************************************************************************************
 * userTypes.behaviors contains the following parameters used to control program behavior:
 * 
 * 		default 			- default screen display type when apps first loads: showMap, vinPrompt, vinLocPrompt
 * 		listVehicles 		- show vehicle list to the user : true, false
 * 		showVIN				- display VIN on vehicle detail panel: true, false
 * 		allowQuickFence 	- allow user to set Quickfence: true, false
 * 		defaultMapView		- default type of map display: street, hybrid
 * 		vehicleInstances	- number of "cloned" vehicles for each physical vehicle: used for demo only
 * 		numLocations		- number of historical locations to obtain in addition to the last known returned by GetVehicles()
 ************************************************************************************************************************************/
var userTypes = {
	Consumer : {
		number : 0,
		name : "Consumer",
		planType : "Advantage",
		behaviors : {
			default : "showMap",
			listVehicles : false,
			showVIN : false,
			allowQuickFence : true,
			defaultMapView : "street",
			markerType : "car",
			vehicleInstances : 1,
			numLocations : 10,
			speedThresholdStep : 5
		}
	},
	Repo : {
		number : 1,
		name : "Repo",
		planType : "",
		behaviors : {
			default : "vinPrompt",
			listVehicles : true,
			showVIN : true,
			allowQuickFence : false,
			defaultMapView : "street",
			markerType : "pin",
			vehicleInstances : 1,
			numLocations : 10,
			speedThresholdStep : 5
		}
	},
	Dealer : {
		number : 2,
		name : "Dealer",
		planType : "",
		behaviors : {
			default : "vinLocPrompt",
			listVehicles : true,
			showVIN : true,
			allowQuickFence : false,
			defaultMapView : "hybrid",
			markerType : "pin",
			vehicleInstances : 1,
			numLocations : 10,
			speedThresholdStep : 5
		}
	},
	Fleet : {
		number : 3,
		name : "Fleet",
		planType : "",
		behaviors : {
			default : "showMap",
			listVehicles : false,
			showVIN : false,
			allowQuickFence : false,
			defaultMapView : "street",
			markerType : "pin",
			vehicleInstances : 4,
			numLocations : 0,
			speedThresholdStep : 1
		}
	}
}

// GLOBAL VARIABLES-----------------------------------------------
var g_screenLayout = null; // Landscape or portrait mode
var g_scroll; // Global iScroll object
var g_priorvListTop; // Used to temporarily store position of vList (used to
						// restore it to the same place) TODO: look for a better
						// way to do this
var g_vListVisible = null; // Is vehicle list visible TODO: look for a better
							// way to do this
var g_map; // Global map object
var g_ieVersion; // Version of IE
var g_accountUser; // The user whose account is being viewed (usually the
					// actual user)
var g_userType = userTypes.Consumer; // User type - defaults to consumer
										// TODO: change to use UserType in the
										// user object

var vehicles = new Vehicles();

// INIT ----------------------------------------------------------
function testLogin() {
	g_ieVersion = getInternetExplorerVersion();

	setvListDimensions();
	postRequest(services.GetVehiclesByVINSuffix,
			'{"vinSuffix":"999999", "userType":' + g_userType.number + '}',
			function(json, textStatus, jq) {
				init();
			});
}

function init() {
	setTimeout(function() {
		init2();
	}, 0);
}

function init2() {
	// TODO: (HACK): Detect user type based on URL.
	// This is currently really an "application type" identifier rather than a
	// "user type" identifier.
	// Should really be based on actual user type in the database.
	if (window.location.toString().toLowerCase().indexOf("repo") > 0) {
		g_userType = userTypes.Repo;
	} else if (window.location.toString().toLowerCase().indexOf("findit") > 0) {
		g_userType = userTypes.Dealer;
	} else if (window.location.toString().toLowerCase().indexOf("fleet") > 0) {
		g_userType = userTypes.Fleet;
		// $("#logoImg").attr("src", "./images/fleet_logo.png");
	}

	// Workaround for lack of Android scrolling DIVs - employed for other
	// browsers for consistency
	g_scroll = new iScroll("vlistContainer", {
		hscroll : false
	});

	if (~navigator.userAgent.indexOf("Firefox")) {
		window.history.replaceState('Object', 'Title', '.');
	}

	// Populate current user info.
	var userName = getCookie("userName");
	var altUserName = getCookie("altUserName");

	if (altUserName == undefined) {
		$("#currentUser").html(userName);
	} else {
		$("#currentUser").html(userName + " as " + altUserName);
	}

	switch (g_userType.behaviors.default) {
	case "showMap" :
		showOnly("vehicleList");
		vehicles.getList();

		// TODO: Don't need this for mobile apps.
		g_accountUser = new User({
			"userType" : "currentUser"
		}, function() {
			$('#acctPanelLoading').fadeOut(125, function() {
				$('#bSettings').fadeIn(125);
			});
			$('#acctPanelLoading').hide();
			populateAcctPanel();
		});

		break;
	case "vinPrompt" :
		showOnly("vinEntry");
		break;
	case "vinLocPrompt" :
		showOnly("vinEntry");
		show("locationSelect");
		break;
	}

	// if (!~navigator.userAgent.indexOf("Android")) {
	$(window)
			.resize(
					function() {
						if ((g_screenLayout != null)
								&& (($(this).width() > minLandscapeWidth + 100) && (g_screenLayout != ScreenLayouts.landscape))
								|| (($(this).width() < minLandscapeWidth + 100) && (g_screenLayout != ScreenLayouts.portrait))) {
							vehicles.showList();
						}
						setvListDimensions();
					});
	// }

	g_map = new Map();

}

function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
	var rv = -1; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}
	return rv;
}

function showOnly(id) {
	hide("map");
	hide("vlistContainer");
	hide("help");
	hide("vinEntry");
	hide("locationSelect");
	hide("error");
	show(id);
}

function show(id) {
	if ((id == "btnHome") || (id == "btnHelp") || (id == "btnSettings")
			|| (id == "btnHistory") || (id == "btnAccount")
			|| (id.substring(0, 6) == "fence_")) {
		if ((id == "btnSettings") || (id == "btnHistory")
				|| (id == "btnAccount")) {
			document.getElementById(id).style.display = "inline";
		} else if ((id != "btnHome") || ($(window).width < 800)) {
			document.getElementById(id).style.visibility = "visible";
		}
	} else if (id.substring(0, 14) == "geoRadiusPanel") {
		$("#" + id).css({
			opacity : 1
		});
	} else {
		document.getElementById(id).style.display = "block";
	}

	if (id == "help") {
		hide("btnHelp"); // Help button should be hidden when help screen is
							// displayed
		g_vlistVisible = isVisible("vlistContainer");
		hide("vlistContainer");
	}

	// If we're displaying the map, we need to reorganize the screen a little
	if (id == "map") {
		document.getElementById("banner").style.opacity = "0.7";

		show("btnHome");

		g_priorvListTop = document.getElementById("vlistContainer").style.top;
		document.getElementById("vlistContainer").style.position = "absolute";
		document.getElementById("vlistContainer").style.top = "auto";
		document.getElementById("vlistContainer").style.left = "4px";
		document.getElementById("vlistContainer").style.bottom = "4px";
		document.getElementById("vlistContainer").style.opacity = "0.7";

		// We're only going to show the selected vehicle,
		// so hide all the separators.
		hideClass('separator');

		// Now hide all vehicles.
		hideClass('vehicle');
	}

	// If showing the vehicle list, we MAY need to hide the map controls.
	showOrHide("map_controls");

	if (id == "vehicleList") {
		setvListDimensions();
	}

	if ((id == "vlistContainer") && (!g_userType.behaviors.listVehicles)) {
		show("btnHome");
	}

	// If we are showing a vehicle and the map is displayed, add the refresh and
	// close icons.
	if (~id.indexOf("vehicle_") && isVisible("map")) {
		var vehicleNumber = id.substr(8, id.length - 7);
		show("refresh_" + vehicleNumber);
		show("close_" + vehicleNumber);
	}
}

function hide(id) {
	if ((id == "btnHome") || (id == "btnHelp")
			|| (id.substring(0, 6) == "fence_")) {
		document.getElementById(id).style.visibility = "hidden";
	} else if (id.substring(0, 14) == "geoRadiusPanel") {
		$("#" + id).css({
			opacity : 0
		});
	} else {
		document.getElementById(id).style.display = "none";
	}

	if (id == "help") {
		show("btnHelp"); // Help button should be visible when help screen is
							// not displayed.
	}

	if ((id == "vlistContainer") && (!g_userType.behaviors.listVehicles)) {
		hide("btnHome");
	}

	// If we're hiding the map, we need to reorganize the screen a little
	if (id == "map") {

		document.getElementById("banner").style.opacity = "1";

		document.getElementById("vlistContainer").style.position = "absolute";
		if (g_priorvListTop != null) {
			document.getElementById("vlistContainer").style.top = g_priorvListTop;
		}
		// document.getElementById("vlistCenter").style.left = "auto";
		document.getElementById("vlistContainer").style.opacity = "1";

		// We're only going to show the selected vehicle,
		// so show all the separators.
		showClass('separator');

		// Now hide all close icons.
		hideClass("closeButton");

		// Now hide all refresh icons.
		hideClass("refreshContainer");

		// Now show all vehicles.
		showClass('vehicle');

		// Ensure vehicle list is displayed
		show("vehicleList");

	}

	showOrHide("map_controls");
}

function showOrHide(id) {
	if (g_map != null) {
		g_map.showOrHide(id);
	}
}

function showClass(className) {
	var elements = new Array();
	elements = getElementsByClassName(className);
	for (i in elements) {
		(className == "refreshContainer" ? elements[i].style.visibility = "visible"
				: elements[i].style.display = "block");
	}
	setvListDimensions();
}

function hideClass(className) {
	var elements = new Array();
	elements = getElementsByClassName(className);
	for (i in elements) {
		(className == "sdrefreshContainer" ? elements[i].style.visibility = "hidden"
				: elements[i].style.display = "none");
	}
	setvListDimensions();
}

function isVisible(id) {
	return (((document.getElementById(id).style.display == "none") || (document
			.getElementById(id).style.visibility == "hidden")) ? false : true);
}

function setvListDimensions() {
	// Makes sure the list container is an appropriate height/width relative to
	// the visible list.
	// Called whenever the list is modified, for example:
	// - updating list of vehicles
	// - hiding vehicles
	// - showing vehicles
	// - updating the address
	// - resizing the screen

	$("#vlistContainer").css("maxHeight",
			($(window).height() - $("#banner").height() - 10) + "px");
	$("#vlistContainer").css("maxWidth", ($(window).width() - 4) + "px");
	$("#vlistContainer").css("left", "0px");
	if (!isVisible("map")) {
		$("#vlistContainer").css(
				"left",
				(($(window).width() / 2) - ($("#vlistContainer").width() / 2))
						+ "px");
	}

	if (g_scroll != null) {
		setTimeout(function() {
			g_scroll.refresh();
		}, 0);
	}

	// Fix for 0 width (seen in IE8)
	if ($("#vlistContainer").css("width") == "0px") {
		$("#vlistContainer").css(
				"width",
				(($(window).width() / 2) - ($("#vlistContainer").width() / 2))
						+ "px");
	}

	// Fix for 0 height on map page(seen in IE8)
	if (isVisible("map")) {
		// Force a height of 175px where size is zero
		if ($("#vlistContainer").css("height") == "0px") {
			$("#vlistContainer").css("height", "175px");
		}
	} else {
		// Revert back to default height
		$("#vlistContainer").css("height", "");
	}

	// IE8 workaround (i.e. use scrolling DIVs)
	if ((g_ieVersion > -1) && (g_ieVersion < 9.0)) {
		$("#vlistContainer").css("overflow", "auto");
	}
}

function getElementsByClassName(classname, node) {
	if (!node)
		node = document.getElementsByTagName("body")[0];
	var a = [];
	var re = new RegExp('\\b' + classname + '\\b');
	var els = node.getElementsByTagName("*");
	for ( var i = 0, j = els.length; i < j; i++)
		if (re.test(els[i].className))
			a.push(els[i]);
	return a;
}

function getDate(dateStr) {
	// Extract the #milliseconds in the epoch - this is actually UTC, but has
	// been encoded by the server as being
	// local to the server timezone (usually MST), meaning it is some number
	// (usually 7) hours off...
	var d = new Date(parseInt(dateStr.substr(6)));

	// ...so correct by the number of hours as reported by the server. Client
	// takes care of localizing for the user.
	ndx = dateStr.indexOf('-');
	return new Date(d - (1000 * 60 * 60 * (-parseInt(dateStr.substr(ndx, 3)))));
}

function formatDate(dateStr, type, time) {
	var month = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" ];

	var d = getDate(dateStr);

	var retVal = "";
	
	if (type="short") {
		retVal = (d.getMonth()+1) +"/"+ d.getDate() +"/" + (d.getFullYear() - 2000);	
	} else {
		retVal = d.getDate() + " " + month[d.getMonth()] + " \'" + (d.getFullYear() - 2000);
	}
	
	if (time) {
		var hours = d.getHours() % 12;
		var mins = d.getMinutes();
		retVal = retVal + " @ " + (hours == 0 ? 12 : hours) + ":" + (mins < 10 ? "0" : "") + mins + (d.getHours() < 12 ? "am" : "pm"); 
	}
	
	return retVal;
}

function formatDuration(durationStr) {
	durationStr = durationStr.replace(/PT/, "").replace(/H/, "h ").replace(/M/,
			"m ").replace(/S/, "s");
	if (durationStr.indexOf("m")) {
		durationStr = durationStr.replace(/m.*/, "m");
	} else if (durationStr.indexOf("h")) {
		durationStr = durationStr.replace(/h.*/, "h");
	}

	return (durationStr);
}

function formatSpeed(speed, units) {
	if ((units == null) || (units == "")) {
		units = "m"
	}
	; // Default to meters (i.e. no conversion)
	return (convertFromMeters(speed, units) + units);
}

function convertFromMeters(speed, units, places) {
	var convSpeed = 0;
	if (places == null) {
		var places = 0
	}
	;
	switch (units) {
	case "mph":
		convSpeed = Math
				.round(speed * (metersToMiles * (Math.pow(10, places))))
				/ (Math.pow(10, places));
		break;
	case "kph":
		convSpeed = Math.round(speed / 1000 * (10 ^ places)) / (10 ^ places);
		break;
	default:
		convSpeed = speed;
	}
	return convSpeed;
}

function convertToMeters(speed, units, places) {
	var convSpeed = 0;
	switch (units) {
	case "mph":
		convSpeed = Math
				.round(speed / (metersToMiles * (Math.pow(10, places))))
				/ (Math.pow(10, places));
		break;
	case "kph":
		convSpeed = Math.round(speed * 1000 * (10 ^ places)) / (10 ^ places);
		break;
	default:
		convSpeed = speed;
	}
	return convSpeed;
}

function help() {
	hide("vehicleList");
	hide("vinEntry");
	show("help");
	show("btnHome");
}

function back() {
	if (isVisible("help")) {
		hide("help");
	} else if (isVisible("map")) {
		hide("map"); // Hiding map canvas takes care of sorting out home and
						// back icon visibility.

		switch (g_userType.behaviors.listVehicles) {
		case true :
			if (vehicles.list.length < 2) {
				showOnly("vinEntry");
				if (g_userType == userTypes.Dealer) {
					show("locationSelect");
				}
				hide("btnHome")
			}
			break;
		case false :
			if ((g_vListVisible) || (!isVisible("map"))) {
				show("vlistContainer");
				show("vehicleList");
			}
			break;
		}
	} else {
		if (g_userType.behaviors.listVehicles) {
			showOnly("vinEntry");
			if (g_userType == userTypes.Dealer) {
				show("locationSelect");
			}
			hide("btnHome")
		}
	}

	if (!isVisible("map") && !g_userType.behaviors.listVehicles) {
		hide("btnHome");
	}
}

// This method shows friendly error messages.
function showError(title, message) {
	$.prompt("<div class='errorTitle'>"+title+"</div><div class='errorMessage'>"+message+"</div>");
}

var promoBox = null;
var promoDismissed = false;

function dismissPromo(){
	promoBox.hide();
	promoDismissed = true;
}

function showPromo() {

	if (promoDismissed)
		return;

	// Ensures a consistent promo is shown in all promo spots.
	// When adding a new promo spot, ensure it is referenced here and
	// that this method is called upon displaying the new promo spot.

	var v = vehicles.getCurrentVehicle();

	var promo = null;
	if (v.promoImageUrl != null && v.promoRedirectUrl != null) {
		promo = "<img class='promoImg' src='" + v.promoImageUrl + "' style='width:290px;cursor:pointer;' onclick=\"location.href='"
				+ v.promoRedirectUrl + "';\"/>";

	if (promoBox == null)
	{
		var promoElement = document.createElement("div");
		document.body.appendChild(promoElement);
		promoBox = $(promoElement);
		promoBox.css("position", "absolute");
		promoBox.css("bottom", "10px");
		promoBox.css("right", "10px");
		promoBox.css("padding", "10px");
		promoBox.css("z-index", "2147483647");
		promoBox.css("background", "#666 url('images/background4.jpg')");
		promoBox.css("border", "1px solid #666");
		promoBox.css("border-radius", "20px");
		promoBox.css("text-align", "right");
	}

	if (promo != null)
	{
		promoBox.html(promo + "<br/><a href='#' onClick='dismissPromo()'>Dismiss</a>");
	}
	else
	{
		promoBox.hide();
	}

/*	We are no longer going to show the promo where it was previously. Now we are just going to show it
	in the bottom right-hand corner of the screen.

		$("#ctrlPromo1").html(promo);
		$("#ctrlPromo2").html(promo);
		$("#ctrlPromo3").html(promo);
		$("#ctrlPromo4").html(promo);
		$("#ctrlPromo5").html(promo);

		$("#acctPromo1").html(promo);
		$("#acctPromo2").html(promo);
		$("#acctPromo3").html(promo);
		$("#acctPromo4").html(promo);

		$("#histPromo1").html(promo);
*/

		// If error rendering this node (e.g. promo image doesn't exist) then
		// kill it.
		// This takes care of "empty image" box displaying in IE.
		$(".promoImg").error(function() {
			this.parentNode.innerHTML = "";
		});

	}
}

function safeNavigate(baseUrl, page) {
	if (baseUrl.toLowerCase().substr(0, 4) != "http") {
		baseUrl = "http://" + baseUrl;
	}

	$.getJSON(
			"http://query.yahooapis.com/v1/public/yql?"
					+ "q=select%20*%20from%20html%20where%20url%3D%22"
					+ encodeURIComponent(baseUrl + page)
					+ "%22&format=xml'&callback=?",

			// this function checks to see if any data was returned

			// from the JSON-P call
			function(data) {
				// if there is data, [url+page] is a valid URL
				if (data.results[0]) {
					baseUrl = baseUrl + page;
				}
				window.open(baseUrl);
			});
}

function isBlank(str) {
	return (!str || /^\s*$/.test(str));
}

function isEmpty(str) {
	return (!str || 0 === str.length);
}

function insertMail(text, userName, domainName) {
	var EmailId;
	var atSign = "&#64;"
	var fullStop = "&#46";

	EmailId = userName;
	EmailId = "" + EmailId + atSign;
	EmailId = EmailId + domainName;

	document.write("<a href='mail" + "to:" + EmailId
			+ "&body=Debug information (please add your comments below): "
			+ navigator.userAgent + "'>" + text + "</A>");
}

function logout() {
	// Call logout page (to clear server-side session)
	window.location.replace("./logout.aspx");
}

function getCookie(c_name) {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == c_name) {
			return unescape(y);
		}
	}
}
