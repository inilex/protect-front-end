//CART
function Cart(mode) {

	var cardTypes = {
		VISA : {
			number : 1,
			name : "VISA"
		},
		MasterCard : {
			number : 3,
			name : "MasterCard"
		},
		AmEx : {
			number : 2,
			name : "AmEx"
		}
	}

	this.mode = (mode == undefined ? 0 : mode);

	this.products = null;
	this.selectedProduct = null;
	this.couponCode = "";
	this.couponProducts = null;
	this.total = null;

	this.init = init;
	this.getProductOfferings = getProductOfferings;
	this.populateCart = populateCart;
	this.getCouponOfferings = getCouponOfferings;
	this.populateCouponCart = populateCouponCart;
	this.selectProduct = selectProduct;
	this.initCheckout = initCheckout;
	this.submitCart = submitCart;
	this.submitBillingInfo = submitBillingInfo;
	this.submitOrder = submitOrder;
	this.updateCCInfo = updateCCInfo;
	this.billingInfo = billingInfo;
	this.populateReceipt = populateReceipt;
	this.isValid = isValid;
	this.isValidCard = isValidCard;
	this.isValidCardNumber = isValidCardNumber;

	this.init();

	function init() {
		hideAcctPanel();

		$("#cartCheckoutPanel").hide();
		$("#cartSuccessPanel").hide();
		$("#cartFailPanel").hide();
		$("#cartPromoPanel").hide();
		$("#couponTd").hide();

		$('#cardError').html("");
		$("#btn_cartPanelSubmit").hide();
		$("#btn_cartSubmit").show(); // We'll only really see this when the
										// panel is displayed.

		switch (this.mode) {
		case 0: // Purchasing Service
			var _this = this;
			this.getProductOfferings(function(response) {
				_this.populateCart(response);
				$("#btn_cartPanelSubmit").show();
			});

			g_accountUser.getCCInfo(function() {
				if (g_accountUser.ccInfo.number != null) {
					populateCCInfo();
				}
			});

			if (!isCtrlPanelOpen()) {
				showCtrlPanel("cartPanel");
			} else {
				$('#vSettings').fadeOut(125, function() {
					$('#cartPanel').fadeIn(125);
				})
			}
			break;
		case 1: // Update payment info
			g_accountUser.getCCInfo(function() {
				if (g_accountUser.ccInfo.number != null) {
					populateCCInfo();
				}
				$('#cartCheckoutLoading').fadeOut(125, function() {
					$('#cartCheckoutPanel').fadeIn(125);
				})
			});

			if (!isCtrlPanelOpen()) {
				showCtrlPanel("cartCheckoutLoading");
			} else {
				$('#vSettings').fadeOut(125, function() {
					$('#cartCheckoutLoading').fadeIn(125);
				})
			}
			break;
		}
	}

	function getProductOfferings(success) {
		var cartTable = $("#cartTable")[0];

		// Delete all but the top row and the bottom 5 rows.
		while (cartTable.tBodies[0].rows.length > 5) {
			cartTable.deleteRow(1);
		}

		// Show "Loading" indicator
		var row = cartTable.insertRow(1);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		cell2.innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:5px; margin-bottom:5px'/>";
		cell2.style.textAlign = "center";
		var cell3 = row.insertCell(2);

		// Request plans that are valid for this device
		postRequest(services.GetProductOfferings, '{"vin":"'
				+ vehicles.getCurrentVehicle().vin + '"}', function(json,
				textStatus, jq) {
			success(json.d);
		});
	}

	function populateCart(products) {

		var hideCouponTd = false;

		var cartTable = $("#cartTable")[0];

		// Delete all but the top row and the bottom 5 rows.
		while (cartTable.tBodies[0].rows.length > 5) {
			cartTable.deleteRow(1);
		}

		for ( var g in products.ProductGroups) {
			var group = products.ProductGroups[g];
			if (group.GroupName == "Subscriptions") {
				this.products = group.LineItems;
				for ( var p in this.products) {
					var row = cartTable.insertRow(parseInt(p) + 1);

					var prodDescCell = row.insertCell(0);
					//hackickity hack, sucking out Service Plan so it will all fit.
					prodDescCell.innerHTML = this.products[p].Description.replace("Advantage", "Protect").replace(" Service Plan","")
							+ (~this.products[p].Description
									.indexOf("ETD Upgrade") ? " (expires "
									+ vehicles.getCurrentVehicle().plans
											.getPlanExpiration("ETD") + ")"
									: "");
					prodDescCell.style.textAlign = "left";

					var prodPriceCell = row.insertCell(1);
					//dual use coupon (Van Tuyl hack)
					if (this.products[p].IsDiscountItem) {
						hideCouponTd = true;
						var priceLine = "$"
								+ this.products[p].Price.toFixed(2) + " - "
								+ (this.products[p].IsAbsolute ? this.products[p].Discount : ((this.products[p].Discount / this.products[p].Price) * 100)) +
								(this.products[p].IsAbsolute ? "$ Off - " : "% Off - ") +
								"$" + this.products[p].FinalPrice.toFixed(2);
						prodPriceCell.innerHTML = priceLine;
						prodPriceCell.style.textAlign = "right";
						prodPriceCell.style.width = "215px";
					} else {
						prodPriceCell.innerHTML = "$"
								+ this.products[p].Price.toFixed(2);
						prodPriceCell.style.textAlign = "right";
						prodPriceCell.style.width = "80px";
					}
					var prodSelectCell = row.insertCell(2);
					prodSelectCell.innerHTML = "<input type='radio'"
							+ (this.products.length == 1 ? " checked='checked'"
									: "")
							+ " name='planSelect' style='width:60px;' value='"
							+ p + "' onclick='cart.selectProduct(this)'/>";
					// hack hide the td with the coupon entry, submit for discount users.
					if(!hideCouponTd)
					{
						$("#couponTd").show();
					} else {
						$("#couponTd").hide();
					}
				}
			}
		}
	}

	function getCouponOfferings(couponCode) {
		// This method is very dumb - it assumes only one product was returned.
		this.couponCode = couponCode;
		var _this = this;

		// Request plans that this coupon applies to and are valid for this
		// device
		postRequest(services.GetCouponOfferings, '{"vin":"'
				+ vehicles.getCurrentVehicle().vin + '", "couponCode":"'
				+ this.couponCode + '"}', function(json, textStatus, jq) {
			_this.populateCouponCart(json.d);
			$("#btn_cartPanelSubmit").removeAttr("disabled"); // Since this coupon is good, ensure submit is enabled.
			$("#cartError").html(""); 						  // Also, ensure we are not telling the user they have to pick a product.
		});
	}

	function populateCouponCart(products) {
		// This method is very dumb - it assumes only one product was returned
		// and that it is free.
		this.couponProducts = products.ProductGroups[0].LineItems;

		$("#couponDetail").html(
				"<h3>This coupon entitles you to:</h3>"
						+ this.couponProducts[0].Description);

		$("#cartProductMessage")
				.html(
						"<h3>If you would like to purchase additional service, please select it below:</h3>");
	}

	function selectProduct(e) {
		this.selectedProduct = e.value;
	}

	function initCheckout() {
		// First, make sure either that a coupon hasn't been entered, or if it has that it has a product associated with it in this order.
		if ((this.couponCode == "") || (this.couponProducts != null)) {
			var _this = this;
			if ((this.selectedProduct != null) || (this.couponProducts != null)) {
				this.submitCart(function(response) {
					_this.total = response.Total;
					if (_this.total > 0) {
						// If money is owed, then request payment information.
						$('#cartPanel').fadeOut(125, function() {
							$('#cartCheckoutPanel').fadeIn(125);
						})
					} else {
						_this.submitBillingInfo();

						// Otherwise, thank the customer for their purchase.
//						$('#cartPanel').fadeOut(125, function() {
//						showPurchaseSuccess();
//						})
					}
				});
			} else {
				$('#cartError').html("<br/>Please select a product to purchase.");
			}
		} else {
			showError("Coupon not applied.","The coupon code you entered has not been applied against this transaction.  Please correct the coupon code if necessary and be sure to click the \"Apply\" button.");
		}
	}

	function showPurchaseSuccess() {
		showCartSuccess("Buy Service",
				"Your order has completed successfully.",
				"Thank you for your purchase.");
	}

	function showUpdateCCSuccess() {
		showCartSuccess("Update Payment Info",
				"Payment information successfully updated.", "Thank you.");
	}

	function showCartSuccess(title, successMsg, thankYouMsg) {
		$('#cartSuccessTitle').html(title);
		$('#cartSuccessMsg').html(successMsg);
		$('#cartThankYouMsg').html(thankYouMsg);
		$('#cartSuccessPanel').fadeIn(125);
	}

	function submitCart(success) {
		// COupons are not implemented on the server side, so not including the
		// following in the call:
		// '"Coupon":"'+$("#couponCode").val()+'",' +

		/*
		 * postRequest(services.SubmitCart, '{"cart":{'+
		 * '"VIN":"'+vehicles.getCurrentVehicle().vin+'",' + '"Coupon":' +
		 * (this.couponCode != null ? '"' + this.couponCode + '"': null) + ',' +
		 * '"Items":[' + (this.selectedProduct != null ?
		 * JSON.stringify(this.products[this.selectedProduct]) : "") +
		 * (this.couponProducts != null ? (this.selectedProduct != null ? ',' :
		 * '') + JSON.stringify(this.couponProducts[0]) : '') + ']}}',
		 * function(json, textStatus, jq) { success(json.d); });
		 */
		// this.couponProducts[0].ID = 2;
		postRequest(
				services.SubmitCart,
				'{"cart":{'
						+ '"VIN":"'
						+ vehicles.getCurrentVehicle().vin
						+ '",'
						+ '"Coupon":'
						+ (this.couponCode != "" ? '"' + this.couponCode
								+ '"' : null)
						+ ','
						+ '"Items":['
						+ (this.selectedProduct != null ? JSON
								.stringify(this.products[this.selectedProduct])
								: "")
						+ (this.couponProducts != null ? (this.selectedProduct != null ? ','
								: '')
								+ JSON.stringify(this.couponProducts[0])
								: '') + ']}}', function(json, textStatus, jq) {
					success(json.d);
				});
	}

	function submitBillingInfo() {
		switch (this.mode) {
		case 0: // Straight purchase
			this.submitOrder();
			break;
		case 1: // Update CC info
			this.updateCCInfo();
			break;
		}
	}

	function submitOrder() {
		if (this.isValid() || this.total==0) {
			var _this = this;
			$('#cartError').html("Submitting order - please wait...");
			$('#cardError').html("Submitting order - please wait...");
			$("#btn_cartSubmit").hide();

			postRequest(
					services.SubmitOrder,
					this.billingInfo("purchase"),
					function(json, textStatus, jq) {
						$('#cartPanel').fadeOut(125);
						$('#cartCheckoutPanel').fadeOut(125, function() {
							showPurchaseSuccess();
						})						

						_this.populateReceipt(json.d.Items);
					},
					function(json, textStatus, jq) {
						$('#cardError')
								.html(
										"Your order could not complete.<br>Please check the billing information and try again.");
						$('#cartError').html("Your order could not complete at this time.<br>Please try again later.");
					});
		}
	}

	function updateCCInfo() {
		if (!$("#chk_storeCC").prop("checked") || this.isValid()) {
			var _this = this;
			$('#cardError').html("Updating billing info - please wait...");
			$("#btn_cartSubmit").hide();

			postRequest(
					services.UpdateCCInfo,
					this.billingInfo("billingInfo"),
					function(json, textStatus, jq) {
						g_accountUser.autoRenew = $("#chk_autoRenew").prop(
								"checked");
						$('#cartCheckoutPanel').fadeOut(125, function() {
							showUpdateCCSuccess();
						})
					},
					function(json, textStatus, jq) {
						$('#cardError')
								.html(
										"We could not update your billing information at this time.<br>Please check the information and try again.");
					});
		}
	}

	function billingInfo(type) {

		var ccExpDate = new Date(); // Supply a default date
		if (($("#cardExpirationMonth").val() > 0) && ($("#cardExpirationMonth").val() > 0)
) {

			var ccExpDate = new Date($("#cardExpirationMonth").val() + "/01/20"
					+ $("#cardExpirationYear").val());
			ccExpDate.setMonth(ccExpDate.getMonth() + 1);

		}


		return '{"' + type + '":{' + '"CCExpirationDate":"\\/Date('
				+ ccExpDate.getTime() + '+00:00)\\/",' + '"CCNumber":"'
				+ $("#cardNumber").val() + '",' + '"CardType":"'
				+ $("#cardType").val() + '",' + '"FirstName":"'
				+ $("#cardFirstName").val() + '",' + '"LastName":"'
				+ $("#cardLastName").val() + '",' + '"StreetAddress":"'
				+ $("#cardStreetAddress").val() + '",' + '"SuiteApt":"'
				+ $("#cardSuiteApt").val() + '",' + '"City":"'
				+ $("#cardCity").val() + '",' + '"State":"'
				+ $("#cardState").val() + '",' + '"Zip":"'
				+ $("#cardZip").val() + '",' + '"SecurityCode":"",'
				+ '"StoreCC":' + $("#chk_storeCC").prop("checked") + ','
				+ '"AutoRenew":' + $("#chk_autoRenew").prop("checked") + '}}';
	}

	function populateReceipt(items) {
		var cartSuccessTable = $("#cartSuccessTable")[0];
		for ( var i in items) {
			var row = cartSuccessTable.insertRow(parseInt(i) + 2);

			var prodDescCell = row.insertCell(0);
			prodDescCell.innerHTML = items[i].Description;
			prodDescCell.style.textAlign = "left";

			var prodPriceCell = row.insertCell(1);
			prodPriceCell.innerHTML = "$" + items[i].Price.toFixed(2);
			prodPriceCell.style.textAlign = "right";
			prodPriceCell.style.width = "80px";

			if(items[i].Discount!==0)
			{
				//hack for dual use coupons
				//show discount
				var discountRow = cartSuccessTable.insertRow(parseInt(i) + 3);
				var discountDescCell = discountRow.insertCell(0);
				discountDescCell.innerHTML = "Discount";
				discountDescCell.style.textAlign = "left";

				var discountAmountCell = discountRow.insertCell(1);
				discountAmountCell.innerHTML = "($" + items[i].Discount.toFixed(2) + ")";
				discountAmountCell.style.textAlign = "right";
				discountAmountCell.style.width = "80px";
				

				// hack for dual use coupons
				// show total
				var totalRow = cartSuccessTable.insertRow(parseInt(i) + 4);
				var totalDescriptionCell = totalRow.insertCell(0);
				totalDescriptionCell.innerHTML = "Total:";
				totalDescriptionCell.style.textAlign = "right";

				var totalAmountCell = totalRow.insertCell(1);
				totalAmountCell.innerHTML = "$" + items[i].FinalPrice.toFixed(2);
				totalAmountCell.style.textAlign = "right";
				totalAmountCell.style.width = "80px";
			}
		}
	}

	function isValid() {
		var errStr = "";

		if (($("#cardFirstName").val() == "")
				|| ($("#cardLastName").val() == "")) {
			errStr = errStr + "Cardholder's full name is required.<br/>";
		}

		if (($("#cardStreetAddress").val() == "")) {
			errStr = errStr + "Street address is required.</br>";
		}

		if (($("#cardCity").val() == "")) {
			errStr = errStr + "City is required.</br>";
		}

		if (($("#cardState").val() == "")) {
			errStr = errStr + "State is required.</br>";
		}

		if (($("#cardZip").val() == "")) {
			errStr = errStr + "ZIP code is required.</br>";
		}

		if ((($("#cardExpirationMonth").val() < 1) || ($("#cardExpirationMonth")
				.val() > 12))) {
			errStr = errStr
					+ "Month is invalid.  Please enter a valid month.</br>";
		}

		if ((parseInt("20" + $("#cardExpirationYear").val()) < new Date()
				.getFullYear())
				|| (parseInt($("#cardExpirationYear").val()) > 99)) {
			errStr = errStr
					+ "Year is invalid.  Please enter a valid year.</br>";
		}

		errStr = errStr + this.isValidCard($("#cardNumber").val());

		if (errStr != "") {
			errStr = "<b>Please correct the following entries:</b></br>"
					+ errStr;
			$("#cardError").html(errStr);
			return false;
		}

		return true;
	}

	function isValidCardNumber(cardNumber) {
		var ccard = new Array(cardNumber.length);
		var i = 0;
		var sum = 0;

		// 6 digit is issuer identifier
		// 1 last digit is check digit
		// most card number > 11 digit
		if (cardNumber.length < 11) {
			return false;
		}
		// Init Array with Credit Card Number
		for (i = 0; i < cardNumber.length; i++) {
			ccard[i] = parseInt(cardNumber.charAt(i));
		}
		// Run step 1-5 above above
		for (i = cardNumber.length - 2; i >= 0; i = i - 2) {
			ccard[i] = ccard[i] * 2;
			if (ccard[i] > 9)
				ccard[i] = ccard[i] - 9;
		}
		for (i = 0; i < cardNumber.length; i++) {
			sum = sum + ccard[i];
		}
		if ((sum % 10) == 0) {
			return "";
		} else {
			return "Card number is invalid.";
		}
	}

	function isValidCard(cardNumber) {
		// Check that card type matches the entered number.
		switch (parseInt($("#cardType").val())) {
		case cardTypes.VISA.number:
			// VISA cards are 13 or 16 digits long and start with 4.
			if (cardNumber.charAt(0) == '4'
					&& (cardNumber.length == 13 || cardNumber.length == 16)) {
				return this.isValidCardNumber(cardNumber);
			}
			break;
		case cardTypes.MasterCard.number:
			// MasterCards are 16 digits long and start with 50, 51, 52, 53, 54
			// or 55
			if (cardNumber.charAt(0) == '5'
					&& ((parseInt(cardNumber.charAt(1)) >= 0) && (parseInt(cardNumber
							.charAt(1)) <= 5)) && cardNumber.length == 16) {
				return this.isValidCardNumber(cardNumber);
			}
			break;
		case cardTypes.AmEx.number:
			// Amex cards are 16 digits long and start with 33, 34 or 37
			if (cardNumber.charAt(0) == '3'
					&& (cardNumber.charAt(1) == '3'
							|| cardNumber.charAt(1) == '4' || cardNumber
							.charAt(1) == '7') && cardNumber.length == 15) {
				return this.isValidCardNumber(cardNumber);
			}
			break;
		}
		return "Card number is invalid for the selected card type.";
	}
}