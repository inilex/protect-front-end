function toggleHistoryPanel() {
	if (isHistoryPanelOpen()) {
		hideHistoryPanel();
	} else {
		hideCtrlPanel();
		hideAcctPanel();
		showHistoryPanel();
	}
}

function isHistoryPanelOpen() {
	return ($("#historyPanel").css("width") == "880px");
}

function adjustHistoryPanelPosition() {
	$("#historyPanel").css("top", ($("#vSelector").height() + 5) + "px");
	$("#menuIndicator").css("top", ($("#vSelector").height() - 20) + "px");
}

function refreshHistoryPanel() {
	if (isHistoryPanelOpen()) {
		adjustHistoryPanelPosition();
		initHistoryPanel();
		switch ($("#tabs").tabs('option', 'selected')) {
		case 0:
			// Events Tab is currently selected
			$("#tab1").click();
			break;
		case 1:
			// Trips Tab is currently selected
			$("#tab2").click();
			break;
		}
	}
}

function showHistoryPanel() {
	$("#btnCtrlPanel").attr("src", "./images/btn_back.png");
	adjustHistoryPanelPosition();
	$("#historyPanel").show();
	initHistoryPanel();
	$("#historyPanel").animate({
		width : "880px",
		height : $("#history").height(),
		opacity : "0.85"
	}, 250, function() {
		$("#tab1").click();
	});
	$("#menuIndicator").css("left", "45px").show();
}

function hideHistoryPanel() {
	$("#btnCtrlPanel").attr("src", "./images/btn_ctrlPanelOpen.png");
	$("#historyPanel").animate({
		width : "0px",
		height : "0px",
		opacity : "0.0"
	}, 250, "linear", function() {
		$("#historyPanel").hide();
	});
	$("#menuIndicator").hide();

	// Show the address bar page indicator on the vehicle detail panel.
	// NOTE: No need to wait for a callback when showing, since we won't be
	// changing the address that is currently displayed.
	if (vehicles.list.length > 0) {
		$("#pageIndicator_" + vehicles.currentVehicleNumber()).show();
	}

	// Ensure we don't leave any stray event markers laying around.
	v.clearAllMarkers();
}

function initHistoryPanel() {
	// Remove stray
	v = vehicles.getCurrentVehicle();
	v.clearAllMarkers();
	v.locationNumber = 0;

	// Hide the address bar page indicator on the vehicle detail panel.
	// NOTE: We need to wait for a callbackwo that we hide the address bar AFTER
	// the most recent location address is displayed in the infowindow.
	var listener = google.maps.event.addListener(g_infowindow, "domready",
			function() {
				if (vehicles.list.length > 0) {
					$("#pageIndicator_" + vehicles.currentVehicleNumber())
							.hide();
				}
				google.maps.event.removeListener(listener);
			});

	v.showAddress(0);

	$("#pageIndicator_" + vehicles.currentVehicleNumber()).attr("class",
			"hidden");

	$("#tabs").tabs();

	$("#chk_emailSummaries")
			.prop(
					"checked",
					(g_accountUser.emailNotifications & ConsumerNotifications.HistorySummary.value) > 0);
}

function showLoading(table) {
	// TODO: IE misbehaves on this, so for now just avoid this "loading"
	// notification in IE.
	if (g_ieVersion > 0) {
		return
	}
	;

	var numColumns = table.dataTable().fnSettings().nTHead.rows[0].children.length;

	var a = [];
	for ( var i = 0; i < numColumns; i++) {
		a.push("");
	}

	var eventArray = [];
	eventArray.push(a);

	table.dataTable().fnClearTable();
	table.dataTable().fnAddData(eventArray);
	table.dataTable().fnSettings().nTBody.rows[0].innerHTML = "<tr><td colspan="
			+ numColumns
			+ " align='center'><img src='images/loading.gif' id='aloading' alt='Loading...'/></td></tr>";
}

function getEvents() {
	showLoading($("#eventTable"));

	// We have to get rid of any we may previously have plotted - since we don't
	// know which the last one was, remove all of them.
	vehicles.getCurrentVehicle().clearAllMarkers();

	// Now get events and populate them.
	vehicles.getFilteredEvents(vehicles.getCurrentVehicle().vin, 100,
			'"locationAlertTypes":[]', $("#eventsStartDate").val(), $(
					"#eventsEndDate").val(), populateEvents);
}

function getSpeedAlerts() {
	showLoading($("#eventTable"));
	vehicles.getFilteredEvents(vehicles.getCurrentVehicle().vin, 100,
			'"locationAlertTypes":["Speed"]', $("#eventsStartDate").val(), $(
					"#eventsEndDate").val(), populateEvents);
}

function getZoneAlerts() {
	showLoading($("#eventTable"));
	vehicles.getFilteredEvents(vehicles.getCurrentVehicle().vin, 100,
			'"locationAlertTypes":["EnterGeo","ExitGeo"]',
			$("#eventsStartDate").val(), $("#eventsEndDate").val(),
			populateEvents);
}

function populateEvents(eventList) {
	var v = vehicles.getCurrentVehicle();

	if (eventList.eventList.length > 0) {
		// Clear the historical event list for the current vehicle and populate
		// it with the events just received.
		v.events.init();
		v.events.addList(eventList.eventList[0].Events);

		var eventArray = [];
		for ( var i = 0; i < eventList.eventList[0].Events.length; i++) {

			var e = eventList.eventList[0].Events[i];
			var address = (e.StreetAddress != "" ? e.StreetAddress + "," : "")
					+ (e.City != "" ? e.City + ", " : "")
					+ (e.State != "" ? e.State + " " : "");

			// TODO: Tidy up enumeration of alerttypes
			/*
			 * {number:0, name:"Location", icon:"locations"}, {number:1,
			 * name:"Ignition On", icon:"alertIgnOn"}, {number:2, name:"Ignition
			 * Off", icon:"alertIgnOff"}, {number:3, name:"Entered Geofence",
			 * icon:"alertGeofence"}, {number:4, name:"Left Geofence",
			 * icon:"alertGeofence"}, {number:5, name:"Speed Threshold
			 * Exceeded", icon:"alertSpeed"}, {number:6, name:"Low Battery
			 * Level", icon:"alertBattery"}, {number:7, name:"Early Theft
			 * Detection", icon:"alertQuickfence"}, {number:8, name:"Power
			 * Disconnected", icon:"alertPowerOff"}
			 */

			// Add detail for geofence & speed violations (i.e. which fence /
			// what speed).
			var eventType = "";
			switch (e.LocationAlarmType) {
			case 3:
			case 4:
				eventType = alertTypes[e.LocationAlarmType].name + " "
						+ (e.LocationAlarmIndex + 1); // Add one, since the
														// value is zero-based
				break;
			case 5:
				eventType = alertTypes[e.LocationAlarmType].name + " ("
						+ convertFromMeters(e.Speed * 1000, "mph", 0) + "mph)";
				break;
			default:
				eventType = alertTypes[e.LocationAlarmType].name;
			}

			var a = [ e.TimeStamp, eventType, address ];
			eventArray.push(a);
		}
	}
	$("#eventTable").dataTable().fnClearTable();
	$("#eventTable").dataTable().fnAddData(eventArray);

	// We repopulated our rows, so add click handlers to them.
	$("#eventTable tbody tr").click(
			function(e) {
				// We have to get rid of any we may previously have plotted -
				// since we don't know which the last one was, remove all of
				// them.
				v.clearAllMarkers();

				v.events.list[$("#eventTable").dataTable().fnGetPosition(this)]
						.plot(true);
				if ($(this).hasClass("row_selected")) {
					$(this).removeClass("row_selected");
				} else {
					$("#eventTable").dataTable().$("tr.row_selected")
							.removeClass("row_selected");
					$(this).addClass("row_selected");
				}
			});

	// Because we hid our table, we need to go through a resize to make sure
	// everything (especially our buttons!) are sized and work properly.
	resizeTable($('#eventTable'));
}

function resizeTable(jqTable) {
	if (jqTable.length > 0) {
		var oTableTools = TableTools.fnGetInstance(jqTable[0]);
		if (oTableTools != null && oTableTools.fnResizeRequired()) {
			/*
			 * A resize of TableTools' buttons and DataTables' columns is only
			 * required on the first visible draw of the table
			 */
			jqTable.dataTable().fnAdjustColumnSizing();
			oTableTools.fnResizeButtons();
		}
	}
}

function getTrips() {
	showLoading($("#tripTable"));

	// We have to get rid of any we may previously have plotted - since we don't
	// know which the last one was, remove all of them.
	vehicles.getCurrentVehicle().clearAllMarkers();

	// Now get trips and populate them.
	vehicles.getCurrentVehicle().trips.getTripsSummaries($("#tripsStartDate")
			.val(), $("#tripsEndDate").val(), 100);
}

function populateTrips() {
	var tripArray = [];

	var v = vehicles.getCurrentVehicle();

	for ( var i = 0; i < v.trips.list.length; i++) {
		var t = v.trips.list[i];
		var a = [ t.start.rawTime, t.start.address, t.end.rawTime,
				t.end.address, t.distance, t.maxSpeed,
				formatDuration(t.duration) ];
		tripArray.push(a);
	}
	$("#tripTable").dataTable().fnClearTable();
	$("#tripTable").dataTable().fnAddData(tripArray);

	// We repopulated our rows, so add click handlers to them.
	$("#tripTable tbody tr").click(
			function(e) {
				// We have to get rid of any we may previously have plotted -
				// since we don't know which the last one was, remove all of
				// them.
				v.clearAllMarkers();
				v.trips.list[$("#tripTable").dataTable().fnGetPosition(this)]
						.plot(true);
				if ($(this).hasClass("row_selected")) {
					$(this).removeClass("row_selected");
				} else {
					$("#tripTable").dataTable().$("tr.row_selected")
							.removeClass("row_selected");
					$(this).addClass("row_selected");
				}
			});

	// Because we hid our table, we need to go through a resize to make sure
	// everything (especially our buttons!) are sized and work properly.
	resizeTable($('#tripTable'));
}

function updateSummaries() {
	g_accountUser.setNotifications(ConsumerNotifications.HistorySummary.value, $(
			"#chk_emailSummaries").is(":checked"), false);
}