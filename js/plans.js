//PLANS ----------------------------------------------------------
function Plans(parent, plans) {
  this.parent = parent;
  this.plans = plans;
  
  this.getPlanExpiration = getPlanExpiration;
  
  this.protectPlanIsCurrent = isPlanCurrent(plans, "Advantage");
  this.etdPlanIsCurrent = isPlanCurrent(plans, "ETD");
  this.motoPlanIsCurrent = isPlanCurrent(plans, "Moto");

	function isPlanCurrent(plans, planID) {
		var planValidity = false;
		
		switch (g_userType) {
		case userTypes.Consumer:
			// Test if this vehicle has a current plan containing the planID string in its name.
			var now = new Date().getTime();
			var expirationDate;
	
			for (var p in plans) {
				var d = new Date(parseInt(plans[p].ExpirationDate.substr(6)));
				d = new Date(d-(1000*60*d.getTimezoneOffset()));
				expirationDate = d.getTime();
	
				if ((~plans[p].Name.indexOf(planID)) && (now < expirationDate))
				{
					planValidity = true;
					break;
				}
			}
			break;
		case userTypes.Repo:
		case userTypes.Dealer:
		case userTypes.Fleet:			// TBD: This will probably need to change when we have plans established for fleet customers.
		  planValidity = true;
		  break;
		}

		return planValidity;
	}
	
	function getPlanExpiration() {
		var retVal = null;

		var now = new Date().getTime();
		var expirationDate;
	
		for (var p in this.plans) {
			var d = new Date(parseInt(plans[p].ExpirationDate.substr(6)));
			d = new Date(d-(1000*60*d.getTimezoneOffset()));
			expirationDate = d.getTime();

			if ((~plans[p].Name.indexOf("ETD")) && (now < expirationDate))
			{
				retVal = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
				break;
			}
		}
		
		return retVal;
	}
}
