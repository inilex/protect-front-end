//GEOFENCE ----------------------------------------------------------
function Geofence(g,parent) {
	// Public properties
	this.parent = parent;
	this.id = g.Index;
	this.myLatlng = new google.maps.LatLng(g.Center.Lat, g.Center.Lon);
	this.radius = g.Radius;
	this.geometry = g.Geometry;
	this.triggerType = g.TriggerType;
	this.name = g.Name;
	this.description = g.Description;
	this.isEnabled = g.IsEnabled;
	this.changed = false;

	// Default fence indicates whether this is a fence we have constructed since 
	// it has not yet been defined, or whether it was defined by the user.
	(g.DefaultFence == undefined ? this.defaultFence = false : this.defaultFence = g.DefaultFence);

	this.map = g_map.map;
	this.distanceWidget = null;

	this.address = "[Obtaining address...]";

	this.getAddress= getAddress;
	this.setFocus = setFocus;
	this.showFence = showFence;
	this.hideFence = hideFence;
	this.moveToAddress = moveToAddress;
	this.moveToPoint = moveToPoint;
	this.setRadius = setRadius;
	this.getTitle = getTitle;
	this.setTitle = setTitle;
	this.setName = setName;
	this.setEnabled = setEnabled;
	this.setTriggerType = setTriggerType;

	//TODO: Ensure that this.changed is properly set on changing name, etc.
	
	function getAddress() {
		var _this = this;
		//TODO: Remove this return to have addresses geocoded
		//return;
		this.parent.geocoder.geocode({location:this.myLatlng}, function (result, status) { 
			_this.address = result[0].formatted_address; 
			if (_this.isEnabled) {
				$("#geoAddress"+_this.id).val(_this.address);

				// If we have a distanceWidget, we are being displayed - so update 
				// the widget title with the new address.
				if (_this.distanceWidget != null) {
					_this.showFence();
				}
			}
		});
	}

	function setFocus () {
		if (this.id == 0) {
			show("geoWrapper0");
		} else {
			hide("geoWrapper0")
		}
		
		if (this.id == 1) {
			show("geoWrapper1");
		} else {
			hide("geoWrapper1")
		}
		
		if (this.id == 2) {
			show("geoWrapper2");
		} else {
			hide("geoWrapper2")
		}
		
		// If we are not currently showing the fence, then do so now.
		if ((this.distanceWidget==null) || (this.distanceWidget.get('map')==null)) {
			this.showFence();
		}
	}
	
	function showFence() {		
		if (this.distanceWidget==null) {
			var _this = this;
			this.isEnabled = true;
			$("#geoName"+_this.id).val(_this.name);
			$("#geoAddress"+_this.id).val(_this.address);
			$("#geoRadius"+_this.id).val(convertFromMeters(_this.radius,'mph',2));
			$('#geoRadiusSlider'+_this.id).slider({value: convertFromMeters(_this.radius,'mph',2)});
			
			this.distanceWidget = new DistanceWidget(this);

			google.maps.event.addListener(this.distanceWidget, 'distance_changed', function() {
				var widgetRadius = parseInt(this.get("distance"));
				if (_this.radius != widgetRadius) {
					_this.radius = widgetRadius;
					_this.changed = true;
				}
				$("#geoRadius"+_this.id).val(convertFromMeters(_this.radius,'mph',2));
				$('#geoRadiusSlider'+_this.id).slider({value: convertFromMeters(_this.radius,'mph',2)});
				
				// Modified the graphical representation of the geofence, so give that geofence focus
				_this.setFocus();
			});

			google.maps.event.addListener(this.distanceWidget, 'dragend', function() {
				_this.moveToPoint(this.get('position'));

				// Modified the graphical representation of the geofence, so give that geofence focus
				_this.setFocus();
			});

			google.maps.event.addListener(this.distanceWidget.radiusWidget, 'click', function() {
				// Modified the graphical representation of the geofence, so give that geofence focus
				_this.setFocus();
			});

		} else {
			this.distanceWidget.set('map',this.map);
		}

		this.setTitle();
		this.map.setCenter(this.myLatlng);
		
		show("geoRadiusPanel"+this.id); 
		show("geoOnOff"+this.id); 
	}

	function hideFence() {		
		if (this.distanceWidget!=null) {
			this.distanceWidget.set('map', null);
		}
	}

	function moveToAddress(address) {
		this.changed = true;
		var _this = this;
		this.parent.geocoder.geocode({address:address}, function (result, status) { 
			_this.myLatlng = result[0].geometry.location;
			_this.distanceWidget.set('position',_this.myLatlng);
			_this.getAddress();
		});
	}

	function moveToPoint(latLng) {
		this.changed = true;
		this.myLatlng = latLng;
		this.getAddress();
	}

	function setRadius(radius) {
		if (this.distanceWidget) {
			if (radius < minRadius) {
				radius = minRadius;
			} else if (radius > maxRadius) {
				radius = maxRadius;
			}
			
			var oldRadius = this.radius;
			this.radius = convertToMeters(radius, "mph", 0);
			this.changed = (oldRadius != this.radius ? true : false);

			this.distanceWidget.radiusWidget.set("radius",this.radius);
			this.distanceWidget.radiusWidget.center_changed();

			this.setFocus();
		}
	}	

	function getTitle() {
		return (this.name+"\n"+
				this.address+
				"\nRadius: "+ convertFromMeters(this.radius,"mph",2) + (convertFromMeters(this.radius,"mph",2)==1 ? " mile" : " miles") +
				"\nTriggered on "+["Entry","Exit","Transition"][this.triggerType]+
				"\nGeofence is "+(this.isEnabled ? "Enabled" : "Disabled")+
				"\n\nDrag to move geofence");
	}

	function setTitle() {
		this.distanceWidget.marker.setTitle(this.getTitle());
	}
	
	function setName(name) {
		if (this.name != name) {
			this.name = name;
			this.changed = true;
		}
	}
	
	function setEnabled(state) {
		this.isEnabled = state;
		this.changed = true;
	}

	function setTriggerType(trigger) {
		this.triggerType= trigger;
		this.changed = true;
	}
}

//GEOFENCES ----------------------------------------------------------
function Geofences(parent) {
	// Public properties
	this.parent = parent;
	this.list = [];
	this.geocoder = new google.maps.Geocoder();
	this.requestedAddresses = false;

	// Public methods
	this.init = init;
	this.getGeofences = getGeofences;
	this.addList = addList;
	this.getAddresses = getAddresses;
	this.update = update;
	this.hide = hide;

	this.init();

	function init()
	{
		this.getGeofences();
	}

	function getGeofences() {
		var _this = this;
		postRequest(services.GetGeofences, '{"vin":"'+this.parent.vin+'"}',
				function(json, textStatus, jq) {
			_this.addList(json.d);
			_this.getAddresses();
			populateZones();
		});
	}

	function addList(glist) {
	
	
		for (var i=0; i<3; i++) {
			if (i<glist.length) {
				this.list[glist[i].Index] = new Geofence(glist[i],this);
			}
		}
		if (glist.length < 3){
			var hasLocations = (this.parent.locations!==null) && (this.parent.locations.list!==null) && (this.parent.locations.list.length > 0);
			for (var j=0; j<3; j++) {
				if (this.list[j] == undefined){
					this.list[j] = new Geofence({Index:j, 
						Center: {
							Lat: hasLocations ? this.parent.locations.list[0].latitude : 39.8667,
							Lon: hasLocations ? this.parent.locations.list[0].longitude : -98.5556
						},
						Radius:1609, Geometry:0, TriggerType:0, 
						Name:"GeoFence "+(j+1), Description:null, 
						IsEnabled:false,
						DefaultFence:true},this);
				}
			}
		}
	}

	function getAddresses() {
		if (!this.requestedAddresses) {
			for (var g in this.list) {
				this.list[g].getAddress();
			}
			this.requestedAddresses=true;
		}
	}

	function update() {
		for (var i=0; i<3; i++) {
			var g = this.list[i];
			if (g.changed) {
				postRequest(services.StoreGeofence, 
						'{"vin":"'+this.parent.vin+'",' +
						'"geofence" : {"__type":"CircularGeofence:#SkyLinkWCFService.DataTypes",' +
									  '"Description" : "'+g.description+'",' +
									  '"Geometry" : '+g.geometry+',' +
									  '"Index" : '+i+',' +
									  '"IsEnabled" : '+g.isEnabled+',' +
									  '"Name" : "'+g.name+'",' +
									  '"TriggerType" : '+g.triggerType+ ',' +
									  '"Center" : {"__type":"GeoPoint:#SkyLinkWCFService.DataTypes",' +
									  '"Lat" : '+ g.myLatlng.lat() + ',' +
									  '"Lon" : ' + g.myLatlng.lng() + '},' +
									  '"Radius" : ' + g.radius +
									  '}}',
						function(json, textStatus, jq) {
				});				
			}
		}
	}
	
	/*
	function update() {
		for (var i=0; i<3; i++) {
			g = this.list[i];
			if (g.changed) {
				postRequest(services.StoreGeofence, 
						'{"vin":"'+this.parent.vin+'",' +
						'"geofence" : {"Description" : "'+g.description+'",' +
									  '"Geometry" : '+g.geometry+',' +
									  '"Index" : '+i+',' +
									  '"IsEnabled" : '+g.isEnabled+',' +
									  '"Name" : "'+g.name+'",' +
									  '"TriggerType" : "'+g.triggerType+'"}}',
						function(json, textStatus, jq) {
					alert("Set GeoFence "+i);
				});				
			}
		}
	}
	*/

	function hide() {
		for (var i=0; i<3; i++) {
			this.list[i].hideFence();
		}		
	}
	
}


/**
 * A distance widget that will display a circle that can be resized and will
 * provide the radius in km.
 *
 * @param {google.maps.Map} map The map on which to attach the distance widget.
 *
 * @constructor
 */
function DistanceWidget(parent) {

	this.set('map', parent.map);
	this.set('position',parent.myLatlng);
	var pinColor = ["FE7569","75FE69","7569FE"][parent.id];

	var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
			new google.maps.Size(21, 34),
			new google.maps.Point(0,0),
			new google.maps.Point(10, 34));

	var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
			new google.maps.Size(40, 37),
			new google.maps.Point(0, 0),
			new google.maps.Point(12, 35));

	this.marker = new google.maps.Marker({
		draggable: true,
		icon:pinImage,
		shadow:pinShadow
	});

	this.marker.setTitle(parent.getTitle());
	
	// Bind the marker map property to the DistanceWidget map property
	this.marker.bindTo('map', this);

	// Bind the marker position property to the DistanceWidget position
	// property
	this.marker.bindTo('position', this);

	// Create a new radius widget
	this.radiusWidget = new RadiusWidget(parent);

	var _this = this;
	google.maps.event.addListener(this.marker, 'dragend', function() {
		google.maps.event.trigger(_this,'dragend');
	});

	// Bind the radiusWidget map to the DistanceWidget map
	this.radiusWidget.bindTo('map', this);

	// Bind the radiusWidget center to the DistanceWidget position
	this.radiusWidget.bindTo('center', this, 'position');
	
	//	Bind to the radiusWidgets' distance property
	this.bindTo('distance', this.radiusWidget);

	// Bind to the radiusWidgets' bounds property
	this.bindTo('bounds', this.radiusWidget);
}
DistanceWidget.prototype = new google.maps.MVCObject();


/**
 * A radius widget that adds a circle to a map and centers on a marker.
 *
 * @constructor
 */
function RadiusWidget(parent) {
	var circle = new google.maps.Circle({
		strokeWeight: 2,
		fillColor:["red","green","blue"][parent.id], 
		fillOpacity:0.2 ,
		zIndex:-1
	});

	// Set the distance property value.
	this.set('distance', parent.radius);

	// Bind the RadiusWidget bounds property to the circle bounds property.
	this.bindTo('bounds', circle);

	// Bind the circle center to the RadiusWidget center property
	circle.bindTo('center', this);

	// Bind the circle map to the RadiusWidget map
	circle.bindTo('map', this);

	// Bind the circle radius property to the RadiusWidget radius property
	circle.bindTo('radius', this);

	google.maps.event.addListener(circle, 'click', function() {
		// Circle is clicked, so give that geofence focus.
		// NOTE: If in the same position as a marker (e.g. car) the marker takes precedence and this event won't be fired.
		parent.setFocus();
	});
	
	this.addSizer_();
}
RadiusWidget.prototype = new google.maps.MVCObject();


/**
 * Update the radius when the distance has changed.
 */
RadiusWidget.prototype.distance_changed = function() {
	this.set('radius', this.get('distance'));
};


/**
 * Add the sizer marker to the map.
 *
 * @private
 */
RadiusWidget.prototype.addSizer_ = function() {
	var radiusImage = new google.maps.MarkerImage("images/icon_resize.png",null,null,new google.maps.Point(25,11),new google.maps.Size(50, 22));

	var sizer = new google.maps.Marker({
		icon:radiusImage,
		draggable: true,
		title: 'Drag to set geofence area'
	});

	sizer.bindTo('map', this);
	sizer.bindTo('position', this, 'sizer_position');

	var me = this;
	google.maps.event.addListener(sizer, 'drag', function() {
		// Set the circle distance (radius)
		me.setDistance();
	});
};


/**
 * Update the center of the circle and position the sizer back on the line.
 *
 * Position is bound to the DistanceWidget so this is expected to change when
 * the position of the distance widget is changed.
 */
RadiusWidget.prototype.center_changed = function() {
	var bounds = this.get('bounds');

	// Bounds might not always be set so check that it exists first.
	if (bounds) {
		var lng = bounds.getNorthEast().lng();

		// Put the sizer at center, right on the circle.
		var position = new google.maps.LatLng(this.get('center').lat(), lng);
		this.set('sizer_position', position);
	}
};

/**
 * Calculates the distance between two latlng locations in km.
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} p1 The first lat lng point.
 * @param {google.maps.LatLng} p2 The second lat lng point.
 * @return {number} The distance between the two points in km.
 * @private
 */
RadiusWidget.prototype.distanceBetweenPoints_ = function(p1, p2) {
	if (!p1 || !p2) {
		return 0;
	}

	var R = 6371000; // Radius of the Earth in m
	var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
	var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
	Math.sin(dLon / 2) * Math.sin(dLon / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
	return d;
};


/**
 * Set the distance of the circle based on the position of the sizer.
 */
RadiusWidget.prototype.setDistance = function() {
	// As the sizer is being dragged, its position changes.  Because the
	// RadiusWidget's sizer_position is bound to the sizer's position, it will
	// change as well.
	var pos = this.get('sizer_position');
	var center = this.get('center');
	var distance = this.distanceBetweenPoints_(center, pos);

	// Set the distance property for any objects that are bound to it
	this.set('distance', distance);
};

function displayInfo(widget) {
	$("#geoAddress1").html(widget.get('position') + ', ' +
			widget.get('distance'));
}
