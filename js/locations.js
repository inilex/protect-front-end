//LOCATION ----------------------------------------------------------
function Location(parent,index, l, markerColor) {
	//Public Functions
	this.display = display;
	this.plot = plot;
	this.unplot = unplot;
	this.setMarkerIcon = setMarkerIcon;
	this.getParentVehicle = getParentVehicle;

	//Properties
	this.parent = parent;
	this.parentVehicle = this.getParentVehicle(this.parent);
	this.id = l.Id;
	this.index = index;

	//Properties
	l.StreetAddress = (l.StreetAddress!=null ? l.StreetAddress: "");
	l.City = (l.City!=null ? l.City : "");
	l.State = (l.State!=null ? l.State : "");
	l.Zip = (l.Zip!=null ? l.Zip : "");
	if (l.StreetAddress == "LOADING...") {
		this.address = l.StreetAddress;
	} else {
		this.address = (l.StreetAddress!="" ? l.StreetAddress+"," : "")+(l.City!="" ? l.City+"," : "")+(l.State!="" ? l.State+" " : "")+l.Zip;
	}
	this.time = (l.TimeStamp!=null ? formatDate(l.TimeStamp, "long", true) : "");
	this.rawTime = l.TimeStamp;
	
	// Slightly adjust lat/lng for cloned vehicles created for demo purposes
	this.latitude =  (l.Lat!=null ? l.Lat : 38.268) + (this.parent.parent.cloneNumber>0 ? 0.25*(Math.random()-0.5) : 0);
	this.longitude = (l.Lon!=null ? l.Lon : -94.99) + (this.parent.parent.cloneNumber>0 ? 0.25*(Math.random()-0.5) : 0);

	this.alertType = l.LocationAlarmType;
    this.alertIndex = l.LocationAlarmIndex;
    this.speed = l.Speed;
    this.listener = null;
    
    this.image = null;

    this.myLatlng = new google.maps.LatLng(this.latitude, this.longitude);

    var color = 0;
    switch (markerColor) {
    	case "red": color = 1;
    				break;
    	case "green": color = 2;
    				break;
    	case "yellow": color = 3;
    				break;
    }
    
	var pinColor = ["FE7569","FF0000","00FF00","00FFFF"][color];

	var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
			new google.maps.Size(21, 34),
			new google.maps.Point(0,0),
			new google.maps.Point(10, 34));

	var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
			new google.maps.Size(40, 37),
			new google.maps.Point(0, 0),
			new google.maps.Point(12, 35));

	this.marker = new google.maps.Marker({
		position: this.myLatlng,
		title: this.parentVehicle.name+"\n"+this.address+"\n"+this.time+" : "+formatSpeed(this.speed,"mph"),
		animation: google.maps.Animation.DROP
	});

	function setMarkerIcon(forcePinMarker) {
		// Dealer users will want a different type of marker (easier to see on a satellite view of a car lot).
		// Also, all but the most recent location should be displayed as a pointer - only the most recent location displays as a vehicle.
		if ((g_userType.behaviors.markerType=="pin") || (this.index>0) || (forcePinMarker)) {
			this.marker.setIcon(pinImage);
			this.marker.setShadow(pinShadow);			
		} else {
			var image = new google.maps.MarkerImage("images/"+this.parent.parent.icon+".png",null,null,new google.maps.Point(35,30),new google.maps.Size(70, 60));
			this.marker.setIcon(image);
		}
	}

	function display() {
		this.setMarkerIcon();
		g_map.display(this);
	}

	function plot(forcePinMarker) {
		this.setMarkerIcon(forcePinMarker);
		g_map.plot(this);
	}
	
	function unplot() {
		this.marker.setMap(null);
	}
	
	function getParentVehicle(parent) {
		if (parent instanceof Vehicle) {
			return parent;
		}	else if (parent.parent != null) {
			return this.getParentVehicle(parent.parent);		
		}
	}
}

//TODO: Tidy up enumeration of alerttypes
var alertTypes = new Array(
		  {number:0, name:"Location", icon:"locations"},
		  {number:1, name:"Ignition On", icon:"alertIgnOn"},
		  {number:2, name:"Ignition Off", icon:"alertIgnOff"},
		  {number:3, name:"Entered Geofence", icon:"alertGeofence"},
		  {number:4, name:"Left Geofence", icon:"alertGeofence"},
		  {number:5, name:"Speed Threshold Exceeded", icon:"alertSpeed"},
		  {number:6, name:"Low Battery Level", icon:"alertBattery"},
		  {number:7, name:"Early Theft Detection", icon:"alertQuickfence"},
		  {number:8, name:"Power Disconnected", icon:"alertPowerOff"},
		  {number:9, name:"Power Reconnected", icon:"alertPowerOne"},
		  {number:10, name:"Vehicle Idle", icon:"alertIdle"}
        );

//LOCATIONS ----------------------------------------------------------
function Locations(parent) {
    this.parent = parent;

	// Public Functions
	this.init = init;
	this.add = add;
	this.addList = addList;
	this.htmlPages = htmlPages;
	this.insert = insert;
	this.shift = shift;
	this.updateLocation = updateLocation;
	this.getLastLocation = getLastLocation;
	this.getLastNLocations = getLastNLocations;
	this.requestLocationUpdate = requestLocationUpdate;
	this.getLocationsAfterId = getLocationsAfterId;
	this.checkForLocationUpdate = checkForLocationUpdate;
	this.endLocationUpdate = endLocationUpdate;
	this.unplot = unplot;

	this.locationUpdateRequested=false;

	this.init();

	
	function init() {
		if (this.list!=undefined) {
			// Let's be sure to unplot anything we've previously plotted.
			this.unplot(true);
		}
		this.list = [];	
		this.updateIterations = 0;	// Default iterations or location updates to 0 for now.
	}

	function addList(llist) {
		for (var i in llist) {
			l = llist[i];
			this.add(i, l);
		}
		/*
		// Used for creating marketing materials
		if (this.parent.vehicleNumber==0) {
			this.list[0].address = '460 S Benson Lane, Chandler, AZ 85224'; 
			this.list[0].latitude = '33.295319'; 
			this.list[0].longitude = '-111.886761';
		} else {
			this.list[0].address = '401 East Jefferson Street, Phoenix, AZ 85004'; 
			this.list[0].latitude = '37.0625'; 
			this.list[0].longitude = '-95.677068';
		}
		*/

		this.parent.setIgnStatus(this.list[0].IgnitionOn);
	}

	function add(locationNumber, l) {
		this.list[locationNumber] = new Location(this, locationNumber, l);
	}

	function insert(llist) {
		this.shift(llist.length);
		this.addList(llist);
	}

	function shift(numItems) {
		n = this.list.length;
		for (var i=(n>9 ? 8 : n-1); i>=numItems-1; i--)
		{
			this.list[i+1] = this.list[i];
		}
	}

	function html(locationNumber) {
		l = this.list[locationNumber];
		return "\
				<div id='location'>\
				<img class='warningIcon' src='images/icon_locations.png' width='64px' height='64px' />\
				<span style='font-weight: bold; font-size: 22px;'>" + l.address + "</span>\
				<br/>" + l.time + "<br/>\
				<p class='clearFloat' />\
				<hr />\
				</div>";
	}

	function htmlPages() {
		if (this.list.length > 0) {
			// Set "loaded" and "unloaded" page indicators
			var len=this.list.length;
			for (var i=0; i<10; i++) {
				document.getElementById("page"+i+"_"+this.parent.vehicleNumber).className = "navPage"+ (i<len ? "Full" : "Empty");
			}
		}
	}
	
	function updateLocation(vehicle) {
	/* 1. We must first get most current locations (so we don't confuse
	      locations that have arrived at the server since the last update 
	      on the client with the update just requested.
	   2. Then issue a location update request.
	   3. When location update request returns, we need to periodically
	      check for new locations.  Anything reported will be the new
	      location.
	*/
		var _this = this;

		this.locationUpdateRequested = true;
		
		postRequest(services.GetLocationsAfterId, '{"vin":"'+this.parent.vin+'","locationId":' + this.list[0].id + '}',
				function(json, textStatus, jq)
				{

			// Received list of more recent locations than we know of.
			
			// If this list contains any new locations (i.e. is not empty) update 
			// the locations for this vehicle and request the update.
			if (json.d.length>0) {
				_this.insert(json.d);
			} 
			_this.updateIterations = locationUpdateIterations;	// Set the desired number of iterations to check for new locations before timing out
			_this.requestLocationUpdate();
				});
	}
	
	function requestLocationUpdate()
	{
		var _this = this;
		postRequest(services.RequestLocationUpdate, '{"vin":"'+this.parent.vin+'"}',
				function(json, textStatus, jq)
				{
			_this.getLocationsAfterId();
				});
	}
	
	function getLocationsAfterId() {
		var _this = this;
		
		postRequest(services.GetLocationsAfterId, '{"vin":"'+this.parent.vin+'","locationId":' + this.list[0].id + '}',
				function(json, textStatus, jq)
				{
			// Received list of more recent locations than we know of.
			
			// If this list contains any new locations (i.e. is not empty) update 
			// the locations for this vehicle and stop subsequent requests.
			if (json.d.length>0)
			{
				_this.insert(json.d);
				_this.htmlPages();
				_this.parent.showAddress(-_this.parent.locationNumber);
				_this.list[0].plot();
				_this.endLocationUpdate(_this);				
			} else {
				// Check periodically for new location to arrive
				if (_this.locationUpdateRequested == true) {	// i.e. this is the response to an update request from the user.
					clearTimeout(_this.locTimer);
					_this.locTimer = setTimeout(function(){_this.checkForLocationUpdate()},locationUpdatePeriod);
				} else {
					_this.endLocationUpdate(_this);				// For automated updates, we don't need to cycle.
				}
			}
				});
	}

	function checkForLocationUpdate() {
		if (this.updateIterations < 1)
		{
			alert("PROTECT could not obtain an updated location - please try again later."); 
			endLocationUpdate(this);
		} else {
			this.updateIterations = this.updateIterations-1;
			this.getLocationsAfterId();
		}
		return;
	}

	function endLocationUpdate(_this) {
		_this.locationUpdateRequested = false;
		hide("locWait_"+_this.parent.vehicleNumber);

		// If we are on the map screen, make sure to reset the onclick to get location updates.
		if (isVisible("map")) {
			document.getElementById("refresh_"+_this.parent.vehicleNumber).onclick=function() { _this.parent.updateLocation(); };
		}

		clearTimeout(_this.locTimer);
		_this.locTimer = setTimeout(function(){_this.getLocationsAfterId()},autoLocationCheckPeriod);
	}

	function getLastLocation()
	{
		var _this = this;
		postRequest(services.GetLastLocation, '{"vin":"'+this.parent.vin+'"}',
				function(json, textStatus, jq)
				{});
	}

	function getLastNLocations(numEvents, success) {
		if (!numEvents) { var numEvents=10 };
		var _this = this;
		show("locWait_"+this.parent.vehicleNumber);
		postRequest(services.GetLastNLocations,'{"vin":"' + this.parent.vin + '","numberOfLocations":'+numEvents+'}',
				function(json, textStatus, jq)
				{	
			if (success!=null) {
				success();
			}

			// Received locations for a vehicle, so repopulate the list.
			_this.init();
			_this.addList(eval('('+jq.responseText+')').d);

			_this.htmlPages();
			_this.parent.showAddress(0);
	
			// And clean up...
			_this.endLocationUpdate(_this);

			//HACK: if numEvents > 10, event list requested these events, so populate array.
			if (numEvents>10) {
				populateEvents();
			}

			// HACK: Go straight to map
			if (_this.parent.vehicleNumber==0) 
			{
				_this.list[0].display();
			}

		});
	}

	function unplot(removeAll) {
		//Remove anything that is plotted EXCEPT for the most recent location.
		for (var i in this.list) {
			if ((i>0) || (removeAll)) {
				l = this.list[i];
				this.list[i].unplot();
			}
		}
	}

}
