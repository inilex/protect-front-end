var ConsumerNotifications = {
	None : {
		value : 0x00
	},
	BatteryAlarm : {
		value : 0x01
	},
	ZoneAlarm : {
		value : 0x02
	},
	EarlyTheftDetection : {
		value : 0x04
	},
	SpeedAlarm : {
		value : 0x08
	},
	IgnitionOn : {
		value : 0x10
	},
	IgnitionOff : {
		value : 0x20
	},
	BatteryDisconnected : {
		value : 0x40
	},
	BatteryReconnected : {
		value : 0x80
	},
	IdleAlarm : {
		value : 0x100
	},
	HistorySummary : {
		value : 0x80000000
	},
	All : {
		value : 0x800001FF
	}
}

// USER ----------------------------------------------------------
function User(u, success) {

	this.userType = u.userType;

	this.id = null;
	this.firstName = null;
	this.lastName = null;
	this.fullName = null;

	this.streetAddress1 = null;
	this.streetAddress2 = null;
	this.suite = null;
	this.city = null;
	this.state = null;
	this.postalCode = null;
	this.fullAddress = null;
	;

	this.primaryPhone = null;
	this.cellPhone = null;
	this.cellProvider = null;

	this.canAddVehicles = u.CanAddVehicles;
	this.canManageUsers = u.CanManageUsers;
	this.canRenewSubscription = u.CanRenewSubscription;

	this.alternateEmail = null;
	;
	this.emailAddress = null;
	this.emailConfirm = null;

	this.timeZone = null;

	this.emailNotifications = null;
	this.smsNotifications = null;

	this.secretQuestionChoice = null;
	this.secretQuestionAnswer = null;
	this.PIN = null;

	// Default CC info to null. If it is stored and we need it, we will look
	// for the actual values when user clicks on "update payment information".
	this.ccInfo = null;

	this.autoRenew = null;
	this.storeCC = null;

	this.init = init;
	this.getUserInfo = getUserInfo;
	this.populate = populate;
	this.updateUserInfo = updateUserInfo;
	this.postUserInfo = postUserInfo;
	this.serialize = serialize;
	this.setNotifications = setNotifications;
	this.isValid = isValid;
	this.getCCInfo = getCCInfo;

	this.init(u, success)

	function init(u, success) {

		this.userType = u.userType;

		// If the user object we were passed does not have a name, then assume
		// it is empty
		// and therefore we must call out to get the current customerinformation
		// from the database.
		if ((u.FirstName == undefined) && (u.LastName == undefined)) {
			this.getUserInfo(success);
		} else {
			this.populate(u);
		}
	}

	function getUserInfo(success) {
		var _this = this;
		postRequest(services.GetCustomerInfo, null, function(json, textStatus,
				jq) {
			_this.populate(json.d);
			success();
		});
	}

	function populate(u) {

		/*
		 * "{"d":{"__type":"SkyLinkUserInformation:http:\/\/www.mysky-link.com\/skylink"," +
		 * ""AlternateEmail":""," + ""CanAddVehicles":true," +
		 * ""CanManageUsers":true," + ""CanRenewSubscription":true," +
		 * ""CellPhone":"602-614-2034"," + ""CellProvider":"AT&T Wireless"," +
		 * ""City":"Chandler"," + ""EmailAddress":"godonnell@inilex.com"," +
		 * ""EmailNotifications":63," + ""FirstName":"Phil"," + ""Id":3119," +
		 * ""LastName":"DeCarlo"," + ""PIN":5565," + ""PostalCode":"85224"," +
		 * ""PrimaryPhone":"480-889-5676"," + ""SecretQuestionAnswer":null," +
		 * ""SecretQuestionChoice":5," + ""SmsNotifications":15," +
		 * ""State":"AZ"," + ""StreetAddress1":"460 S Benson Lane"," +
		 * ""StreetAddress2":"b"," + ""Suite":null}}"
		 */

		this.id = u.Id;

		this.firstName = u.FirstName;
		this.lastName = u.LastName;

		this.fullName = (u.FirstName != "" ? u.FirstName + " " : "")
				+ (u.LastName != undefined ? u.LastName : "");

		this.streetAddress1 = u.StreetAddress1;
		this.streetAddress2 = u.StreetAddress2;
		this.suite = u.Suite;
		this.city = u.City;
		this.state = u.State;
		this.postalCode = u.PostalCode;

		this.fullAddress = (u.StreetAddress1 != "" ? u.StreetAddress1 + ","
				: "")
				+ (u.StreetAddress2 != "" ? u.StreetAddress2 + ", " : "")
				+ (u.City != "" ? u.City + ", " : "")
				+ (u.State != "" ? u.State + " " : "") + u.PostalCode;

		this.primaryPhone = u.PrimaryPhone;
		this.cellPhone = u.CellPhone;
		this.cellProvider = u.CellProvider;

		this.canAddVehicles = u.CanAddVehicles;
		this.canManageUsers = u.CanManageUsers;
		this.canRenewSubscription = u.CanRenewSubscription;

		this.alternateEmail = u.AlternateEmail;
		this.emailAddress = u.EmailAddress;
		this.emailConfirm = u.EmailAddress;

		this.timeZone = u.TimeZone;

		this.emailNotifications = u.EmailNotifications;
		this.smsNotifications = u.SmsNotifications;

		// TODO: Should not receive secret question answer or PIN in plain text
		this.secretQuestionChoice = u.SecretQuestionChoice;
		this.secretQuestionAnswer = u.SecretQuestionAnswer;
		this.PIN = u.PIN;

		this.autoRenew = u.AutoRenew;
		if (this.autoRenew) {
			// If auto-renewing, then we must also be saving CC info.
			this.storeCC = true;
		} else {
			// Otherwise, assume we're not storing CC info for now. If we find
			// otherwise
			// when we check, then we will update this value at that time.
			this.storeCC = false;
		}
	}

	function updateUserInfo() {
		if (this.isValid()) {
			this.postUserInfo();
		}
	}

	function postUserInfo() {
		var _this = this;
		postRequest(services.UpdateCurrentSkyLinkUser, this.serialize(),
				function(json, textStatus, jq) {
					// _this.populate(json.d);
					success();
				});
	}

	function isValid() {
		var errStr = "";

		if (isBlank(this.firstName) || isBlank(this.lastName)) {
			errStr = errStr + "Full name is required.<br/>";
		}

		if (isBlank(this.streetAddress1)) {
			errStr = errStr + "Street address is required.</br>";
		}

		if (isBlank(this.city)) {
			errStr = errStr + "City is required.</br>";
		}

		if (isBlank(this.state)) {
			errStr = errStr + "State is required.</br>";
		}

		if (isBlank(this.postalCode)) {
			errStr = errStr + "ZIP code is required.</br>";
		}

		if (isBlank(this.timeZone)) {
			errStr = errStr + "Please select a timezone.</br>";
		}

		if (isBlank(this.emailAddress)) {
			errStr = errStr + "Email address is required.</br>";
		} else if (isBlank(this.emailConfirm)) {
			errStr = errStr
					+ "Please re-enter email address in the Confirm Email field.</br>";
		} else if (this.emailAddress != this.emailConfirm) {
			errStr = errStr
					+ "Email addresses do not match - please re-enter..</br>";
		}

		if (errStr != "") {
			errStr = "<b>Please correct the following entries:</b></br>"
					+ errStr;
			$("#customerError").html(errStr);
			return false;
		}

		return true;
	}

	function serialize() {
		return ('{"skyLinkUserInformation":' + '{"FirstName":"'
				+ this.firstName
				+ '",'
				+ '"LastName":"'
				+ this.lastName
				+ '",'
				+ '"StreetAddress1":"'
				+ this.streetAddress1
				+ '",'
				+ '"StreetAddress2":"'
				+ this.streetAddress2
				+ '",'
				+ '"Suite":"'
				+ this.suite
				+ '",'
				+ '"City":"'
				+ this.city
				+ '",'
				+ '"State":"'
				+ this.state
				+ '",'
				+ '"PostalCode":"'
				+ this.postalCode
				+ '",'
				+ '"PrimaryPhone":"'
				+ this.primaryPhone
				+ '",'
				+ '"CellPhone":"'
				+ this.cellPhone
				+ '",'
				+ '"CellProvider":"'
				+ this.cellProvider
				+ '",'
				+ '"EmailAddress":"'
				+ this.emailAddress
				+ '",'
				+ '"AlternateEmail":"'
				+ this.alternateEmail
				+ '",'
				+ '"CanAddVehicles":'
				+ this.canAddVehicles
				+ ','
				+ '"CanManageUsers":'
				+ this.canManageUsers
				+ ','
				+ '"CanRenewSubscription":'
				+ this.canRenewSubscription
				+ ','
				+ '"SmsNotifications":'
				+ this.smsNotifications
				+ ','
				+ '"EmailNotifications":'
				+ this.emailNotifications
				+ ','
				+ '"SecretQuestionAnswer":"'
				+ this.secretQuestionAnswer
				+ '",'
				+ '"SecretQuestionChoice":'
				+ this.secretQuestionChoice
				+ ','
				+ '"TimeZone":'
				+ this.timeZone
				+ ','
				+ '"PIN":'
				+ this.PIN
				+ ','
				+ '"Id":'
				+ this.id
				+ (this.newPassword != "" ? ',"newpassword":"'
						+ this.newPassword + '","oldpassword":"'
						+ this.oldPassword + '"' : '') + '}}');
	}

	function setNotifications(notificationType, emailState, smsState) {
		var currEmailNotifications = this.emailNotifications;
		var currSmsNotifications = this.smsNotifications;

		switch (emailState) {
		case false:
			this.emailNotifications = this.emailNotifications
					& (ConsumerNotifications.All.value - notificationType);
			break;
		case true:
			this.emailNotifications = this.emailNotifications
					| notificationType;
			break;
		}

		switch (smsState) {
		case false:
			this.smsNotifications = this.smsNotifications
					& (ConsumerNotifications.All.value - notificationType);
			break;
		case true:
			this.smsNotifications = this.smsNotifications | notificationType;
			break;
		}

		if ((currEmailNotifications != this.emailNotifications)
				|| (currSmsNotifications != this.smsNotifications)) {
			this.serialize();
			this.postUserInfo();
		}
	}

	function getCCInfo(success) {
		var _this = this;
		this.ccInfo = new CreditCard(function() {
			if (_this.ccInfo.number != null) {
				// If we retrieved a non-empty list of CC info, then obviously
				// we're saving it
				_this.storeCC = true;
			}
			success();
		});
	}	
}

// USERS ----------------------------------------------------------
function Users() {

	this.init = init;
	this.addList = addList;

	this.init();

	function init() {
		this.list = [];
	}

	function add(userInfo, success) {
		this.list[userInfo.userNumber] = new User(userInfo, success);
	}
}
