//CREDIT CARD ----------------------------------------------------------
function CreditCard(success) {

	this.init = init;
	this.getCCInfo = getCCInfo;
	this.populate = populate;

	this.init(success)

	function init(success) {
		this.getCCInfo(success);
	}

	function getCCInfo(success) {
		var _this = this;
		postRequest(services.GetCCInfo, null, function(json, textStatus,
				jq) {
			_this.populate(json.d);
			success();
		});
	}

	function populate(response) {
		if (response.length > 0) {
			// If we received at least one credit card, then populate (with the first in the list).
			this.cardType = response[0].CreditCardType;
			this.number = response[0].Number;
			this.expirationMonth = getDate(response[0].ExpireDate).getMonth()+1;
			this.expirationYear = getDate(response[0].ExpireDate).getFullYear()-2000;
			this.firstName = response[0].FirstName;
			this.lastName = response[0].LastName;
			this.streetAddress1 = response[0].StreetAddress1;
			this.suite = response[0].Suite;
			this.city = response[0].City;
			this.state = response[0].State;
			this.postalCode = response[0].Zip;
		}
	}
}
