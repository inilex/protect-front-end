//DEALER ----------------------------------------------------------
function Dealer(d) {

	this.init = init;
	this.populate = populate;
	
	this.init(d)
	
	function init(d) {	
			this.populate(d);
			// Now we know who the dealer is, enable the promos.
			showPromo();
	}
	
	function populate(d) {

/*      "Dealership": {
        "__type": "DealerShip:#SkyLinkWCFService.DataTypes",
        "City": "Shawnee Mission",
        "CompanyName": "Showcase Honda",
        "DealerId": 41,
        "PhoneNumber": "(602) 274-3800",
        "PostalCode": "66201",
        "State": "KS",
        "StreetAddress1": "PO Box 634",
        "StreetAddress2": "",
        "Suite": "",
        "URL": "www.showcasehonda.com"
    },
    */
		
		this.id = d.DealerId
		this.companyName = d.CompanyName;
		this.streetAddress1 = d.StreetAddress1;
		this.streetAddress2 = d.StreetAddress2;
		this.suite = d.Suite;
		this.city = d.City;
		this.state = d.State;
		this.postalCode = d.PostalCode;
		this.url = d.URL;
		this.phone = d.PhoneNumber;
	}	
}
