//VEHICLE MAINTENANCE INTERVAL ----------------------------------------------------------
function VehMaintInterval(i,parent) {
	// Public properties
	this.parent = parent;
	this.id = i.MaintenanceIntervalID;
	this.index = i.Index;
	this.name = i.Name;
	this.description = i.Description;

	this.start = {date:i.Start.Date, odometer:Math.round(i.Start.Odometer*metersToMiles), hours:(i.Start.Utilization!=null ? Math.round(i.Start.Utilization/36)/100 : null), user:i.Start.User};
	this.due = {date:i.Due.Date, odometer:Math.round(i.Due.Odometer*metersToMiles), hours:(i.Due.Utilization!=null ? Math.round(i.Due.Utilization/36)/100 : null)};
	this.recorded = {date:i.Recorded.Date, odometer:Math.round(i.Recorded.Odometer*metersToMiles), hours:(i.Recorded.Utilization!=null ? Math.round(i.Recorded.Utilization/36)/100 : null)};
	this.duePercentage = {days:i.DuePercent.Days, odometer:i.DuePercent.Distance, hours:i.DuePercent.Utilization};
	
	this.changed = false;

	//Public methods
	this.setName = setName;
	this.setEnabled = setEnabled;
	this.set = set;
	this.log = log;

	function setFocus () {
		if (this.id == 0) {
			$("#maintMore0").html("(...less)");
			$("#maintMore0").click("loseFocus()");
			show("maintWrapper0");
		} else {
			$("#maintMore0").html("(...more)");
			$("#maintMore0").click("setFocus()");
			hide("maintWrapper0")
		}
		
		if (this.id == 1) {
			$("#maintMore1").html("(...less)");
			$("#maintMore1").click("loseFocus()");
			show("maintWrapper1");
		} else {
			$("#maintMore1").html("(...more)");
			$("#maintMore1").click("setFocus()");
			hide("maintWrapper1")
		}

		if (this.id == 2) {
			$("#maintMore2").html("(...less)");
			$("#maintMore2").click("loseFocus()");
			show("maintWrapper2");
		} else {
			$("#maintMore2").html("(...more)");
			$("#maintMore2").click("setFocus()");
			hide("maintWrapper2")
		}
	}
	
	function loseFocus() {
		if (this.id == 0) {
			$("#maintMore0").html("(...more)");
			$("#maintMore0").click("setFocus()");
			hide("maintWrapper0")
		}
		
		if (this.id == 1) {
			$("#maintMore1").html("(...more)");
			$("#maintMore1").click("setFocus()");
			hide("maintWrapper1")
		}

		if (this.id == 2) {
			$("#maintMore2").html("(...more)");
			$("#maintMore2").click("setFocus()");
			hide("maintWrapper2")
		}
	}
		
	function setName(name) {
		if (this.name != name) {
			this.name = name;
			this.changed = true;
		}
	}
	
	function setEnabled(state) {
		this.isEnabled = state;
		this.changed = true;
	}
	
	function set(maintenanceIntervalID) {
		this.id = maintenanceIntervalID;
		this.changed = true;

		// If we have selected a maintenance interval, indicate we're updating the detail, otherwise just blank it out.
		$("#maintDueIn"+this.index).html((this.id != null ? "Updating..." : ""));
		$("#maintDetail"+this.index).html("");

	}
	
	function log(intervalID) {
		$("#maintDueIn"+this.index).html("Updating...");
		$("#maintDetail"+this.index).html("");

		var _this = this;
		postRequest(services.LogVehicleMaintenanceIntervals, 
					'{"vin" : "' +this.parent.parent.vin + '",' + 
					'"intervalIndexes" : [' + _this.index + ']}',
					function(json, textStatus, jq) {
						// If a maintenance interval id was passed in, then we need to set the 
						// maintenance interval and update in the database.
						if (intervalID!=undefined) {
							_this.set(intervalID); 
							_this.parent.update();
						}
					}
			);			
	}
	
}

//VEHICLE MAINTENANCE INTERVALS ----------------------------------------------------------
function VehMaintIntervals(parent) {
	// Public properties
	this.parent = parent;
	this.list = [];

	// Public methods
	this.init = init;
	this.getVehMaintIntervals = getVehMaintIntervals;
	this.addList = addList;
	this.update = update;
	this.hide = hide;

	this.init();

	function init()
	{
		this.list = [];
		this.getVehMaintIntervals();
	}

	function getVehMaintIntervals() {
		var _this = this;
		postRequest(services.GetVehicleMaintenanceIntervals, '{"vin":"'+this.parent.vin+'"}',
				function(json, textStatus, jq) {
			_this.addList(json.d);
			populateVehMaintIntervals();
		});
	}

	function addList(ilist) {
		for (var j=0; j<3; j++) {
			if (j<ilist.length) {
				i = ilist[j];
			} else {
				i = {Index:j, 
						Name: "-- Select Interval --",
						Description: "",
						ID: "0",
						Start:{Date:null, Odometer:null, Utilization:null, User:null},
						Due:{Date:null, Odometer:null, Utilization:null, User:null},
						Recorded:{Date:null, Odometer:null, Utilization:null, User:null},
						DuePercent:{Days:null, Odometer:null, Utilization:null}
						};
			}
			
			this.list[i.Index] = new VehMaintInterval(i,this);
		}
	}

	function update() {
		var valStr = "";
		var changed = false;
		
 		for (var j=0; j<3; j++) {
			var i = this.list[j];
			valStr = valStr + (i.id!=undefined ? i.id : null) + (j<2 ? "," : "");
			if (i.changed) {
				changed = true;
			}
 		}
 		
 		if (changed) {
 			var _this = this;
 			postRequest(services.UpdateVehicleMaintenanceIntervals, 
 					'{"vin":"'+this.parent.vin+'",' +
 					'"maintenanceIntervalID" : [' +
 					valStr + 
 					']}',
 					function(json, textStatus, jq) {
 						_this.list = [];
 						_this.addList(json.d);
 						populateVehMaintIntervals();
 					});		
 		}
	}
	
}
