//VEHICLE ----------------------------------------------------------
function Vehicle(v, cloneNumber) {
	// Public properties
	this.vehicleNumber = v.vehicleNumber;
	this.cloneNumber = cloneNumber;

	this.promoImageUrl = v.PromoImageUrl + "?vin=" + v.Vin;
	this.promoRedirectUrl = v.PromoRedirectUrl + "?vin=" + v.Vin;

	this.vin = v.Vin;
	this.deviceSerial = v.DeviceSerial;
	this.year = v.Year;
	this.make = v.Make;
	this.model = v.Model;
	this.name = (v.Name!=null ? v.Name+(this.cloneNumber>0 ? " "+this.cloneNumber : "") : this.year+" "+this.make+" "+this.model+(this.cloneNumber>0 ? " "+this.cloneNumber : "") );
	this.licensePlate = v.LicensePlate;
	this.lastIgnStatus = null;
	this.lastTime = "";
	this.lastAddress = "";

	this.isBatteryAlarmed = null;
	this.isSpeedAlarmed = null;
	this.speedAlertValue = null;
	
	this.isQuickFenceViolated = v.isQuickFenceViolated;
	this.isQuickFenceEnabled = v.isQuickFenceEnabled;
	
	this.isBatteryAlertEnabled = v.isBatteryAlertEnabled;
	this.isSpeedAlertSet = v.isSpeedAlertSet;
	
	this.insurer = null;
	this.dealer = null;
	
	this.locationNumber = 0;
	this.locTimer;
	this.fenceTimer;
	this.autoGetFenceStatus=true;			// Indicates whether we are waiting for an automatic quickfence status to occur (true) or if the user has initiated a change to status (false)

	this.geofences = null;
	this.vehMaintIntervals = null;

	this.plans = new Plans(this, v.SkyLinkPlan);
		
	this.locations = new Locations(this);	// 10 most recent historical events loaded by default and selectable on vehicle details panel.
	this.events = new Locations(this);		// Historical events loaded when viewing the history panel.
	
	this.trips = new Trips(this);	

	if (this.plans.protectPlanIsCurrent) {
		// Initialize vehicle locations with default "LOADING..."
//		this.locations.add(0,{"StreetAddress":"LOADING...","LocationAlarmType":0});
	}

	this.color = v.Color.toLowerCase();
	if (this.color == "silver") {
		this.color = "gray";
	}

	if ((this.color != "red") 
			&& (this.color != "white") 
			&& (this.color != "blue") 
			&& (this.color != "green") 
			&& (this.color != "purple") 
			&& (this.color != "brown") 
			&& (this.color != "black") 
			&& (this.color != "yellow") 
			&& (this.color != "gray")) {
		this.color = "blue";
	}

	this.icon = "car_" + this.color;

	this.isVisible = false;
		
	// Public methods
	this.init = init;
	this.html = html;
	this.htmlVehicleTable = htmlVehicleTable;
	this.htmlVehicleIcon = htmlVehicleIcon;
	this.htmlLocationIcon = htmlLocationIcon;
	this.htmlAddressDiv = htmlAddressDiv;
	this.htmlAddress = htmlAddress;
	this.setIcon = setIcon;
	this.showAddress = showAddress;
	this.showVehicle = showVehicle;
	this.setIgnStatus = setIgnStatus;
	this.showIgnStatus = showIgnStatus;
	this.updateLocation = updateLocation;

	this.toggleQFAutoArm = toggleQFAutoArm;
	this.toggleQuickFence = toggleQuickFence;
	this.setQuickFence = setQuickFence;
	this.checkForQuickFenceUpdate = checkForQuickFenceUpdate;
	this.checkQuickFenceSet = checkQuickFenceSet;
	this.endQuickFenceUpdate = endQuickFenceUpdate;
	this.showQuickFenceStatus = showQuickFenceStatus;

	this.setBatteryAlert = setBatteryAlert;
	this.checkForBatteryAlertUpdate = checkForBatteryAlertUpdate;
	this.checkBatteryAlertSet = checkBatteryAlertSet;
	this.endBatteryAlertUpdate = endBatteryAlertUpdate;
	this.showBatteryAlertStatus = showBatteryAlertStatus;

	this.getDashboard = getDashboard;
	this.setSpeedAlert = setSpeedAlert;
	
	this.clearAllMarkers = clearAllMarkers;
	
	this.init();
	
	function init() {
		// We have to create the HTML for this vehicle before we get locations
		// since we need to display the "wait" indicator on top of the "location" icon.
		this.showVehicle();
	
		// Now show the last known location for users/plans that support location.
		if ((g_userType!=userTypes.Consumer) || (this.plans.protectPlanIsCurrent)) {
			this.locations.add(0, v.LastLocation);
			this.showAddress(0);
		}
		
		// Only interested in locations and dashboard for vehicles that have a current Protect plan,
		// and then only when the user is interested in them.
		if ((this.plans.protectPlanIsCurrent) && (g_userType.behaviors.numLocations>0)) {
			var _this = this;
			this.locations.getLastNLocations(g_userType.behaviors.numLocations,function () {} ); // Get the list of last N locations ...
			_this.getDashboard();								  // ...and the dashboard info.
		}
	}

	function html() {
			return "\
					<div id='vehicle_"+this.vehicleNumber+"' class='vehicle' style='font-size:large;'>" + ((this.vehicleNumber>0) && (!isVisible("map"))? "<div class='separator'><hr/></div>" : "") +
				    (vehicles.list.length>1 ? "<img src='images/page_Previous.png' id='vehPrev_"+this.vehicleNumber+"' onclick='vehicles.showVehicle(-1);' class='pageNavIcon'/>":"") +
					"<span style='font-weight: bold;'>" + this.name + "</span>" +
					(vehicles.list.length>1 ? "<img src='images/page_Next.png' id='vehNext_"+this.vehicleNumber+"' onclick='vehicles.showVehicle(1);' class='pageNavIcon'/>":"") +
				    "<img src='images/btn_close.png' id='close_"+this.vehicleNumber+"' onclick='hide(\"vlistContainer\")' class='closeButton'/>" +
					(g_userType.behaviors.showVIN ? "<br/><div id='vin'>"+this.vin+'</div>': "") +
					"<br />" + this.htmlVehicleTable() +"\
				</div>";
	}

	function htmlVehicleTable() {
		if (window.document.body.clientWidth > minLandscapeWidth) {
			g_screenLayout = ScreenLayouts.landscape;
			return "\
					 <table border=0 width=100%>\
					      <tr><td width=1px>" + this.htmlVehicleIcon() +"</td>\
					      <td>" + this.htmlAddressDiv() + "</td>\
					      <td width=1px>"+ this.htmlLocationIcon() +"</td>\
					    </tr>\
				      </table>\
				      ";
		} else {
			g_screenLayout = ScreenLayouts.portrait;
			return "\
					 <table border=0 width=100%>\
					    <tr>\
					      <td>\
					        <table>\
					          <tr>\
					            <td width=1px>" + this.htmlVehicleIcon() + "</td>\
							  </tr>\
							  <tr>\
							    <td width=1px>"+ this.htmlLocationIcon() +"</td>\
							  </tr>\
						    </table>\
						  </td><td>" + this.htmlAddressDiv() + "</td>\
					  </tr>\
					</table>\
					";
		}
	}
	
	function htmlVehicleIcon() {
		return "\
				<div style='position:relative'"+ (this.plans.protectPlanIsCurrent || this.plans.etdPlanIsCurrent ? "onclick='vehicles.list["+this.vehicleNumber+"].toggleQuickFence()'" : "") + ">\
		            <div class='fenceContainer'>\
				      <img class='vehicleIcon' src='images/" +this.icon + ".png' id='vehicle_" +this.vehicleNumber+ "' title='Click here to set the QuickFence'/>\
				      <img class='fenceIcon' src='images/quickfence.png' id='fence_" +this.vehicleNumber+ "' "+(this.isQuickFenceEnabled ? "style='visibility:visible;' " : "")+"/>\
			        </div>\
			        " + (g_userType.behaviors.allowQuickFence ? "<img  style='position:absolute; top:6px; left:23px; z-index:2; display:none;' id='fenceWait_"+this.vehicleNumber+"' src='images/wait30trans.gif' width='36px' height='36px'/>" : "") + "\
			      </div>\
			    "; 
	}
	
	function htmlLocationIcon() {
//      <img src='images/icon_locations.png' onclick='vehicles.showList()' style='width:64px; height:64px; padding:0px 10px 0px 10px'/>\

		// TODO: For mobile apps, change onclick below to call display() instead of updateLocation().  Probably
		// best to do that conditionally for mobile, rather than leaving it that way and having to update it dynamically for non-mobile.
		// i.e. onclick='vehicles.list[" +this.vehicleNumber+ "].locations.list[0].display("+this.vehicleNumber+")
		//
		// Also, add "display:none" back into refreshContiner CSS definition.
		return "\
		  <div id='locationContainer_"+this.vehicleNumber+"' style='position:relative' onclick='vehicles.list[" +this.vehicleNumber+ "].updateLocation();' >\
		    "+ (this.plans.protectPlanIsCurrent ? "<img src='images/icon_locations.png' style='width:64px; height:64px; padding:0px 10px 0px 10px'/>\
	        <div class='refreshContainer' id='refresh_"+this.vehicleNumber+"'>\
	          <img  class='refreshIcon' src='images/icon_refresh.png' width='48px' height='48px'/>\
	        </div>\
	        <img  style='position:absolute; top:-6px; left:24px; display:none; z-index:2;' id='locWait_"+this.vehicleNumber+"' src='images/wait30trans.gif' width='36px' height='36px'/>" : "") + "\
	      </div>\
		    "; 
	}
	
	function htmlAddressDiv() {
		if (this.plans.protectPlanIsCurrent) {
			return "\
					<div class='address'>\
					  <div id='address_"+this.vehicleNumber+"'>" +this.htmlAddress()+ "</div>\
					  <div id='pageIndicator_"+this.vehicleNumber+"' class='pageIndicator'>\
					    <img src='images/page_Previous.png' id='addPrev_"+this.vehicleNumber+"' onclick='vehicles.list["+this.vehicleNumber+"].showAddress(-1)' class='pageNavIcon'/>\
					    <div class='navContainer'>\
					      <div class='navPage" + (this.locationNumber == 0 ? "Active" : (1<this.locations.list.length ? "Full" : "Empty")) + "'  id='page0_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 1 ? "Active" : (1<this.locations.list.length ? "Full" : "Empty")) + "'  id='page1_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 2 ? "Active" : (3<this.locations.list.length ? "Full" : "Empty")) + "'  id='page2_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 3 ? "Active" : (3<this.locations.list.length ? "Full" : "Empty")) + "'  id='page3_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 4 ? "Active" : (4<this.locations.list.length ? "Full" : "Empty")) + "'  id='page4_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 5 ? "Active" : (5<this.locations.list.length ? "Full" : "Empty")) + "'  id='page5_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 6 ? "Active" : (6<this.locations.list.length ? "Full" : "Empty")) + "'  id='page6_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 7 ? "Active" : (7<this.locations.list.length ? "Full" : "Empty")) + "'  id='page7_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 8 ? "Active" : (8<this.locations.list.length ? "Full" : "Empty")) + "'  id='page8_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPage" + (this.locationNumber == 9 ? "Active" : (9<this.locations.list.length ? "Full" : "Empty")) + "'  id='page9_"+this.vehicleNumber+"' style='margin-right:0px;'>&nbsp;</div>\
					    </div>\
					    <img src='images/page_Next.png' id='addNext_"+this.vehicleNumber+"' onclick='vehicles.list["+this.vehicleNumber+"].showAddress(1)' class='pageNavIcon'/>\
					  </div>\
					</div>\
				";
		} else if (this.plans.etdPlanIsCurrent) {
			return "Click on the vehicle to turn QuickFence on and off.<br/>To find out how to access more features, <a onclick='promoCart();'>Click here.</a>";		
		} else {
			return "This vehicle does not have a current SkyLink PROTECT plan.<br/><a onclick='promoCart();'>Click here</a> to PROTECT your vehicle.";
		}
	}
	
	function htmlAddress() {
		if (this.locations.list.length > 0) {
			var l = this.locations.list[this.locationNumber];
			return "<table width=100%><tr><td width=1px>\
					<img src='images/icon_"+alertTypes[l.alertType].icon+".png' width='32px' height='32px' onclick=''/>\
					</td><td>"+l.time+"\
					<br/>"+l.address+"\
					</td></tr></table>";
		}
	}

	function setIcon() {
		document.getElementById("vehicle_" +this.vehicleNumber).src = "images/" +this.icon + ".png";
		this.showQuickFenceStatus();
		(this.isQuickFenceEnabled==true ? show("fence_" +this.vehicleNumber) : hide("fence_" +this.vehicleNumber));
	}
	
	function showAddress(locDelta) {
		if (this.locations.list.length > 0) {
			
			// Remove any markers we may have left lying around.
			this.clearAllMarkers();
			
			var llist = this.locations.list;
			var len = llist.length;

			if ((this.locationNumber+locDelta >= 0) && (this.locationNumber+locDelta<len)) {
				if (document.getElementById("page"+this.locationNumber+"_"+this.vehicleNumber) != null) {
					// Turn off current page indicator.
					document.getElementById("page"+this.locationNumber+"_"+this.vehicleNumber).className = "navPageFull";
				}

				//Remove any but the most recent marker, which always stays.
				if (this.locationNumber>0) {
					llist[this.locationNumber].unplot();
				}
				
				this.locationNumber += locDelta;
				l = llist[this.locationNumber];
				if (document.getElementById("address_"+this.vehicleNumber) != null) {
					document.getElementById("address_"+this.vehicleNumber).innerHTML = this.htmlAddress();
				}

				// Turn on current page indicator.
				if (document.getElementById("page"+this.locationNumber+"_"+this.vehicleNumber) != null) {
					document.getElementById("page"+this.locationNumber+"_"+this.vehicleNumber).className = "navPageActive";
				}

				// Only plot the address if the vehicle is currently being displayed.
//				if (isVisible("vehicle_"+this.vehicleNumber)) {
					l.plot();
//				}

				setvListDimensions();
			}
		}
	}
		
	function showVehicle() {	
		$("#vehicleList").html($("#vehicleList").html()+this.html());

		if(!this.isVisible) {
			hide("vehicle_"+v.vehicleNumber); 
		} else {
			show("vehicle_"+v.vehicleNumber); 				
		}
		// Also, while we're here, ensure the fence icon is set appropriately
		this.setIcon();
		// And the page display...
		this.locations.htmlPages();
		// And the current page...
		this.showAddress(0);
	}	
	
	function updateLocation() {
		// Don't allow another update request while one is in progress
		document.getElementById("locationContainer_"+this.vehicleNumber).onclick=function() { };
	
		show("locWait_"+this.vehicleNumber);
	
		this.locations.updateLocation();
	}
	
	
	function setIgnStatus (ignStatus) {
//		this.lastIgnStatus = (ignStatus ? "ON" : "OFF");
//		this.showIgnStatus(); 
	}

	function showIgnStatus(vehicleNumber) {
		document.getElementById("ignStatus_"+this.vehicleNumber).innerHTML = this.lastIgnStatus +")";
	}

	// QUICKFENCE
	function toggleQFAutoArm() {
		// TODO: Build auto arm toggle for Moto QuickFence feature.
	}

	function toggleQuickFence() {
		// Only perform toggle if we are in the mode of waiting for an auto update
		// (i.e. we are not waiting for a repsonse to a prior toggle request).
		if (this.autoGetFenceStatus) {
			this.isQuickFenceEnabled = !this.isQuickFenceEnabled;
			this.autoGetFenceStatus=false;						// Indicate that we are NOT doing an auto update of fence status.
			this.setQuickFence();
		}
	}

	function setQuickFence() {
		var _this = this;
		show("fenceWait_"+this.vehicleNumber);

		postRequest(services.SetQuickFence, '{"vin":"'+this.vin+'", "enable":'+this.isQuickFenceEnabled+'}',
				function(json, textStatus, jq)
				{
			// Successfully requested to set quickfence

			// Since SetQuickFence WS does not return status of fence,
			// we need to request it to confirm it was properly set.
			clearTimeout(_this.fenceTimer);
			_this.fenceTimer = setTimeout(function(){_this.checkForQuickFenceUpdate(fenceStatusUpdateIterations)},fenceStatusUpdatePeriod);
				});
	}

	function checkForQuickFenceUpdate(iterations) {
		if (iterations < 1)
		{
			alert("Could not set QuickFence - try again later"); 
			hide("fenceWait_"+this.vehicleNumber);
		} else {
			this.checkQuickFenceSet();
			var _this = this;
			this.autoGetFenceStatus=false;	// Indicate that we are NOT doing an auto update of fence status.
			clearTimeout(this.fenceTimer);
			this.fenceTimer = setTimeout(function(){_this.checkForQuickFenceUpdate(iterations-1)},fenceStatusUpdatePeriod);
		}
	}
	
	function checkQuickFenceSet() {
		var _this = this;
		if (!_this.autoGetFenceStatus) {
			show("fenceWait_"+this.vehicleNumber);
		}
		
		postRequest(services.IsQuickFenceSet, '{"vin":"'+this.vin+'"}',
				function(json, textStatus, jq)
				{
			// If this is an auto update of fence status, then accept it for what it is
			// (otherwise, we initiated a toggle and therefore need to wait for it to change).
			if ((_this.autoGetFenceStatus) || (_this.isQuickFenceEnabled==null))
			{
				_this.isQuickFenceEnabled = eval('('+jq.responseText+')').d;
			}
			
			// Received quickfence status for a vehicle, so repopulate the icon.
			if (_this.isQuickFenceEnabled == eval('('+jq.responseText+')').d)
			{
				_this.endQuickFenceUpdate();
			}
				}); 		
	}

	function endQuickFenceUpdate() {
		hide("fenceWait_"+ this.vehicleNumber);
		this.setIcon();
		this.autoGetFenceStatus=true; // Indicate that we ARE doing an auto update of fence status.
		var _this = this;
		clearTimeout(this.fenceTimer);
		
		this.fenceTimer = setTimeout(function(){_this.checkQuickFenceSet()},autoFenceCheckPeriod);
	}
	
	function showQuickFenceStatus() {
		if (this.isQuickFenceEnabled) {
			show("fence_" +this.vehicleNumber)
			$("#qfStatus_On").attr("checked", true);
		} else {
			hide("fence_" +this.vehicleNumber);
			 $("#qfStatus_Off").attr("checked", true);
		}
	}

	// BATTERY
	function setBatteryAlert(state) {
		var _this = this;
		this.batteryAlertEnabled = state;

		postRequest(services.SetBatteryAlertState, '{"vin":"'+this.vin+'", "enable":'+state+'}',
				function(json, textStatus, jq)
				{
			// Successfully requested to set battery alert state

			// Since SetBatteryAlertState WS does not return status of battery alert setting,
			// we need to request it to confirm it was properly set.
			clearTimeout(_this.batteryTimer);
			_this.batteryTimer = setTimeout(function(){_this.checkForBatteryAlertUpdate(batteryStatusUpdateIterations)},batteryStatusUpdatePeriod);
				});
	}

	function checkForBatteryAlertUpdate(iterations) {
		if (iterations < 1)
		{
			alert("Could not set battery state - try again later"); 
		} else {
			this.checkBatteryAlertSet();
			var _this = this;
			clearTimeout(this.batteryTimer);
			this.batteryTimer = setTimeout(function(){_this.checkForBatteryAlertUpdate(iterations-1)},batteryStatusUpdatePeriod);
		}
	}
	
	function checkBatteryAlertSet() {
		var _this = this;
		
		postRequest(services.IsBatteryAlertEnabled, '{"vin":"'+this.vin+'"}',
				function(json, textStatus, jq)
				{
			// Received battery status for a vehicle, so we're done.
			if (_this.batteryAlertStatus == eval('('+jq.responseText+')').d)
			{
				_this.endBatteryAlertUpdate();
			}
				}); 
	}

	function endBatteryAlertUpdate() {
		this.showBatteryAlertStatus();
		clearTimeout(this. batteryTimer);
	}

	function showBatteryAlertStatus() {
		if (this.batteryAlertEnabled) {
			$("#batteryAlerts_On").attr("checked", true);
		} else {
			$("#batteryAlerts_Off").attr("checked", true);
		}
	}
	
	function getDashboard() {
		var _this = this;
		postRequest(services.GetDashboard, '{"vin":"'+this.vin+'"}',
				function(json, textStatus, jq)
				{
					_this.owner = new User(json.d.InsuranceCard.OwnerInformation);
					_this.insurer = new Insurer(json.d.InsuranceCard);
					_this.dealer = new Dealer(json.d.InsuranceCard.Dealership);
					_this.isBatteryAlarmed = json.d.IsBatteryAlarmed;
					_this.isSpeedAlarmed = json.d.IsSpeedAlarmed;
					_this.speedAlertValue = json.d.SpeedAlertValue;
					_this.isQuickFenceViolated = json.d.IsQuickFenceViolated;
					// TODO: Determine whether response contains "IsQuickFenceSet" vs. "IsQuickFenceEnabled"
					//_this.isQuickFenceEnabled = json.d.IsQuickFenceSet;
					_this.batteryAlertEnabled = _this.isBatteryAlertEnabled;
					_this.speedAlertEnabled = json.d.SpeedAlertEnabled;
				});
	}
	
	function setSpeedAlert(speed) {
		var _this = this;
		postRequest(services.SetSpeedAlert, '{"vin":"'+this.vin+'", "value":'+speed+'}',
				function(json, textStatus, jq)
				{ });
	}

	function clearAllMarkers() {
		this.locations.unplot();
		this.events.unplot(true);
		this.trips.unplot(true);
	}
}


//VEHICLES ----------------------------------------------------------
function Vehicles() {

	//Public Functions
	this.init = init;
//	this.setCurrentVehicle = setCurrentVehicle;
//	this.getCurrentVehicle = getCurrentVehicle;
//	this.currentVehicleNumber = currentVehicleNumber;
	this.getByVIN = getByVIN;
	this.getByVINSuffix = getByVINSuffix;
	this.getByLowBattery = getByLowBattery;
	this.getEvents = getEvents;
	this.getFilteredEvents = getFilteredEvents;
	this.getList = getList;
	this.addList = addList;
	this.showList = showList;
	this.getID = getID;
	this.showVehicle = showVehicle;
	this.getDashboards = getDashboards;

	var currentVehicle = 0;
	
	this.init();

	function init() {
		this.list = [];
	}

	this.getCurrentVehicle = function () {
		return this.list[currentVehicle];
	}

	this.currentVehicleNumber = function () {
		return this.list[currentVehicle].vehicleNumber;
	}
	
	this.setCurrentVehicle = function (v) {
		switch (typeof(v)) {
		case "number":
			currentVehicle = v;
			break;
		case "Vehicle":
			currentVehicle = v.vehicleNumber;
			break;
		}
	}
	
	function getByVIN() {
		document.getElementById("vehicleList").innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:20px; margin-bottom:20px'/>";
		showOnly("vlistContainer");

		var _this = this;
		postRequest(services.GetVehiclesByVIN, null,
				function(json, textStatus, jq) {
			_this.init();
			_this.addList(eval('('+jq.responseText+')').d);
			_this.showList();
		});
	}

	function getByLowBattery(Location) {
		document.getElementById("vehicleList").innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:20px; margin-bottom:20px'/>";
		showOnly("vlistContainer");
		
		var startDate = new Date();
		startDate.setDate(startDate.getDate()-3);  

		var startDateStr = "\\/Date("+startDate.getTime()+"-0700)\\/";

		this.getEvents('{"vFilter":{"area":{"id":0, "Radius":5, '+Location+'},"maxCount":20},"eFilter":{"locationAlertTypes":["Battery"],"maxCount":1,"startDate":"'+startDateStr+'"},"lastLocation":true,"userType":' +g_userType.number+ '}',
						function() {
			//TODO: Is _this still necessary now that we've moved this code into a function that is passed into GetEvents()?
			_this.init();
			if (json.d.vehicles.length>0) {		// If vehicles were returned, show them.
				_this.addList(eval('('+jq.responseText+')').d.vehicles);
				_this.showList();

				// TODO: Make changes to vehicle.show() to accomodate obtaining locations as events.  Since vehicle is
				// now responsible for getting regular locations, we'll have to modify it to support FindIt battery alerts.
				/*
				// We now have the list of vehicles, so for each vehicle...
				for (var i=0; i<_this.list.length; i++) {
					// Only interested in vehicles that have a current Protect plan.
					if (_this.list[i].plans.protectPlanIsCurrent) {
						// Received locations for a vehicle, so repopulate the list.
						_this.list[i].locations.init();
						// Find eventList for this vehicle
						for (var e in json.d.eventList) {
							if (json.d.eventList[e].Vin==_this.list[i].vin) {
								// And show the most recent event for it.
								_this.list[i].locations.addList(json.d.eventList[e].Events);
								_this.list[i].locations.htmlPages();
								_this.list[i].showAddress(0);
							}
						}
					}
				}*/
			} else {
				showError("No matching vehicles found.");
			}

		});
	}

	function getFilteredEvents(vin, numEvents, events, startDate, endDate, success) {
		if (!startDate) { 
			var startDate=0; 
		} else { 
			startDate = new Date(startDate); 
			startDate.setHours(startDate.getHours()+7);
			startDate = startDate.getTime();
		}
		
		if (!endDate) { 
			var endDate=new Date(); 
		} else {
			endDate = new Date(endDate);
			endDate.setDate(endDate.getDate()+1);
		}
		
		//Server isn't smart enough to compensate yet, so we will calculate timezone offset for it.
		endDate.setHours(endDate.getHours()+7);
		endDate = endDate.getTime(); 			
		
		this.getEvents('{"vFilter":{"vins":["'+vin+'"],"maxCount":1},"eFilter":{'+events+',"maxCount":'+numEvents+',"startDate":"\/Date('+startDate+'-0000)\/","endDate":"\/Date('+endDate+'-0000)\/"},"userType":' +g_userType.number+ '}',success);
	}
	
	function getEvents(data, success) {
				var _this = this;
				postRequest(services.GetEvents, data,
						function(json, textStatus, jq) {
					success(json.d);
				});
	}

	function getByVINSuffix(vinSuffix) {
		if (vinSuffix.length == 6) {
			document.getElementById("vehicleList").innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:20px; margin-bottom:20px'/>";
			showOnly("vlistContainer");


			// TODO: Potentially awkward assumption here...
			// If we already have a vehicle that matches the suffix, then no need
			// to re-query the DB.  The awkwardness arrives if, say, we had a list of vehicles returned
			// but the one we really wanted wasn't included because, for example, it hadn't been put into
			// the right state.  The vehicle state is modified and we want to re-query the database to get
			// a new list, but it won't make the call because we already have the list.
			// This is an unlikely scenario.  To work around it, just refresh the page, or query 
			// another 6 digits string and then query this one again.
			if ((this.list.length == 0) || (this.list[0].vin.substr(-6) != vinSuffix)) {
				var _this = this;
				postRequest(services.GetVehiclesByVINSuffix, '{"vinSuffix":"' + vinSuffix + '", "userType":' +g_userType.number+ '}',
						function(json, textStatus, jq) {
					_this.init();
					if (json.d.length>0) {		// If vehicles were returned, show them.
						_this.addList(eval('('+jq.responseText+')').d);
						_this.showList();
					} else {
						showError("No matching vehicles found.");
					}
				});
			} else { 
				this.showList();
			}
		}else {
			showError("Incorrect number of digits.<br/>Please enter the last 6 digits of the VIN.");
		}
	}
	
	function getList() {
		var _this = this;
		postRequest(services.GetVehicles, null,
				function(json, textStatus, jq) {
			_this.init();
			_this.addList(eval('('+jq.responseText+')').d);
			_this.showList();			
		});
	}

	function addList(vlist) {
		var color = "";
		var v;

		for (var i in vlist) {
			v = vlist[i];

			// Number of vehicleInstances will only be > 0 for demo purposes
			for (var j=0; j<(g_userType.behaviors.vehicleInstances); j++) {
				v.vehicleNumber = this.list.length; 
				this.list[v.vehicleNumber] = new Vehicle(v, j);
				
				// HACK: Go straight to map
				if (!isVisible("map") && (this.list[i].locations.list.length > 0)) 
				{
					this.list[i].locations.list[0].display();
				}
			}
		}
		
		// If we have < 2 vehicles, don't need the next & previous arrows in the vehicle selector.
		if (vlist.length < 2) {
			$("#vSelPrev").hide();
			$("#vSelNext").hide();		
		}
	}

	function deprecated_add(v) {
		var vehicleNumber = this.list.length; 
		this.list[vehicleNumber] = new Vehicle(v);
//		this.list[vehicleNumber] = new Vehicle(vehicleNumber, vin,icon,name,year,make,model,lastIgnStatus,lastTime,lastAddress,quickFenceStatus,null,[], currentPlan);
	}

	function showList() {
		// If help screen or map are open , leave them up
		if ((!isVisible("help")) && (!isVisible("map"))) {
			showOnly("vlistContainer");
		}

		setvListDimensions();
	}

	function showVehicle(vehicleDelta) {
		var newVehicle = this.currentVehicleNumber() + vehicleDelta;
		if (newVehicle<0)
		{
			newVehicle=this.list.length-1;
		} else if (newVehicle>this.list.length-1) {
			newVehicle = 0;
		}

		// Remove any history markers that have been left behind.
		this.getCurrentVehicle().clearAllMarkers();
	
		// Now set the vehicle to the new one.
		this.setCurrentVehicle(newVehicle);

		v = this.getCurrentVehicle();

		// If we have loaded locations for this vehicle, then plot the most recent
		if (v.locations.list.length > 0) {
			v.locationNumber = 0;
			v.locations.list[0].plot();

			show("btnSettings");
			show("btnHistory");
		} else {
			hide("btnSettings");
			hide("btnHistory");
			g_infowindowVisible = false;
			g_infowindow.close();
		}
	
		$("#vSelector").show();
		$("#span_vName").html(this.getCurrentVehicle().name);
		showPromo(); // Make sure we show the promo associated with the selected vehicle's dealer.
		if (!this.getCurrentVehicle().plans.protectPlanIsCurrent) {
			$("#vehicleInfo").html(this.getCurrentVehicle().htmlVehicleTable());	
		} else {
			$("#vehicleInfo").html("");						
		}
		
		// Refresh the history panel
		refreshHistoryPanel();
		
		// And the control panel
		refreshCtrlPanel();

		// And the account panel
		refreshAcctPanel();
	}
	
	function getID(vin) {
		for (var i=0; i<this.list.length; i++)
		{
			if (this.list[i].vin == vin) break
		}

		return (i<this.list.length ? i : -1);
	}

	function getDashboards() {
		for (var v in this.list) {
			this.list[v].getDashboard();
		}
	}
	
}





/* *************** Dashboard ******************
{
"d": {
    "__type": "DashBoardInfo:#SkyLinkWCFService.DataTypes",
    "BatteryLevelSetting": null,
    "CurrentUserInfomation": {
        "__type": "SkyLinkUserInformation:http://www.mysky-link.com/skylink",
        "AlternateEmail": "",
        "CanAddVehicles": true,
        "CanManageUsers": true,
        "CanRenewSubscription": true,
        "CellPhone": "602-614-2034",
        "CellProvider": "T-Mobile",
        "City": "Phoenix",
        "EmailAddress": "godonnell@inilex.com",
        "EmailNotifications": 63,
        "FirstName": "Phil",
        "Id": 3119,
        "LastName": "DeCarlo",
        "PIN": 5565,
        "PostalCode": "85008",
        "PrimaryPhone": "480-555-1212",
        "SecretQuestionAnswer": null,
        "SecretQuestionChoice": 5,
        "SmsNotifications": 15,
        "State": "AZ",
        "StreetAddress1": "4908 E McDowell Rd",
        "StreetAddress2": "",
        "Suite": null
    },
    "GeoFences": [],
    "InsuranceCard": {
        "__type": "InsuranceCard:#SkyLinkWCFService.DataTypes",
        "AgentName": "",
        "AgentPhoneNumber": "",
        "Dealership": {
            "__type": "DealerShip:#SkyLinkWCFService.DataTypes",
            "City": "Shawnee Mission",
            "CompanyName": "Showcase Honda",
            "DealerId": 41,
            "PhoneNumber": "(602) 274-3800",
            "PostalCode": "66201",
            "State": "KS",
            "StreetAddress1": "PO Box 634",
            "StreetAddress2": "",
            "Suite": "",
            "URL": "www.showcasehonda.com"
        },
        "InsuranceCarrier": "",
        "OwnerInformation": {
            "__type": "SkyLinkUserInformation:http://www.mysky-link.com/skylink",
            "AlternateEmail": "",
            "CanAddVehicles": true,
            "CanManageUsers": true,
            "CanRenewSubscription": true,
            "CellPhone": "602-614-2034",
            "CellProvider": "T-Mobile",
            "City": "Phoenix",
            "EmailAddress": "godonnell@inilex.com",
            "EmailNotifications": 63,
            "FirstName": "Phil",
            "Id": 3119,
            "LastName": "DeCarlo",
            "PIN": 5565,
            "PostalCode": "85008",
            "PrimaryPhone": "480-555-1212",
            "SecretQuestionAnswer": null,
            "SecretQuestionChoice": 5,
            "SmsNotifications": 15,
            "State": "AZ",
            "StreetAddress1": "4908 E McDowell Rd",
            "StreetAddress2": "",
            "Suite": null
        },
        "PolicyNumber": "",
        "Vehicle": {
            "__type": "Vehicle:http://mysky-link.com/skylink",
            "batteryAlertLevel": null,
            "Color": "SILVER",
            "DeviceSerial": "INILEX_TEST0026",
            "Make": "Honda ",
            "Model": "ACCORD",
            "Name": null,
            "SkyLinkPlan": [
                {
                    "__type": "LinkSubscriptionPlan:http://mysky-link.com/skylink",
                    "Description": "Default SVR Plan",
                    "ExpirationDate": "/Date(1577862000000-0700)/",
                    "IsActive": false,
                    "Name": "SkyLink SVR"
                },
                {
                    "__type": "LinkSubscriptionPlan:http://mysky-link.com/skylink",
                    "Description": "Provides vehicle tracking, trip history and Geofence services in addition to stolen vehicle recovery protection",
                    "ExpirationDate": "/Date(1577862000000-0700)/",
                    "IsActive": false,
                    "Name": "SkyLink Advantage"
                }
            ],
            "Vin": "1FTFX1EF3BFC72437",
            "Year": 2003,
            "isBatteryAlertEnabled": false,
            "isQuickFenceEnabled": false,
            "isQuickFenceViolated": false,
            "isSpeedAlertSet": false
        }
    },
    "IsBatteryAlarmed": false,
    "IsQuickFenceSet": false,
    "IsQuickFenceViolated": false,
    "IsSpeedAlarmed": false,
    "LastLocation": {
        "__type": "LocationPoint:http://mysky-link.com/skylink",
        "Lat": 18.234121322631836,
        "Lon": -66.04850006103516,
        "AlarmCleared": true,
        "City": "Caguas",
        "Country": "US",
        "County": "",
        "Heading": 0,
        "Id": 3991280,
        "IgnitionOn": false,
        "LocationAlarmIndex": null,
        "LocationAlarmType": 0,
        "Speed": 0,
        "State": "PR",
        "StreetAddress": "Cll Cordova",
        "TimeStamp": "/Date(1303537202000-0700)/",
        "Zip": "87114"
    },
    "LatestTripsSummary": null,
    "NumberOfGeofencesSupported": 3,
    "SpeedAlertValue": null,
    "SupportedFenceTypes": [
        0,
        3,
        2
    ]
}
}

**********************Dashboard **************************/