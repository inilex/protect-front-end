var cart = null;

function toggleCtrlPanel() {
	if (isCtrlPanelOpen()) {
		if ($("#speedSettings").css("display") == "block") {
			$("#speedSettings").fadeOut(125, function () { $("#speedSettings").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#zoneSettings").css("display") == "block") {
			$("#zoneSettings").fadeOut(125, function () { vehicles.getCurrentVehicle().geofences.hide(); $("#zoneSettings").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#batterySettings").css("display") == "block") {
			$("#batterySettings").fadeOut(125, function () { $("#batterySettings").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#qfSettings").css("display") == "block") {
			$("#qfSettings").fadeOut(125, function () { $("#qfSettings").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#maintSettings").css("display") == "block") {
			$("#maintSettings").fadeOut(125, function () { $("#maintSettings").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#idleSettings").css("display") == "block") {
			$("#idleSettings").fadeOut(125, function () { $("#idleSettings").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#cartPanel").css("display") == "block") {
			$("#cartPanel").fadeOut(125, function () { clearCart(); $("#cartPanel").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#cartSuccessPanel").css("display") == "block") {
			$("#cartSuccessPanel").fadeOut(125, function () { clearCart(); $("#cartSuccessPanel").hide(); $("#vSettings").fadeIn(125); });
		} else if ($("#cartCheckoutPanel").css("display") == "block") {
			switch (cart.mode) {
				case 0: // Purchase
					$("#cartCheckoutPanel").fadeOut(125, function () { clearCartCard(); $("#cartCheckoutPanel").hide(); $("#cartPanel").fadeIn(125); });
					break;
				case 1:  // Update CC Info
					$("#cartCheckoutPanel").fadeOut(125, function () { clearCartCard(); $("#cartCheckoutPanel").hide(); hideCtrlPanel(); showAcctPanel(); });
					break;
			}
		} else {
			hideCtrlPanel();
		}
	} else {
		hideHistoryPanel();
		hideAcctPanel();
		showCtrlPanel();
	}
}

function isCtrlPanelOpen() {
	return ($("#ctrlPanel").css("width") >= "360px");
}

function refreshCtrlPanel() {
	if (isCtrlPanelOpen()) {
		adjustCtrlPanelPosition();

		if (((!vehicles.getCurrentVehicle().plans.protectPlanIsCurrent) ||	// If vehicle doesn't currently have a valid PROTECT plan
				(!g_accountUser.autoRenew)) || 								// OR the account is not set to auto-renew
				(!g_accountUser.canRenewSubscription)) {					// AND the account user is allowed to renew
			$("#menuCart").css("display", "block");							// Hide the Buy Service menu option.
		} else {
			$("#menuCart").css("display", "none");
		}

		if ($("#speedSettings").css("display") == "block") {
			initSpeedSettings();
		} else if ($("#zoneSettings").css("display") == "block") {
			initZoneSettings();
		} else if ($("#batterySettings").css("display") == "block") {
			initBatterySettings();
		} else if ($("#qfSettings").css("display") == "block") {
			initQuickFenceSettings();
		} else if ($("#cartPanel").css("display") == "block") {
			initCart();
		}
	}
}

function showCtrlPanel(panel) {
	if (panel == undefined) {
		var panel = "vSettings";
	}

	if (((!vehicles.getCurrentVehicle().plans.protectPlanIsCurrent) ||	// If vehicle doesn't currently have a valid PROTECT plan
			(!g_accountUser.autoRenew)) || 								// OR the account is not set to auto-renew
			(!g_accountUser.canRenewSubscription)) {					// AND the account user is allowed to renew
		$("#menuCart").css("display", "block");							// Hide the Buy Service menu option.
	} else {
		$("#menuCart").css("display", "none");
	}

	$("#btnCtrlPanel").attr("src", "./images/btn_back.png");
	adjustCtrlPanelPosition();
	$("#ctrlPanel").show();
	$("#" + panel).show();
	$("#ctrlPanel").animate({ width: "360px", height: $("#vSettings").height(), opacity: "0.85" }, 250);

	// If we are updating payment info show indicator above "Account" button.
	if (panel != "cartCheckoutLoading") {
		$("#menuIndicator").css("left", "115px").show();
	} else {
		$("#menuIndicator").css("left", "200px").show();
	}
}

function adjustCtrlPanelPosition() {
	$("#ctrlPanel").css("top", ($("#vSelector").height() + 5) + "px");
	$("#menuIndicator").css("top", ($("#vSelector").height() - 20) + "px");
}

function hideCtrlPanel() {
	$("#btnCtrlPanel").attr("src", "./images/btn_ctrlPanelOpen.png");
	$("#ctrlPanel").animate({ width: "0px", height: "0px", opacity: "0.0" }, 250, "linear", function () {
		$("#ctrlPanel").hide();
		$("#vSettings").hide();
		$("#zoneSettings").hide();
		$("#qfSettings").hide();
		$("#batterySettings").hide();
		$("#speedSettings").hide();
		$("#maintSettings").hide();
		$("#idleSettings").hide();

		$("#cartPanel").hide();
		$("#cartCheckoutPanel").hide();
		$("#cartSuccessPanel").hide();
	});
	$("#menuIndicator").hide();

	//		alert($("#ctrlPanel").css("width"));
}

//SPEED
function initSpeedSettings() {
	$("#maxSpeedSlider").slider({ step: g_userType.behaviors.speedThresholdStep });

	$("#maxSpeed").val(convertFromMeters(vehicles.getCurrentVehicle().speedAlertValue * 1000, "mph", 0));
	$("#maxSpeedSlider").slider("value", convertFromMeters(vehicles.getCurrentVehicle().speedAlertValue * 1000, "mph", 0));

	$('#vSettings').fadeOut(125, function () { $('#speedSettings').fadeIn(125); })

	$("#chk_smsSpdAlerts").prop("checked", (g_accountUser.smsNotifications & ConsumerNotifications.SpeedAlarm.value) > 0);
	$("#chk_emailSpdAlerts").prop("checked", (g_accountUser.emailNotifications & ConsumerNotifications.SpeedAlarm.value) > 0);
}

function updateMaxSpeed(maxSpeed) {
	if (maxSpeed < minSpeedThreshold) {
		maxSpeed = minSpeedThreshold;
	} else if (maxSpeed > maxSpeedThreshold) {
		maxSpeed = maxSpeedThreshold;
	}
	$("#maxSpeed").val(maxSpeed);
	$("#maxSpeedSlider").slider("value", maxSpeed);
}

function updateSpeedSettings() {
	vehicles.getCurrentVehicle().setSpeedAlert(parseInt(convertToMeters($("#maxSpeedSlider").slider("value"), "mph", 0) / 1000));
	g_accountUser.setNotifications(ConsumerNotifications.SpeedAlarm.value, $("#chk_emailSpdAlerts").is(":checked"), $("#chk_smsSpdAlerts").is(":checked"));
	toggleCtrlPanel();
}

//IDLE
function initIdleSettings() {
	$("#maxIdle").val(15);
	$("#maxIdleSlider").slider("value", 15);

	$('#vSettings').fadeOut(125, function () { $('#idleSettings').fadeIn(125); })

	$("#chk_smsIdleAlerts").prop("checked", true);
	$("#chk_emailIdleAlerts").prop("checked", true);

	$("#chk_smsIdleAlerts").prop("checked", (g_accountUser.smsNotifications & ConsumerNotifications.IdleAlarm.value) > 0);
	$("#chk_emailIdleAlerts").prop("checked", (g_accountUser.emailNotifications & ConsumerNotifications.IdleAlarm.value) > 0);
}

function updateMaxIdle(maxIdle) {
	if (maxIdle < minIdleTime) {
		maxIdle = minIdleTime;
	} else if (maxIdle > maxIdleTime) {
		maxIdle = maxIdleTime;
	}
	$("#maxIdle").val(maxIdle);
	$("#maxIdleSlider").slider("value", maxIdle);
}

function updateIdleSettings() {
	g_accountUser.setNotifications(ConsumerNotifications.IdleAlarm.value, $("#chk_emailIdleAlerts").is(":checked"), $("#chk_smsIdleAlerts").is(":checked"));
	toggleCtrlPanel();
}

//ZONES (GEOFENCES)
function initZoneSettings() {
	var v = vehicles.getCurrentVehicle();

	if (v.geofences == null) {
		v.geofences = new Geofences(v);
	} else {
		populateZones();
	}
}

function populateZones() {
	var v = vehicles.getCurrentVehicle();

	$("#chk_smsZoneAlerts").prop("checked", (g_accountUser.smsNotifications & ConsumerNotifications.ZoneAlarm.value) > 0);
	$("#chk_emailZoneAlerts").prop("checked", (g_accountUser.emailNotifications & ConsumerNotifications.ZoneAlarm.value) > 0);

	$('#vSettings').fadeOut(125, function () {
		$('#zoneSettings').fadeIn(125, function () {

			var g = v.geofences.list;

			for (var i = g.length - 1; i >= 0; i--) {
				$("#geoRadiusUnits" + g[i].id).html(convertFromMeters(g[i].radius, "mph", 2) == 1 ? " mile" : " miles");

				$("#geoName" + i).val(g[i].name);
				$("#geoAddress" + i).val(g[i].address);

				// Indicate whether the geofence is enabled or not - refresh is required to update the display
				(g[i].isEnabled ? $("#geoOnOff" + i + "_On").attr("checked", true) : $("#geoOnOff" + i + "_Off").attr("checked", true));
				//				$("#geoOnOff"+i).buttonset("refresh");

				// Set the triggerType 
				switch (g[i].triggerType) {
					case 0: $("#geoTrigger" + i + "_Entry").attr("checked", true);
						break;
					case 1: $("#geoTrigger" + i + "_Exit").attr("checked", true);
						break;
					case 2: $("#geoTrigger" + i + "_Trans").attr("checked", true);
						break;
				}

				// Only show geofence initially if it is defined.
				if ((!g[i].defaultFence) || (g[i].changed)) {
					g[i].showFence();
					show("geoRadiusPanel" + i);
				}

				// By default, GeoFences will be collapsed
				hide("geoWrapper" + i);

				$("#geoRadius" + g[i].id).val(convertFromMeters(g[i].radius, "mph", 2));
				$("#geoRadiusSlider" + g[i].id).slider({ value: convertFromMeters(g[i].radius, "mph", 2) });
			}
		});
	});
}

function onRadius0(value) {
	var g = vehicles.getCurrentVehicle().geofences.list[0];
	//	g.setRadius(value);
}

function onRadius1(value) {
	var g = vehicles.getCurrentVehicle().geofences.list[1];
	//	g.setRadius(value);
}

function onRadius2(value) {
	var g = vehicles.getCurrentVehicle().geofences.list[2];
	//	g.setRadius(value);
}

function updateZoneSettings() {
	vehicles.getCurrentVehicle().geofences.update();
	g_accountUser.setNotifications(ConsumerNotifications.ZoneAlarm.value, $("#chk_emailZoneAlerts").is(":checked"), $("#chk_smsZoneAlerts").is(":checked"));
	toggleCtrlPanel();
}

//QUICKFENCE
function initQuickFenceSettings() {
	// Change the label if this vehicle has a MOTO plan on it
	if (vehicles.getCurrentVehicle().plans.motoPlanIsCurrent) {
		$("#qfStatusLabel").html("Auto-arm QuickFence:");
	} else {
		$("#qfStatusLabel").html("QuickFence Status:");
	}

	vehicles.getCurrentVehicle().showQuickFenceStatus();
	$("#chk_smsQfAlerts").prop("checked", (g_accountUser.smsNotifications & ConsumerNotifications.EarlyTheftDetection.value) > 0);
	$("#chk_emailQfAlerts").prop("checked", (g_accountUser.emailNotifications & ConsumerNotifications.EarlyTheftDetection.value) > 0);
	$('#vSettings').fadeOut(125, function () { $('#qfSettings').fadeIn(125); })
}

function updateQuickFenceSettings() {
	if (vehicles.getCurrentVehicle().isQuickFenceEnabled != ($("#qfStatus_On").attr("checked") == "checked")) {
		// If this vehicle has a Moto plan on it, then toggle the auto-arm feature.
		if (vehicles.getCurrentVehicle().plans.motoPlanIsCurrent) {
			vehicles.getCurrentVehicle().toggleQFAutoArm();
		} else {
			vehicles.getCurrentVehicle().toggleQuickFence();
		}
	}
	g_accountUser.setNotifications(ConsumerNotifications.EarlyTheftDetection.value, $("#chk_emailQfAlerts").is(":checked"), $("#chk_smsQfAlerts").is(":checked"));
	toggleCtrlPanel();
}

//BATTERY ALERTS
function initBatterySettings() {
	vehicles.getCurrentVehicle().showBatteryAlertStatus();
	$("#chk_smsBatteryAlerts").prop("checked", (g_accountUser.smsNotifications & ConsumerNotifications.BatteryAlarm.value) > 0);
	$("#chk_emailBatteryAlerts").prop("checked", (g_accountUser.emailNotifications & ConsumerNotifications.BatteryAlarm.value) > 0);
	$('#vSettings').fadeOut(125, function () { $('#batterySettings').fadeIn(125); })
}

function updateBatterySettings() {
	if (vehicles.getCurrentVehicle().batteryAlertEnabled != ($("#batteryAlerts_On").attr("checked") == "checked")) {
		// If this vehicle has a Moto plan on it, then toggle the auto-arm feature.
		vehicles.getCurrentVehicle().setBatteryAlert(($("#batteryAlerts_On").attr("checked") == "checked"));
	}
	g_accountUser.setNotifications(ConsumerNotifications.BatteryAlarm.value, $("#chk_emailBatteryAlerts").is(":checked"), $("#chk_smsBatteryAlerts").is(":checked"));
	toggleCtrlPanel();
}

//CART
function promoCart(mode) {
	$("#vlistContainer").hide();
	$("#btnSettings").hide();
	$("#cartCheckoutPanel").hide();
	$("#cartSuccessPanel").hide();
	$("#cartLoadingPanel").hide();
	$("#cartPanel").hide();

	//cartVehicleNumber = vehicleNumber;

	if (!isVisible("map")) {
		// Center the promo panel
		$("#ctrlPanel").css("margin-left", "auto");
		$("#ctrlPanel").css("margin-right", "auto");
		$("#ctrlPanel").css("position", "relative");

		// Now make all close buttons go back to SkyLink
		$(".closeBtnControl").attr("onclick", "window.location='https://www.mysky-link.com/SkyLinkUserInfo.aspx';");
	}

	showCtrlPanel("cartPromoPanel");
}

function initCart(mode) {
	if (vehicles.getCurrentVehicle().plans.etdPlanIsCurrent) {
		$("#cartProductMessage").html("<h3>PROTECT Service Plan:</h3>" +
								  "<table><tr><td width='150px'>1 year service</td><td>$119</td></tr>" +
								  "<tr><td>2 year service</td><td>$199</td></tr>" +
								  "<tr><td>3 year service</td><td>$259</td></tr>" +
								  "<tr><td>4 year service</td><td>$299</td></tr>" +
								  "<tr><td>5 year service</td><td>$329</td></tr></table>" +
								  "<h4>As an ETD subscriber, your discounted price is:</h4>");
	} else {
		$("#cartProductMessage").html("<h3>Select the service to which you would like to subscribe:</h3>");
	}

	// Clear error message.
	$("#cartError").html("");

	cart = new Cart(mode);

	// Change panel title based on mode of operation
	switch (cart.mode) {
		case 0:	// Purchase
			$("#cartCheckoutTitle").html("Buy Service");
			break;
		case 1: 	// Update CC info
			$("#cartCheckoutTitle").html("Update Payment Info");
			break;
	}

	if (!g_accountUser.storeCC) {
		// If CC info was not saved, default to populating the address from the Account info.
		$("#cardFirstName").val(g_accountUser.firstName);
		$("#cardLastName").val(g_accountUser.lastName);
		$("#cardStreetAddress").val(g_accountUser.streetAddress1);
		$("#cardSuiteApt").val(g_accountUser.suite);
		$("#cardCity").val(g_accountUser.city);
		$("#cardState").val(g_accountUser.state);
		$("#cardZip").val(g_accountUser.postalCode);
		$("#chk_storeCC").prop("checked", true);
		$("#chk_autoRenew").prop("checked", true);
	}
}

function populateCCInfo() {
	// When credit card was saved, this method is used in the callback to populate as much info as possible.
	$("#cardFirstName").val(g_accountUser.ccInfo.firstName);
	$("#cardLastName").val(g_accountUser.ccInfo.lastName);
	$("#cardStreetAddress").val(g_accountUser.ccInfo.streetAddress1);
	$("#cardSuiteApt").val(g_accountUser.ccInfo.suite);
	$("#cardCity").val(g_accountUser.ccInfo.city);
	$("#cardState").val(g_accountUser.ccInfo.state);
	$("#cardZip").val(g_accountUser.ccInfo.postalCode);
	//	$("#cardNumber").val(g_accountUser.ccInfo.number);
	//	$("#cardNumber").val("");
	$("#cardReminder").html("<div style='font-size:0.8em;'><b>For your security, re-enter<br/>card number ending with:<br/>" + g_accountUser.ccInfo.number + "<br/></b></div>");
	$("#cardExpirationMonth").val(g_accountUser.ccInfo.expirationMonth);
	$("#cardExpirationYear").val(g_accountUser.ccInfo.expirationYear);
	$("#cardType").val(g_accountUser.ccInfo.cardType);
	$("#chk_storeCC").prop("checked", g_accountUser.storeCC);
	$("#chk_autoRenew").prop("checked", g_accountUser.autoRenew);
}

function clearCart() {
	$("#cardFirstName").val("");
	$("#cardLastName").val("");
	$("#cardStreetAddress").val("");
	$("#cardSuiteApt").val("");
	$("#cardCity").val("");
	$("#cardState").val("");
	$("#cardZip").val("");
	$("#couponDetail").html("");
	$("#couponCode").val("");
	clearCartCard();
}

function clearCartCard() {
	$("#cardType").val("");
	$("#cardNumber").val("");
	$("#cardExpirationMonth").val("");
	$("#cardExpirationYear").val("");
}

function changedCouponCode(couponCode) {
	cart.couponCode = couponCode;
}

//VEHICLE MAINTENANCE INTERVALS
function initVehMaintSettings() {
	var v = vehicles.getCurrentVehicle();

	// Get maintenance intervals defined on the account
	if (g_accountUser.maintIntervals == null) {
		g_accountUser.maintIntervals = new MaintIntervals(g_accountUser, function () { });
	}

	// Get maintenance intervals defined for the vehicle
	if (v.vehMaintIntervals == null) {
		v.vehMaintIntervals = new VehMaintIntervals(v);
	} else {
		populateVehMaintIntervals();
	}

}

// Populate maintenance intervals defined for the ACCOUNT
function populateMaintIntervals() {
	var v = vehicles.getCurrentVehicle();

	var m = g_accountUser.maintIntervals.list;
	var i = v.vehMaintIntervals.list;

	// Remove any current options
	$('#maintName0').find('option').remove()
	$('#maintName1').find('option').remove()
	$('#maintName2').find('option').remove()

	// Add default "none" option
	$('#maintName0').append(new Option("-- Select an interval --", null, true, true));
	$('#maintName1').append(new Option("-- Select an interval --", null, true, true));
	$('#maintName2').append(new Option("-- Select an interval --", null, true, true));

	//Add relevant options to the select fields
	for (var j = 0; j < m.length; j++) {
		if ((m[j].intervalID != i[0].id) &&
			(m[j].intervalID != i[1].id) &&
			(m[j].intervalID != i[2].id)) {
			$('#maintName0').append(new Option(m[j].name, m[j].intervalID, true, true));
			$('#maintName1').append(new Option(m[j].name, m[j].intervalID, true, true));
			$('#maintName2').append(new Option(m[j].name, m[j].intervalID, true, true));
		} else {
			if (i[0].id == m[j].intervalID) {
				$('#maintName0').append(new Option(m[j].name, m[j].intervalID, true, true));
			} else if (i[1].id == m[j].intervalID) {
				$('#maintName1').append(new Option(m[j].name, m[j].intervalID, true, true));
			} else if (i[2].id == m[j].intervalID) {
				$('#maintName2').append(new Option(m[j].name, m[j].intervalID, true, true));
			}
		}
	}
}

// Populate maintenance intervals defined for the VEHICLE
function populateVehMaintIntervals() {
	populateMaintIntervals();

	$("#ctrlPanel").css("width", "400px");
	$('#vSettings').fadeOut(125, function () { $('#maintSettings').fadeIn(125); })

	var v = vehicles.getCurrentVehicle();
	var i = v.vehMaintIntervals.list;

	for (var j = i.length - 1; j >= 0; j--) {
		$("#maintName" + j).val(i[j].id);
		$("#maintDueIn" + j).html(i[j].due.date != null || i[j].due.odometer != null || i[j].due.hours != null ?
								"Percent Due: " + Math.max(i[j].duePercentage.days, i[j].duePercentage.odometer, i[j].duePercentage.hours) + "%" : "");
		$("#maintDetail" + j).html(buildVehMaintTable(i[j]));
	}
}

function buildVehMaintTable(interval) {
	return (interval.due.date != null || interval.due.odometer != null || interval.due.hours != null ?
			"<table><tr><td></td><td><b>Current</b></td><td><b>Due</b></td><td><b>Due In</b></td><td><b>%Due</b></td></tr>" +
				(interval.due.date != null ? "<tr><td><b>Date</b></td><td>" + formatDate(interval.recorded.date, "short", false) + "</td><td>" + formatDate(interval.due.date, "short", false) + "</td><td>" + (Math.round((getDate(interval.due.date) - getDate(interval.recorded.date)) / (60 * 60 * 24 * 10)) / 100) + "</td><td>" + interval.duePercentage.days + "%</td></tr>" : "") +
				(interval.due.odometer != null ? "<tr><td><b>Miles</b></td><td>" + interval.recorded.odometer + "</td><td>" + interval.due.odometer + "</td><td>" + (interval.due.odometer - interval.recorded.odometer) + "</td><td>" + interval.duePercentage.odometer + "%</td></tr>" : "") +
				(interval.due.hours != null ? "<tr><td><b>Hours</b></td><td>" + interval.recorded.hours + "</td><td>" + interval.due.hours + "</td><td>" + Math.round(100 * (interval.due.hours - interval.recorded.hours)) / 100 + "</td><td>" + interval.duePercentage.hours + "%</td></tr>" : "") +
			"<table" : "");
}

function setVehMaintFocus(id) {
	if (id == 0) {
		$("#maintMore0").html("(...less)");
		document.getElementById("maintMore0").onclick = function () { loseVehMaintFocus(0); }
		//		$("#maintMore0").unbind("click");		// Remove current click events
		//		$("#maintMore0").click(function () { "loseVehMaintFocus(0);" });
		show("maintWrapper0");
	} else {
		loseVehMaintFocus(0);
	}

	if (id == 1) {
		$("#maintMore1").html("(...less)");
		document.getElementById("maintMore1").onclick = function () { loseVehMaintFocus(1); }
		//		$("#maintMore1").unbind("click");		// Remove current click events
		//		$("#maintMore1").click(function () { loseVehMaintFocus(1); });
		show("maintWrapper1");
	} else {
		loseVehMaintFocus(1);
	}

	if (id == 2) {
		$("#maintMore2").html("(...less)");
		document.getElementById("maintMore2").onclick = function () { loseVehMaintFocus(2); }
		//		$("#maintMore2").unbind("click");		// Remove current click events
		//		$("#maintMore2").click(function () { loseVehMaintFocus(2); });
		show("maintWrapper2");
	} else {
		loseVehMaintFocus(2);
	}
}

function loseVehMaintFocus(id) {
	if (id == 0) {
		$("#maintMore0").html("(more...)");
		//		$("#maintMore0").unbind("click");		// Remove current click events
		//		$("#maintMore0").click("setVehMaintFocus(0)");
		document.getElementById("maintMore0").onclick = function () { setVehMaintFocus(0); }
		hide("maintWrapper0")
	}

	if (id == 1) {
		$("#maintMore1").html("(more...)");
		//		$("#maintMore1").unbind("click");		// Remove current click events
		//		$("#maintMore1").click("setVehMaintFocus(1)");
		document.getElementById("maintMore1").onclick = function () { setVehMaintFocus(1); }
		hide("maintWrapper1")
	}

	if (id == 2) {
		$("#maintMore2").html("(more...)");
		//		$("#maintMore2").unbind("click");		// Remove current click events
		//		$("#maintMore2").click("setVehMaintFocus(2)");
		document.getElementById("maintMore2").onclick = function () { setVehMaintFocus(2); }
		hide("maintWrapper2")
	}
}

function setVehMaintValue(intervalIndex, value) {
	if ((vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].id != null) && (vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].id != value)) {
		var state = [{
			html: "You are changing a maintenance interval associated with this vehicle.<br/><br/>" +
			"Do you wish to:<br/><ul><li>Log that the maintenance was performed;</li>" +
			"<li>Discard the existing maintenance interval and start a new one;</li>" +
			"<li>Cancel the change?</li></ul>",
			focus: 1,
			buttons: { "Log Maintenance": 1, "Discard Maintenance": 2, "Cancel": 0 },
			position: { container: ("#maintName" + intervalIndex), x: $("#maintName" + intervalIndex).width() + 5, y: -15, width: 500, arrow: "lt" },
			submit: function (e, v, m, f) {
				finalizeVehMaintValue(intervalIndex, value, v);
			}
		}];

		$.prompt(state);
	} else {
		finalizeVehMaintValue(intervalIndex, value, 2);	// Action of 2 means don't create a log entry, just make the change.
	}
}

function finalizeVehMaintValue(intervalIndex, value, action) {
	switch (action) {
		case 0: // Cancel (revert to prior value)
			$("#maintName" + intervalIndex).val(vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].id);
			break;
		case 1:	// Log maintenance
			vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].log(value);		// Perform reset - TODO: Consider performing some validation here. Although current UI design should hopefully prevent dups etc. from being selected, it's still good practice.
			break;
		case 2: // Discard maintenance
			vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].set(value);		// Set new maintenance interval...
			updateVehMaintSettings();															// ...and update in the database. - TODO: Consider adding call to update() to set() method.
			break;
	}
}

function updateVehMaintSettings() {
	//TODO: Validation - no duplicate intervals

	vehicles.getCurrentVehicle().vehMaintIntervals.update();
}

function resetVehicleMaintenanceInterval(intervalIndex) {
	var state = [{
		html: "You have chosen to reset a maintenance interval associated with this vehicle.<br/><br/>" +
		"Do you wish to:<br/><ul><li>Log that the maintenance was performed;</li>" +
		"<li>Discard the existing maintenance interval and start a new one;</li>" +
		"<li>Cancel the change?</li></ul>",
		focus: 1,
		buttons: { "Log Maintenance": 1, "Discard Maintenance": 2, "Cancel": 0 },
		position: { container: ("#maintName" + intervalIndex), x: $("#maintName" + intervalIndex).width() + 5, y: -15, width: 500, arrow: "lt" },
		submit: function (e, v, m, f) {
			finalizeResetVehMaintInterval(intervalIndex, v);
		}
	}];

	$.prompt(state);
}

function finalizeResetVehMaintInterval(intervalIndex, action) {
	switch (action) {
		case 0: // Cancel (do nothing)
			break;
		case 1:	// Log maintenance
			vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].log();			// Log maintenance as being performed and perform reset.
			break;
		case 2: // Discard maintenance
			vehicles.getCurrentVehicle().vehMaintIntervals.list[intervalIndex].log();			// Perform reset without logging maintenance.  TODO: Does this make sense?  Should we record SOMETHING to say we performed this action? Either way, this call WILL log maintenance, so some changes required anyway.
			break;
	}
}