<script type="text/javascript" src="js/historyPanel.js"></script>
<link rel="stylesheet" type="text/css" href="css/ctrlPanel.css" />

<script type="text/javascript" src="addins/tableScroll/jquery.tablescroll.js"></script>
<link rel="stylesheet" type="text/css" href="addins/tableScroll/jquery.tablescroll.css" />

<script type="text/javascript" src="addins/datatables/js/jquery.datatables.js"></script>
<link rel="stylesheet" type="text/css" href="addins/datatables/css/jquery.datatables.css" />

<script type="text/javascript" src="addins/tableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" src="addins/tableTools/media/js/TableTools.js"></script>
<link rel="stylesheet" type="text/css" href="addins/tableTools/media/css/TableTools_JUI.css" />

<div id="historyPanel" class="ctrlPanel">
	<div id="history" class="subPanel" style="display:block">
	<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="hideHistoryPanel();"/>
	<div id="historySub" class="address" style="width:97%">
		<div id="tabs" style="background-color:transparent">
			<ul>
				<li><a href="#tabs-1" id="tab1">Events</a></li>
				<li><a href="#tabs-2" id="tab2">Trips</a></li>
			</ul>
			<div id="tabs-1">	
				<table id="eventTable" class="display" border=0 style="color:000">
					<thead><tr><td>Time</td><td>Event Type</td><td>Address</td></tr></thead>
					<tbody>
					<tr><td></td><td align="center"><img src='images/loading.gif' id="aloading" alt='Loading...'/></td><td></td></tr>
					</tbody>
				</table>			
			</div>
			<div id="tabs-2">
				<table id="tripTable" class="display" border=0 style="color:000">
					<thead><tr><td width="30%">Start Time</td><td width="5%">Start Address</td><td width="5%">End Time></td><td width="5%">End Address</td><td width="5%">Miles</td><td>Max MPH</td><td width="20%">Duration</td></tr></thead>
					<tbody>
					<tr><td></td><td></td><td></td><td align="center"><img src='images/loading.gif' id="aloading" alt='Loading...'/></td><td></td><td></td><td></td></tr>
					</tbody>
				</table>
			</div>
		</div>
		<br/>
		<div>
			<input type="checkbox" id="chk_emailSummaries" value="summariesEmail" onchange="updateSummaries();" /> Send me monthly summaries by email<br />
		</div>
	</div>
	<br/>
	<div id="histPromo1"></div>
	</div>
</div>


<script>
	// EVENT TABLE
	$('#eventTable').dataTable({
	  "sScrollY":"200px",
	  "bPaginate":false,
	  "aoColumnDefs":[
	   {"aTargets": [ 0 ],
	   "mDataProp": function ( source, type, val ) {
    		if (type === 'set') {
          	  source[0] = val;
	          return;
	        }
	        else if (type === 'display') {
	          return formatDate(source[0],"long",true);
	        }
	        else if (type === 'filter') {
	          return formatDate(source[0], "long", true);
	        }
	        // 'sort', 'type' and undefined all just use the base type
	        return source[0];
	    }},
	  	{"sClass":"dataTableColumnCenter", "aTargets": [ 1 ]}
	  ],
	  "aaSorting":[[0,'desc']],
	  "sDom": '<"#eFilterContainer"T<"clear"><"#eventFilter">f>lrtip',
	  "oTableTools": {
			"sSwfPath": "addins/tableTools/media/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"print",
				{
				"sExtends": "copy",
				"sButtonText": "Copy",
				"mColumns": "all"
				},
				{
				"sExtends": "pdf",
				"sButtonText": "Export to PDF",
				"mColumns": "all"
				},
				{
				"sExtends": "xls",
				"sButtonText": "Export to Excel",
				"mColumns": "all"
				}
			] 
		}
	});


	// Add table filter
	$("#eFilterContainer").addClass("tableFilterContainer");
	$("#eventFilter").addClass("tableFilter");
	$("#eventFilter").html("<table><tr><td style='width:255px'>" +
			  "Date Range:<br><input type='text' id='eventsStartDate' style='width:80px' />&nbsp;to&nbsp;" +
			  "<input type='text' id='eventsEndDate' style='width:80px' />" +
			"</td><td>" +
			  "Show Only:<br/><input type='radio' name='eFilter' value='all' checked onclick='getEvents();'>All&nbsp;&nbsp;"  +
			  "<input type='radio' name='eFilter' value='speedAlerts' onclick='getSpeedAlerts();'>Speed Alerts&nbsp;&nbsp;" +
			  "<input type='radio' name='eFilter' value='zoneAlerts' onclick='getZoneAlerts();'>Zone Alerts" +
			"</td></tr></table>" );


	// TRIP TABLE
	$('#tripTable').dataTable({
	  "sScrollY":"200px",
	  "bPaginate":false,
	  "aoColumnDefs":[
	   {"aTargets": [ 0 ],
	   "mDataProp": function ( source, type, val ) {
    		if (type === 'set') {
          	  source[0] = val;
	          return;
	        }
	        else if (type === 'display') {
	          return formatDate(source[0], "long", true);
	        }
	        else if (type === 'filter') {
	          return formatDate(source[0], "long", true);
	        }
	        // 'sort', 'type' and undefined all just use the base type
	        return source[0];
	    }},
	   {"aTargets": [ 2 ],
	   "mDataProp": function ( source, type, val ) {
    		if (type === 'set') {
          	  source[2] = val;
	          return;
	        }
	        else if (type === 'display') {
	          return formatDate(source[2], "long", true);
	        }
	        else if (type === 'filter') {
	          return formatDate(source[2], "long", true);
	        }
	        // 'sort', 'type' and undefined all just use the base type
	        return source[2];
	    }}
	  ],
	  "aaSorting":[[0,'desc']],
	  "sDom": '<"#tFilterContainer"T<"clear"><"#tripFilter">f>lrtip',
	  "oTableTools": {
			"sSwfPath": "addins/tableTools/media/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"print",
				{
				"sExtends": "copy",
				"sButtonText": "Copy",
				"mColumns": "all"
				},
				{
				"sExtends": "pdf",
				"sButtonText": "Export to PDF",
				"mColumns": "all"
				},
				{
				"sExtends": "xls",
				"sButtonText": "Export to Excel",
				"mColumns": "all"
				}
			] 
			
		}	  
	});

	$("#tFilterContainer").addClass("tableFilterContainer");
	$("#tripFilter").addClass("tableFilter");
	$("#tripFilter").html("<table><tr><td>Date Range:<br><input type='text' id='tripsStartDate' style='width:80px' />&nbsp;to&nbsp;" +
			  "<input type='text' id='tripsEndDate' style='width:80px' /></td></tr></table>" );


	$("#eventsStartDate").datepicker({minDate:null, maxDate:0, showButtonPanel:true, onSelect: function( selectedDate ) { 
		$( "#eventsEndDate" ).datepicker( "option", "minDate", selectedDate );
//		$( "#eventsEndDate" ).datepicker( "option", "maxDate", selectedDate+90 );
		if ($("#eventsEndDate").val() != "") {
			$("#tab1").click();
		}
	} });
	
	$("#eventsEndDate").datepicker({minDate:null, maxDate:0, showButtonPanel:true, onSelect: function( selectedDate ) {
//		$( "#eventsStartDate" ).datepicker( "option", "minDate", selectedDate-90 );
		$( "#eventsStartDate" ).datepicker( "option", "maxDate", selectedDate );
		if ($("#eventsStartDate").val() != "") {
			$("#tab1").click();
		}
	} });
	
	$("#tripsStartDate").datepicker({minDate:null, maxDate:0, showButtonPanel:true, onSelect: function( selectedDate ) {
		$( "#tripsEndDate" ).datepicker( "option", "minDate", selectedDate );
//		$( "#tripsEndDate" ).datepicker( "option", "maxDate", selectedDate+90 );
		if ($("#tripsEndDate").val() != "") {
			$("#tab2").click();
		}
	} });
	
	$("#tripsEndDate").datepicker({minDate:null, maxDate:0, showButtonPanel:true, onSelect: function( selectedDate ) {
//		$( "#tripsStartDate" ).datepicker( "option", "minDate", selectedDate-90 );
		$( "#tripsStartDate" ).datepicker( "option", "maxDate", selectedDate );
		if ($("#tripsEndDate").val() != "") {
			$("#tab2").click();
		}
	} });


	// TAB DEFINITIONS
	$('#tab1').on('click', function () {
		// Repopulate whatever was selected last time around.
		switch ($('input:radio[name=eFilter]:checked').val())
		{
		case "speedAlerts":
			getSpeedAlerts();
			break;
		case "zoneAlerts":
			getZoneAlerts();
			break;
		default:
			getEvents();
		}
	});

	$('#tab2').on('click', function () {
		getTrips();
	});
</script>