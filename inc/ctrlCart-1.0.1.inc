	<div id="cartPanel" style="width:480px;" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable" id="cartTable">
				<tr><td align="center" colspan=3>
					<table>
						<tr valign="middle"><td align="right"><img src="./images/menuCart.png" /></td><td align="left"><h2>Buy Service</h2></td></tr>
						<tr >
						<td id="couponTd" colspan=2 align="center" valign="middle">Coupon Code:<input type="text" id="couponCode" class="textEntry" onchange="changedCouponCode($('#couponCode').val());"/>
							<img style="vertical-align:middle;" id="btn_applyCoupon" src="./images/btn_apply.png" width="80px" onclick="if($('#couponCode').val() != '') { cart.getCouponOfferings($('#couponCode').val()); }"/>
						</td>
						</tr>
						<tr><td colspan=2 align="center"><span id="couponDetail"/></td></tr>		
						<tr><td align="center" colspan=2><span id="cartProductMessage" /></td></tr>
					</table>
				</td></tr>
				<tr height="20px"></tr>		
				<tr><td colspan=2><span style="color:red" id="cartError"></span></td></tr>
				<tr height="20px"></tr>
				<tr><td colspan=3><img id="btn_cartPanelSubmit" src="./images/btn_submit.png" width="100px" onclick="cart.initCheckout($('#couponCode').val());"/></td></tr>	
			</table>
		</div>
		<div id="ctrlPromo5"></div>
	</div>	

	<div id="cartCheckoutLoading" style="width:480px;" class="subPanel">
		<img src='images/loading.gif' id='loading' style='margin-top:5px; margin-bottom:5px'/>
	</div>
	
		<div id="cartPromoPanel" style="width:480px;" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="closeCart();"/>
		<div>
			<table class="subTable" id="cartPromoTable">
				<tr><td align="center" colspan=3>
					<table>
						<tr valign="middle"><td align="right"><img src="./images/menuTracking.png" /></td>
						<td align="left"><span style="font-size:0.9em"><b>Know what is happening to your<br/>vehicle by upgrading to</b></span><br/>
						<span style="font-size:1.6em"><b>SkyLINK PROTECT</b></span></td></tr>
					</table>
				</td></tr>
				<tr><td align="right"><img width=55px src="./images/menuTracking.png" /></td><td width="275px" align="left"><h4>Locate your vehicle on a map in real-time.</h4></td><td></td></tr>
				<tr><td></td><td align="right"><h4>Email and text notification when your vehicle is speeding.</h4></td><td align="left"><img width=55px src="./images/menuSpeed.png" /></td></tr>
				<tr><td align="right"><img width=55px src="./images/menuHistory.png" /></td><td align="left"><h4>Historical trip reports.</h4></td><td></td></tr>
				<tr><td></td><td align="right"><h4>Notifications when your vehicle crosses geographic zones (Geo-Fence).</h4></td><td align="left"><img width=55px src="./images/menuZones.png" /></td></tr>
				<tr><td align="right"><img width=55px src="./images/menuBattery.png" /></td><td align="left"><h4>Low battery notification.</h4></td><td></td></tr>
				<tr><td colspan=3><img src="./images/menuCart.png" onclick="initCart();"/></td></tr>
				<tr><td colspan=3>Click here to see how much<br/>you can save by upgrading NOW!</td></tr>

				<!--
				<tr><td align="right"><img width=70px src="./images/menuTracking.png" /></td><td align="left">Here be some verbiage related to Location tracking.</td><td></td></tr>
				<tr><td></td><td align="right"><img width=70px src="./images/menuSpeed.png" /></td><td align="left">Here be some verbiage related to Speed alerts.</td></tr>
				<tr><td align="right"><img width=70px src="./images/menuIgnition.png" /></td><td align="left">Here be some verbiage related to Ignition history.</td><td></td></tr>
				<tr><td></td><td align="right"><img width=70px src="./images/menuZones.png" /></td><td align="left">Here be some verbiage related to GeoFences.</td></tr>
				<tr><td align="right"><img width=70px src="./images/menuHistory.png" /></td><td align="left">Here be some verbiage related to History.</td><td></td></tr>
				<tr><td></td><td align="right"><img width=70px src="./images/menuETD.png" /></td><td align="left">Here be some verbiage related to ETD / QuickFence.</td></tr>
				<tr><td align="right"><img width=70px src="./images/menuBattery.png" /></td><td align="left">Here be some verbiage related to Battery alerts.</td><td></td></tr>
				<tr><td colspan=3><img src="./images/menuCart.png" onclick="initCart();"/></td></tr>
				<tr><td colspan=3><h4>Click here to see how much<br/>you can save by upgrading NOW!</h4></td></tr>
				-->
			</table>
		</div>
		<div id="ctrlPromo5"></div>
	</div>	
	
	
	<div id="cartCheckoutPanel" style="width:480px;" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table id="cartTable" class="subTable">
				<tr><td align="center" colspan=3>
					<table><tr valign="middle"><td align="right"><img src="./images/menuCart.png" /></td><td align="left"><h2><span id="cartCheckoutTitle" /></h2></td></tr></table>
				</td></tr>
				<tr><td align="right">Name:</td><td align="left"><input style="width:80px" type="text" id="cardFirstName" class="textEntry"/>&nbsp;<input style="width:100px" type="text" id="cardLastName" class="textEntry"/>*</td></tr>
				<tr height="10px"></tr>
				<tr><td align="right">Address:</td><td align="left"><input type="text" id="cardStreetAddress" class="textEntry"/>*</td></tr>
				<tr><td align="right">Unit/Suite/Apt:</td><td align="left"><input type="text" id="cardSuiteApt" class="textEntry"/></td></tr>
				<tr><td align="right">City:</td><td align="left"><input type="text" id="cardCity" class="textEntry"/>*</td></tr>
				<tr><td align="right">State:</td><td align="left">
				  <select id="cardState" name="cardState">
					<!--#include file="states.inc" -->
				  </select>*
				</td></tr>
				<tr><td align="right">ZIP:</td><td align="left"><input style="width:80px" type="text" id="cardZip" class="textEntry"/>*</td></tr>
				<tr height="10px"></tr>
				<tr><td align="right">Card Type:</td><td align="left">
					<select id="cardType" name="cardType">
	  					<option value="1" >Visa</option>
	  					<option value="3" >Mastercard</option>
	  					<option value="2" >American Express</option>
					</select>
				*</td></tr>
				<tr><td align="right" valign="top">Card Number:</td><td align="left"><input type="text" id="cardNumber" class="textEntry"/>*<span id="cardReminder" /></td></tr>
				<tr><td align="right">Expiration Date:</td><td align="left"><input style="width:20px" type="text" id="cardExpirationMonth" class="textEntry"/>&nbsp;/&nbsp;<input style="width:20px" type="text" id="cardExpirationYear" class="textEntry"/>* (MM/YY)</td></tr>
				<tr height="10px"></tr>
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_autoRenew" value="cardAutoRenew" onclick='$("#chk_storeCC").prop("checked",$("#chk_autoRenew").prop("checked") || $("#chk_storeCC").prop("checked"));'/> Automatically renew subscription<br/>(requires card to be stored)<br />
						<input type="checkbox" id="chk_storeCC" value="cardStoreCC" /> Store this credit card for future use<br />
				</div>
				</td></tr>
				<tr height="10px"></tr>
				<tr><td colspan=2><span style="color:red" id="cardError"></span></td></tr>
				<tr height="10px"></tr>
				<tr><td colspan=3><img id="btn_cartSubmit" src="./images/btn_submit.png" width="100px" onclick="cart.submitBillingInfo();"/></td></tr>	
			</table>
		</div>
	</div>

	<div id="cartSuccessPanel" style="width:480px;" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
		  <table id="cartSuccessTable" class="subTable">
			<tr><td align="center" colspan=3>
				<table><tr valign="middle"><td align="right"><img src="./images/menuCart.png" /></td><td align="left"><h2><span id="cartSuccessTitle" /></h2></td></tr></table>
			</td></tr>
			<tr><td colspan=2><b><span id="cartSuccessMsg" /></b></td></tr>
			<tr height="10px"></tr>
			<tr height="10px"></tr>
			<tr><td colspan=2><span id="cartThankYouMsg" /></td></tr>
			<tr height="10px"></tr>
		  </table>
		</div>
	</div>
