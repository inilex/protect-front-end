	<div id="zoneSettings" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable" >
				<tr valign="middle"><td align="right"><img src="./images/menuZones.png" /></td><td align="left"><h2>Zone Alerts</h2></td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">This Vehicle</span>
			<table class="subTable" >				
				<tr><td colspan=2>
					<div id="geofence0" class="geofence" onclick="vehicles.getCurrentVehicle().geofences.list[0].setFocus();">
						<div class="geoBkgnd">
							<input type="text" id="geoName0" onchange="vehicles.getCurrentVehicle().geofences.list[0].setName(this.value);" class="geoValueHdr"/><br/>
							<div id="geoOnOff0" class="geoValue">
								<input type="radio" id="geoOnOff0_On" name="geoOnOff0" onclick="vehicles.getCurrentVehicle().geofences.list[0].setEnabled(true);"/><label for="geoOnOff0_On">On</label>
								<input type="radio" id="geoOnOff0_Off" name="geoOnOff0" onclick="vehicles.getCurrentVehicle().geofences.list[0].setEnabled(false);"/><label for="geoOnOff0_Off">Off</label>
							</div>
						</div>
						<div id="geoWrapper0" class="geoWrapper">
							<div class="geoBkgnd">
								<textarea rows="1" id="geoAddress0" onchange="vehicles.getCurrentVehicle().geofences.list[0].moveToAddress(this.value); " class="geoValue"></textarea>
								<div id="geoTrigger0" class="geoValue">
									<input type="radio" id="geoTrigger0_Entry" name="geoTrigger0" onclick="vehicles.getCurrentVehicle().geofences.list[0].setTriggerType(0);"/><label for="geoTrigger0_Entry">Entry</label>
									<input type="radio" id="geoTrigger0_Exit" name="geoTrigger0" onclick="vehicles.getCurrentVehicle().geofences.list[0].setTriggerType(1);"/><label for="geoTrigger0_Exit">Exit</label>
									<input type="radio" id="geoTrigger0_Trans" name="geoTrigger0" onclick="vehicles.getCurrentVehicle().geofences.list[0].setTriggerType(2);"/><label for="geoTrigger0_Trans">Transition</label>
								</div>
							</div>
							<div class="geoBkgnd">
								<div class="geoValue" id="geoRadiusPanel0" style="text-align:center;">
										<label for="amount">Radius <input type="text" id="geoRadius0" style="width:50px; font-weight:bold;" onchange="vehicles.getCurrentVehicle().geofences.list[0].setRadius(value);"/> miles</label>
									<br/>	
									<div style="text-align:center; width:100%;" >
										<div id="geoRadiusSlider0" style="width:200px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</td></tr>
				<tr><td colspan=2 align="center">
					<div id="geofence1" class="geofence" onclick="vehicles.getCurrentVehicle().geofences.list[1].setFocus();">
						<div class="geoBkgnd">
							<input type="text" id="geoName1" onchange="vehicles.getCurrentVehicle().geofences.list[1].setName(this.value);" class="geoValueHdr"/>
							<div id="geoOnOff1" class="geoValue">
								<input type="radio" id="geoOnOff1_On" name="geoOnOff1" onclick="vehicles.getCurrentVehicle().geofences.list[1].setEnabled(true);"/><label for="geoOnOff1_On">On</label>
								<input type="radio" id="geoOnOff1_Off" name="geoOnOff1" onclick="vehicles.getCurrentVehicle().geofences.list[1].setEnabled(false);"/><label for="geoOnOff1_Off">Off</label>
							</div>						
						</div>
						<div id="geoWrapper1" class="geoWrapper">
							<div class="geoBkgnd">
								<textarea rows="1" id="geoAddress1" onchange="vehicles.getCurrentVehicle().geofences.list[1].moveToAddress(this.value);" class="geoValue"></textarea>
								<div id="geoTrigger1" class="geoValue">
									<input type="radio" id="geoTrigger1_Entry" name="geoTrigger1" onclick="vehicles.getCurrentVehicle().geofences.list[1].setTriggerType(0);"/><label for="geoTrigger1_Entry">Entry</label>
									<input type="radio" id="geoTrigger1_Exit" name="geoTrigger1" onclick="vehicles.getCurrentVehicle().geofences.list[1].setTriggerType(1);"/><label for="geoTrigger1_Exit">Exit</label>
									<input type="radio" id="geoTrigger1_Trans" name="geoTrigger1" onclick="vehicles.getCurrentVehicle().geofences.list[1].setTriggerType(2);"/><label for="geoTrigger1_Trans">Transition</label>
								</div>
								
							</div>
							<div class="geoBkgnd">
								<div class="geoValue" id="geoRadiusPanel1" style="text-align:center;">
									<label for="amount">Radius <input type="text" id="geoRadius1" style="width:50px; font-weight:bold;" onchange="vehicles.getCurrentVehicle().geofences.list[1].setRadius(value);"/> miles</label>
									<br/>	
									<div style="text-align:center; width:100%;" >
										<div id="geoRadiusSlider1" style="width:200px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</td></tr>
				
				<tr><td colspan=2>
				<div id="geofence2" class="geofence" onclick="vehicles.getCurrentVehicle().geofences.list[2].setFocus();">
						<div class="geoBkgnd">
							<input type="text" id="geoName2" onchange="vehicles.getCurrentVehicle().geofences.list[2].setName(this.value);" class="geoValueHdr"/><br/>
							<div id="geoOnOff2" class="geoValue">
								<input type="radio" id="geoOnOff2_On" name="geoOnOff2" onclick="vehicles.getCurrentVehicle().geofences.list[2].setEnabled(true);"/><label for="geoOnOff2_On">On</label>
								<input type="radio" id="geoOnOff2_Off" name="geoOnOff2" onclick="vehicles.getCurrentVehicle().geofences.list[2].setEnabled(false);"/><label for="geoOnOff2_Off">Off</label>
							</div>
						</div>
						<div id="geoWrapper2" class="geoWrapper">
							<div class="geoBkgnd">
								<textarea rows="1" id="geoAddress2" onchange="vehicles.getCurrentVehicle().geofences.list[2].moveToAddress(this.value);" class="geoValue"></textarea><br/>
								<div id="geoTrigger2" class="geoValue">
									<input type="radio" id="geoTrigger2_Entry" name="geoTrigger2" onclick="vehicles.getCurrentVehicle().geofences.list[2].setTriggerType(0);"/><label for="geoTrigger2_Entry">Entry</label>
									<input type="radio" id="geoTrigger2_Exit" name="geoTrigger2" onclick="vehicles.getCurrentVehicle().geofences.list[2].setTriggerType(1);"/><label for="geoTrigger2_Exit">Exit</label>
									<input type="radio" id="geoTrigger2_Trans" name="geoTrigger2" onclick="vehicles.getCurrentVehicle().geofences.list[2].setTriggerType(2);"/><label for="geoTrigger2_Trans">Transition</label>
								</div>
							</div>
							<div class="geoBkgnd">
								<div class="geoValue" id="geoRadiusPanel2" style="text-align:center;">
									<label for="amount">Radius <input type="text" id="geoRadius2" style="width:50px; font-weight:bold;" onchange="vehicles.getCurrentVehicle().geofences.list[2].setRadius(value);"/> miles</label>
									<br/>	
									<div style="text-align:center; width:100%;" >
										<div id="geoRadiusSlider2" style="width:200px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</td></tr>
				<tr><td colspan=2>Click on a zone above to edit settings</td></tr>
			</table>
		</div>

		<div class="subSubPanel">
			<span class="subSubTitle">All Vehicles</span>
			<table class="subTable" >		
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_smsZoneAlerts" value="zoneAlertsSMS" /> Notify me of violations by SMS<br />
						<input type="checkbox" id="chk_emailZoneAlerts" value="zoneAlertsEmail" /> Notify me of violations by email<br />
					</div>
				</td></tr>
			</table>
		</div>
		
		<table class="subTable">				
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="updateZoneSettings()"/></td></tr>
		</table>
		<div id="ctrlPromo4"></div>
	</div>
