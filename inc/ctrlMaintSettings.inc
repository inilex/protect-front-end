	<div id="maintSettings" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable" >
				<tr valign="middle"><td align="right"><img src="./images/menuMaintenance.png" /></td><td align="left"><h2>Maintenance Intervals</h2></td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">This Vehicle</span>
			<table class="subTable" >				
				<tr><td colspan=2>
					<div id="maint0" class="vehMaintInterval" onclick="//setVehMaintFocus(0);">
						<div class="maintBkgnd">
							<select id="maintName0" onchange="setVehMaintValue(0, this.value);" class="maintValueHdr"/>
								<option value="">--Select interval --</option>
							</select><br/>
							<div id="maintOnOff0" class="maintValue">
								<span id="maintDueIn0"></span>&nbsp;<span id="maintMore0" class="maintMore" onclick="setVehMaintFocus(0);">(more...)</span>
								<!-- >
								<input type="radio" id="maintOnOff0_On" name="maintOnOff0" onclick="//vehicles.getCurrentVehicle().maintInternals.list[0].setEnabled(true);"/><label for="geoOnOff0_On">On</label>
								<input type="radio" id="maintOnOff0_Off" name="maintOnOff0" onclick="//vehicles.getCurrentVehicle().vehMaintIntervals.list[0].setEnabled(false);"/><label for="geoOnOff0_Off">Off</label>
								<-->
							</div>
						</div>
						<div id="maintWrapper0" class="maintWrapper">
							<div class="maintBkgnd">
								<div id="vehMaintIntervalDef0" class="maintValue" style="text-align:center">
									<span id="maintDetail0"></span>
									<!-- br/><input type="checkbox" id="chk_resetMaint0" value="maintReset0" /> Reset interval counter<br / -->
									<img src="./images/btn_reset.png" width="80px" onclick="resetVehicleMaintenanceInterval(0)"/>
								</div>
							</div>
						</div>
					</div>
				</td></tr>
				
				<tr><td colspan=2 align="center">
					<div id="maint1" class="vehMaintInterval" onclick="//setVehMaintFocus(1);">
						<div class="maintBkgnd">
							<select id="maintName1" onchange="setVehMaintValue(1, this.value);" class="maintValueHdr"/>
								<option value="">--Select interval --</option>
							</select><br/>
							<div id="maintOnOff1" class="maintValue">
								<span id="maintDueIn1"></span>&nbsp;<span id="maintMore1" class="maintMore" onclick="setVehMaintFocus(1);">(more...)</span>
								<!-- >
								<input type="radio" id="maintOnOff1_On" name="maintOnOff1" onclick="//vehicles.getCurrentVehicle().geofences.list[1].setEnabled(true);"/><label for="geoOnOff1_On">On</label>
								<input type="radio" id="maintOnOff1_Off" name=",maintOnOff1" onclick="//vehicles.getCurrentVehicle().geofences.list[1].setEnabled(false);"/><label for="geoOnOff1_Off">Off</label>
								< -->
							</div>						
						</div>
						<div id="maintWrapper1" class="maintWrapper">
							<div class="maintBkgnd">
								<div id="vehMaintIntervalDef1" class="maintValue">
									<span id="maintDetail1"></span>
									<!-- br/><input type="checkbox" id="chk_resetMaint1" value="maintReset0" /> Reset interval counter<br / -->
									<img src="./images/btn_reset.png" width="80px" onclick="resetVehicleMaintenanceInterval(1)"/>
								</div>
							</div>
						</div>
					</div>
				</td></tr>
				
				<tr><td colspan=2>
				<div id="maint2" class="vehMaintInterval" onclick="//setVehMaintFocus(2);">
						<div class="maintBkgnd">
							<select id="maintName2" onchange="setVehMaintValue(2, this.value);" class="maintValueHdr"/>
								<option value="">--Select interval --</option>
							</select><br/>
							<div id="maintOnOff2" class="maintValue">
								<span id="maintDueIn2"></span>&nbsp;<span id="maintMore2" class="maintMore" onclick="setVehMaintFocus(2);">(more...)</span>
								<!-- >
								<input type="radio" id="maintOnOff2_On" name="maintOnOff2" onclick="//vehicles.getCurrentVehicle().geofences.list[2].setEnabled(true);"/><label for="geoOnOff2_On">On</label>
								<input type="radio" id="maintOnOff2_Off" name="maintOnOff2" onclick="//vehicles.getCurrentVehicle().geofences.list[2].setEnabled(false);"/><label for="geoOnOff2_Off">Off</label>
								< -->
							</div>
						</div>
						<div id="maintWrapper2" class="maintWrapper">
							<div class="maintBkgnd">
								<div id="vehMaintIntervalDef2" class="maintValue">
									<span id="maintDetail2"></span>
									<!-- br/><input type="checkbox" id="chk_resetMaint0" value="maintReset2" /> Reset interval counter<br / -->
									<img src="./images/btn_reset.png" width="80px" onclick="resetVehicleMaintenanceInterval(2)"/>
								</div>
							</div>
						</div>
					</div>
				</td></tr>
			</table>
		</div>

		<!--
		<div class="subSubPanel">
			<span class="subSubTitle">All Vehicles</span>
			<table class="subTable" >		
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_smsZoneAlerts" value="zoneAlertsSMS" /> Notify me of due services by SMS<br />
						<input type="checkbox" id="chk_emailZoneAlerts" value="zoneAlertsEmail" /> Notify me of due services by email<br />
					</div>
				</td></tr>
			</table>
		</div>
		
		<table class="subTable">				
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="updateVehMaintSettings()"/></td></tr>
		</table>
		-->
		<div id="ctrlPromo4"></div>
	</div>
