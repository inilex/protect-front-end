		<div id="batterySettings" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable">
				<tr valign="middle"><td align="right"><img src="./images/menuBattery.png" /></td><td align="left"><h2>Battery Alerts</h2></td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">This Vehicle</span>
			<table class="subTable">		
				<tr><td colspan=2>
					<div id="batteryAlerts">
					Send battery alerts:
						<input type="radio" id="batteryAlerts_On" name="batteryAlerts" "/><label for="batteryAlerts_On">On</label>
						<input type="radio" id="batteryAlerts_Off" name="batteryAlerts" "/><label for="batteryAlerts_Off">Off</label>
					</div>
					<br/>
				</td></tr>	
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">All Vehicles</span>
			<table class="subTable">			
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_smsBatteryAlerts" value="batteryAlertsSMS" /> Notify me of alerts by SMS<br />
						<input type="checkbox" id="chk_emailBatteryAlerts" value="batteryAlertsEmail" /> Notify me of alerts by email<br />
					</div>
				</td></tr>
			</table>
		</div>
		<table class="subTable">			
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="updateBatterySettings();"/></td></tr>
		</table>
		<div id="ctrlPromo6"></div>
	</div>
	