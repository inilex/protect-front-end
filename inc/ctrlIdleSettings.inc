	<div id="idleSettings" class="subPanel" style="text-align:center">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable">
				<tr valign="middle"><td align="right"><img src="./images/menuIdle.png" /></td><td align="left"><h2>Idle Alerts</h2></td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">This Vehicle</span>
			<table class="subTable">
				<tr><td colspan=2>
					<div id="batteryAlerts">
					Send idle alerts:
						<input type="radio" id="idleAlerts_On" name="idleAlerts" "/><label for="idleAlerts_On">On</label>
						<input type="radio" id="idleAlerts_Off" name="idleAlerts" "/><label for="idleAlerts_Off">Off</label>
					</div>
					<br/>
				</td></tr>	
				<!-- tr><td colspan=2>
					<label for="amount">Maximum idle time: <input type="text" id="maxIdle" style="width:50px; font-weight:bold;" onchange='updateMaxIdle(value)'/> mins</label>
					<p/>
					<div style="text-align:center; width:100%;" >
						<div id="maxIdleSlider" style="width:200px;"></div>
					</div>
					<br/>				
				</td></tr -->
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">All Vehicles</span>
			<table class="subTable">		
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_smsIdleAlerts" value="spdAlertsSMS" /> Notify me of violations by SMS<br />
						<input type="checkbox" id="chk_emailIdleAlerts" value="spdAlertsEmail" /> Notify me of violations by email<br />
					</div>
				</td></tr>
			</table>
		</div>
		<table class="subTable">		
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="updateIdleSettings()"/><br/></td></tr>
		</table>
		<div id="ctrlPromo2"></div>
	</div>
