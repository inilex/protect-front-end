	<div id="speedSettings" class="subPanel" style="text-align:center">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable">
				<tr valign="middle"><td align="right"><img src="./images/menuSpeed.png" /></td><td align="left"><h2>Speed Alerts</h2></td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">This Vehicle</span>
			<table class="subTable">
					<tr><td colspan=2>
					<label for="amount">Maximum speed: <input type="text" id="maxSpeed" style="width:50px; font-weight:bold;" onchange='updateMaxSpeed(value)'/> mph</label>
					<p/>
					<div style="text-align:center; width:100%;" >
						<div id="maxSpeedSlider" style="width:200px;"></div>
					</div>
					<br/>				
				</td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">All Vehicles</span>
			<table class="subTable">		
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_smsSpdAlerts" value="spdAlertsSMS" /> Notify me of violations by SMS<br />
						<input type="checkbox" id="chk_emailSpdAlerts" value="spdAlertsEmail" /> Notify me of violations by email<br />
					</div>
				</td></tr>
			</table>
		</div>
		<table class="subTable">		
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="updateSpeedSettings()"/><br/></td></tr>
		</table>
		<div id="ctrlPromo2"></div>
	</div>
