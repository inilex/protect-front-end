	<div id="qfSettings" class="subPanel">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleCtrlPanel();"/>
		<div>
			<table class="subTable">
				<tr valign="middle"><td align="right"><img src="./images/menuETD.png" /></td><td align="left"><h2>QuickFence</h2></td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">This Vehicle</span>
			<table class="subTable">
				<tr><td colspan=2>
					<div id="qfStatus">
					<label id="qfStatusLabel">QuickFence Status:</label>
						<input type="radio" id="qfStatus_On" name="qfStatus" "/><label for="qfStatus_On">On</label>
						<input type="radio" id="qfStatus_Off" name="qfStatus" "/><label for="qfStatus_Off">Off</label>
					</div>
					<br/>
				</td></tr>
			</table>
		</div>
		<div class="subSubPanel">
			<span class="subSubTitle">All Vehicles</span>
			<table class="subTable">				
				<tr><td colspan=2>
					<div>
						<input type="checkbox" id="chk_smsQfAlerts" value="qfAlertsSMS" /> Notify me of violations by SMS<br />
						<input type="checkbox" id="chk_emailQfAlerts" value="qfAlertsEmail" /> Notify me of violations by email<br />
					</div>
				</td></tr>
			</table>
		</div>
		<table class="subTable">				
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="updateQuickFenceSettings();"/></td></tr>
		</table>
		<div id="ctrlPromo5"></div>
	</div>
