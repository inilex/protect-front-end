	<div id="account" class="subPanel" style="text-align:center">
		<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="toggleAcctPanel();"/>
		<div>
			<table class="subTable" width="100%">
				<tr valign="middle">	
					<td align="right"><img src="./images/menuAccount.png" /></td><td align="left"><h2>My Account</h2></td>
				</tr>
			</table>
		</div>
		
		<div class="subSubPanel">
			<span class="subSubTitle">Account Owner</span>	
			<table class="subTable">
				<tr><td align="right">First Name:</td><td align="left"><input type="text" id="firstName" class="textEntry"/>*</td></tr>
				<tr><td align="right">Last Name:</td><td align="left"><input type="text" id="lastName" class="textEntry"/>*</td></tr>
				<tr height="10px"></tr>
				<tr><td align="right">Address:</td><td align="left"><input type="text" id="streetAddress1" class="textEntry" style="width:200px"/>*</td></tr>
				<tr><td align="right"></td><td align="left"><input type="text" id="streetAddress2" class="textEntry" style="width:200px"/></td></tr>
				<tr><td align="right">Unit/Suite:</td><td align="left"><input type="text" id="suite" class="textEntry"/></td></tr>
				<tr><td align="right">City:</td><td align="left"><input type="text" id="city" class="textEntry"/>*</td></tr>
				<tr><td align="right">State:</td><td align="left">
				  <select id="state" name="state">
					<!--#include file="states.inc" -->
				  </select>*
				</td></tr>
				<tr><td align="right">ZIP:</td><td align="left"><input type="text" id="postalCode" class="textEntry"/>*</td></tr>
				<tr height="10px"></tr>
				<tr><td align="right">Timezone:</td><td align="left">
				  <select name="timeZone" id="timeZone" class="dateEntry">
					<!--#include file="timezones.inc" -->
				  </select>*</td>
				</tr>
				<tr height="10px"></tr>
				<tr><td align="right">Phone:</td><td align="left"><input type="text" id="primaryPhone" class="textEntry"/></td></tr>
				<tr><td align="right">Cell:</td><td align="left"><input type="text" id="cellPhone" class="textEntry"/></td></tr>
				<tr><td align="right">Cell Provider:</td><td align="left"><select name="cellProvider" id="cellProvider"></select></td></tr>
				<tr height="10px"></tr>
				<tr><td align="right">Email:</td><td align="left"><input type="text" id="emailAddress" class="textEntry" style="width:200px"/>*</td></tr>
				<tr><td align="right">Confirm Email:</td><td align="left"><input type="text" id="emailConfirm" class="textEntry" style="width:200px"/>*</td></tr>
				<tr height="10px"></tr>
				<tr><td colspan=2><span style="color:red" id="customerError"></span></td></tr>
				<tr height="10px"></tr>
			</table>
		</div>

		<table class="subTable">
			<tr><td colspan=2><img src="./images/btn_submit.png" width="100px" onclick="acctPanelSubmit()"/><br/></td></tr>
		</table>
		<div id="acctPromo2"></div>
	</div>
