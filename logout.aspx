<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cs/logout.aspx.cs"
    Inherits="CurrentSubscriberLogin" %>
<%@ Register Assembly="RolloverButton" Namespace="LenderLink.Controlls" TagPrefix="pkp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SkyLINK</title>
    <link rel="shortcut icon" href="images/favicon.ico" />

	<link rel="apple-touch-icon" href="images/touch-icon-iphone.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/touch-icon-iphone4.png" />
    
	<link rel="stylesheet" type="text/css" href="css/protect.css" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
</head>

<body>
</body>
</html>
