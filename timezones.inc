	<option value="DatelineStandardTime">Dateline Standard Time</option>
	<option value="UTC_11">UTC-11</option>
	<option value="SamoaStandardTime">Samoa Standard Time</option>
	<option value="HawaiianStandardTime">Hawaiian Standard Time</option>
	<option selected="selected" value="AlaskanStandardTime">Alaskan Standard Time</option>

	<option value="PacificStandardTimeMexico">Pacific Standard Time (Mexico)</option>
	<option value="PacificStandardTime">Pacific Standard Time</option>
	<option value="USMountainStandardTime">US Mountain Standard Time</option>
	<option value="MountainStandardTimeMexico">Mountain Standard Time (Mexico)</option>
	<option value="MountainStandardTime">Mountain Standard Time</option>
	<option value="CentralAmericaStandardTime">Central America Standard Time</option>

	<option value="CentralStandardTime">Central Standard Time</option>
	<option value="CentralStandardTimeMexico">Central Standard Time (Mexico)</option>
	<option value="CanadaCentralStandardTime">Canada Central Standard Time</option>
	<option value="SAPacificStandardTime">SA Pacific Standard Time</option>
	<option value="EasternStandardTime">Eastern Standard Time</option>
	<option value="USEasternStandardTime">US Eastern Standard Time</option>

	<option value="VenezuelaStandardTime">Venezuela Standard Time</option>
	<option value="ParaguayStandardTime">Paraguay Standard Time</option>
	<option value="AtlanticStandardTime">Atlantic Standard Time</option>
	<option value="CentralBrazilianStandardTime">Central Brazilian Standard Time</option>
	<option value="SAWesternStandardTime">SA Western Standard Time</option>
	<option value="PacificSAStandardTime">Pacific SA Standard Time</option>

	<option value="NewfoundlandStandardTime">Newfoundland Standard Time</option>
	<option value="E__PERIOD__SouthAmericaStandardTime">E. South America Standard Time</option>
	<option value="ArgentinaStandardTime">Argentina Standard Time</option>
	<option value="SAEasternStandardTime">SA Eastern Standard Time</option>
	<option value="GreenlandStandardTime">Greenland Standard Time</option>
	<option value="MontevideoStandardTime">Montevideo Standard Time</option>

	<option value="UTC_02">UTC-02</option>
	<option value="Mid_AtlanticStandardTime">Mid-Atlantic Standard Time</option>
	<option value="AzoresStandardTime">Azores Standard Time</option>
	<option value="CapeVerdeStandardTime">Cape Verde Standard Time</option>
	<option value="MoroccoStandardTime">Morocco Standard Time</option>
	<option value="UTC">UTC</option>

	<option value="GMTStandardTime">GMT Standard Time</option>
	<option value="GreenwichStandardTime">Greenwich Standard Time</option>
	<option value="W__PERIOD__EuropeStandardTime">W. Europe Standard Time</option>
	<option value="CentralEuropeStandardTime">Central Europe Standard Time</option>
	<option value="RomanceStandardTime">Romance Standard Time</option>
	<option value="CentralEuropeanStandardTime">Central European Standard Time</option>

	<option value="W__PERIOD__CentralAfricaStandardTime">W. Central Africa Standard Time</option>
	<option value="NamibiaStandardTime">Namibia Standard Time</option>
	<option value="JordanStandardTime">Jordan Standard Time</option>
	<option value="GTBStandardTime">GTB Standard Time</option>
	<option value="MiddleEastStandardTime">Middle East Standard Time</option>
	<option value="EgyptStandardTime">Egypt Standard Time</option>

	<option value="SyriaStandardTime">Syria Standard Time</option>
	<option value="SouthAfricaStandardTime">South Africa Standard Time</option>
	<option value="FLEStandardTime">FLE Standard Time</option>
	<option value="IsraelStandardTime">Israel Standard Time</option>
	<option value="E__PERIOD__EuropeStandardTime">E. Europe Standard Time</option>
	<option value="ArabicStandardTime">Arabic Standard Time</option>

	<option value="ArabStandardTime">Arab Standard Time</option>
	<option value="RussianStandardTime">Russian Standard Time</option>
	<option value="E__PERIOD__AfricaStandardTime">E. Africa Standard Time</option>
	<option value="IranStandardTime">Iran Standard Time</option>
	<option value="ArabianStandardTime">Arabian Standard Time</option>
	<option value="AzerbaijanStandardTime">Azerbaijan Standard Time</option>

	<option value="MauritiusStandardTime">Mauritius Standard Time</option>
	<option value="GeorgianStandardTime">Georgian Standard Time</option>
	<option value="CaucasusStandardTime">Caucasus Standard Time</option>
	<option value="AfghanistanStandardTime">Afghanistan Standard Time</option>
	<option value="EkaterinburgStandardTime">Ekaterinburg Standard Time</option>
	<option value="PakistanStandardTime">Pakistan Standard Time</option>

	<option value="WestAsiaStandardTime">West Asia Standard Time</option>
	<option value="IndiaStandardTime">India Standard Time</option>
	<option value="SriLankaStandardTime">Sri Lanka Standard Time</option>
	<option value="NepalStandardTime">Nepal Standard Time</option>
	<option value="CentralAsiaStandardTime">Central Asia Standard Time</option>
	<option value="BangladeshStandardTime">Bangladesh Standard Time</option>

	<option value="N__PERIOD__CentralAsiaStandardTime">N. Central Asia Standard Time</option>
	<option value="MyanmarStandardTime">Myanmar Standard Time</option>
	<option value="SEAsiaStandardTime">SE Asia Standard Time</option>
	<option value="NorthAsiaStandardTime">North Asia Standard Time</option>
	<option value="ChinaStandardTime">China Standard Time</option>
	<option value="NorthAsiaEastStandardTime">North Asia East Standard Time</option>

	<option value="SingaporeStandardTime">Singapore Standard Time</option>
	<option value="W__PERIOD__AustraliaStandardTime">W. Australia Standard Time</option>
	<option value="TaipeiStandardTime">Taipei Standard Time</option>
	<option value="UlaanbaatarStandardTime">Ulaanbaatar Standard Time</option>
	<option value="TokyoStandardTime">Tokyo Standard Time</option>
	<option value="KoreaStandardTime">Korea Standard Time</option>

	<option value="YakutskStandardTime">Yakutsk Standard Time</option>
	<option value="Cen__PERIOD__AustraliaStandardTime">Cen. Australia Standard Time</option>
	<option value="AUSCentralStandardTime">AUS Central Standard Time</option>
	<option value="E__PERIOD__AustraliaStandardTime">E. Australia Standard Time</option>
	<option value="AUSEasternStandardTime">AUS Eastern Standard Time</option>
	<option value="WestPacificStandardTime">West Pacific Standard Time</option>

	<option value="TasmaniaStandardTime">Tasmania Standard Time</option>
	<option value="VladivostokStandardTime">Vladivostok Standard Time</option>
	<option value="MagadanStandardTime">Magadan Standard Time</option>
	<option value="CentralPacificStandardTime">Central Pacific Standard Time</option>
	<option value="NewZealandStandardTime">New Zealand Standard Time</option>
	<option value="UTC+12">UTC+12</option>

	<option value="FijiStandardTime">Fiji Standard Time</option>
	<option value="KamchatkaStandardTime">Kamchatka Standard Time</option>
	<option value="TongaStandardTime">Tonga Standard Time</option>

