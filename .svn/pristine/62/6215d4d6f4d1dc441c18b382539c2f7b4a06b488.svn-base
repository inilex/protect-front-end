<?xml version="1.0"?>
<doc>
    <assembly>
        <name>ITP.DeviceRx</name>
    </assembly>
    <members>
        <member name="T:ITP.DeviceRx.IAccountingService">
            <summary>
            Interface for an accounting service
            </summary>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.AirTimeUsed(System.Collections.Generic.IEnumerable{ITP.Common.Types.AirTime})">
            <summary>
            Called to report air time utilization
            </summary>
            <param name="records">Air time utilization records</param>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.CreateDevice(ITP.Common.Types.DeviceIdentification,System.String)">
            <summary>
            Creates a device for use in the system
            </summary>
            <param name="device">The device to create</param>
            <param name="serviceId">Network service ID</param>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.ActivateDevice(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Activates a device for use on the wireless network
            </summary>
            <param name="device">Device to activate</param>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.SuspendDevice(ITP.Common.Types.DeviceIdentification,System.Boolean)">
            <summary>
            Suspends operation of a device on the wireless network
            </summary>
            <param name="device">The device to suspend</param>
            <param name="checkFeatures">Whether or not features should be checked before suspending</param>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.DestroyDevice(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Destroys the ability of a device to communicate wirelessly
            </summary>
            <param name="device">The device to destroy</param>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetNetworkSpecificIDs(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Gets network specific identifiers for a device
            </summary>
            <param name="device">The device to retrieve IDs for</param>
            <returns>The network specific IDs</returns>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetDeviceIdentificationFromNetworkID(System.String,ITP.DeviceRx.NetworkID)">
            <summary>
            Retrieves device identification infomration from a network specific ID
            </summary>
            <param name="serviceID">The service a device utilizes</param>
            <param name="networkSpecificID">The network specific ID</param>
            <returns>Device ID</returns>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetNetworkServiceID(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Gets the network service ID for a device
            </summary>
            <param name="device">The device</param>
            <returns>The network service ID</returns>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetNetworkServiceConfiguration(System.String)">
            <summary>
            Gets the network service configuration for a network service
            </summary>
            <param name="NetworkServiceID">The network service's ID</param>
            <returns>Configuration for the network service</returns>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetDeviceServiceID(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Gets the device service for a device
            </summary>
            <param name="device">The device</param>
            <returns>The device service ID</returns>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetDeviceServiceConfiguration(System.String)">
            <summary>
            Gets the configuration for a device service
            </summary>
            <param name="DeviceServiceID">The device service's ID</param>
            <returns>Configuration for the device service</returns>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.SetDeviceStateInfo(ITP.Common.Types.DeviceIdentification,System.Byte[])">
            <summary>
            Stores device state information
            </summary>
            <param name="device">The device</param>
            <param name="stateInfo">The state information to store</param>
        </member>
        <member name="M:ITP.DeviceRx.IAccountingService.GetDeviceStateInfo(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Retrieves device state information
            </summary>
            <param name="device">The device to retrieve</param>
            <returns>Device state information</returns>
        </member>
        <member name="T:ITP.DeviceRx.INetworkService">
            <summary>
            Interface for a network service
            </summary>
        </member>
        <member name="M:ITP.DeviceRx.INetworkService.DeviceSuspended(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Notification that a device's wireless service was suspended
            </summary>
            <param name="device">The device which was suspended</param>
        </member>
        <member name="M:ITP.DeviceRx.INetworkService.DeviceActivated(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Notification that a device's wireless service was activated
            </summary>
            <param name="device">The device which was activated</param>
        </member>
        <member name="M:ITP.DeviceRx.INetworkService.CheckUpdates(ITP.Common.Types.DeviceIdentification)">
            <summary>
            Checks to see if a device needs updating
            </summary>
            <param name="deviceIdent">The device to check</param>
            <returns>Whether or not updates are necessary</returns>
        </member>
        <member name="M:ITP.DeviceRx.INetworkService.MessageReceived(ITP.Common.Types.DeviceIdentification,ITP.Common.Types.DeviceEvent,System.DateTime)">
            <summary>
            Called when a message is received from a device for the network service
            </summary>
            <param name="device">Which device sent the message</param>
            <param name="eventInformation">Infomration contained within the message</param>
            <param name="receivedTime">Timestamp of when the message was received</param>
        </member>
        <member name="M:ITP.DeviceRx.INetworkService.UpdateTransactionDisposition(System.Guid,ITP.Common.Types.TransactionDisposition,System.String)">
            <summary>
            Reports disposition of a transaction
            </summary>
            <param name="transactionId">Transaction ID</param>
            <param name="result">Result of the transaction</param>
            <param name="additionalInfo">Additional information about the transaction</param>
        </member>
        <member name="M:ITP.DeviceRx.INetworkService.DeviceVerificationReceived(ITP.Common.Types.DeviceIdentification,ITP.Common.Types.DeviceVerificationReport)">
            <summary>
            Reports device verification of configuration items
            </summary>
            <param name="device">The device which reported its configuration</param>
            <param name="report">Information about the configuration of the device</param>
        </member>
        <member name="T:ITP.DeviceRx.IPNetworkID">
            <summary>
            Contains information about a network assigned static IP address
            </summary>
        </member>
        <member name="T:ITP.DeviceRx.NetworkID">
            <summary>
            Base class for network specific unique ID
            </summary>
        </member>
        <member name="P:ITP.DeviceRx.NetworkID.IsPrimary">
            <summary>
            Whether or not the ID is the primary ID
            </summary>
        </member>
        <member name="P:ITP.DeviceRx.IPNetworkID.Address">
            <summary>
            IP address for the device
            </summary>
        </member>
        <member name="T:ITP.DeviceRx.PinNetworkID">
            <summary>
            Contains information about a network assigned PIN address
            </summary>
        </member>
        <member name="P:ITP.DeviceRx.PinNetworkID.Pin">
            <summary>
            The PIN for the device
            </summary>
        </member>
        <member name="T:ITP.DeviceRx.SerialNumberNetworkID">
            <summary>
            Contains the serial number of the device
            </summary>
        </member>
        <member name="P:ITP.DeviceRx.SerialNumberNetworkID.SerialNumber">
            <summary>
            The serial number of the device
            </summary>
        </member>
    </members>
</doc>
