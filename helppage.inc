<div class="popup-box" id="help">
	<h1>MobilePROTECT</h1><p>
	<h2>Vehicle Screen</h2>
	If you have more than one vehicle on your account, you will be shown a summary view of all those vehicles.<p>
	Click a car <img src='images/car_red.png' class='miniIconFloor' alt='Red car'/> to turn QuickFence&reg; on and off.<p>	
	The most recently reported locations, up to a maximum of 10,<br/>will be loaded for each vehicle.  Icons show what type of event caused a location to be recorded:<p>
	<ul  style="text-align:left; margin-left:55px;">
	<li><img src='images/icon_locations.png' class='miniIcon' alt='Location received icon'/> location requested or reported<p></li>
	<li><img src='images/icon_alertIgnOn.png' class='miniIcon' alt='Ignition on alert icon'/> ignition turned on<p></li>
	<li><img src='images/icon_alertIgnOff.png' class='miniIcon' alt='Ignition on alert icon'/> ignition turned off<p></li>
	<li><img src='images/icon_alertQuickFence.png' class='miniIcon' alt='QuickFence icon'/> QuickFence&reg; violation<p></li>
	<li><img src='images/icon_alertSpeed.png' class='miniIcon' alt='Speed alert icon'/> speed threshold exceeded<p></li>
	<li><img src='images/icon_alertGeoFence.png' class='miniIcon' alt='GeoFence alert icon'/> geofence boundary crossed<p></li>
	<li><img src='images/icon_alertBattery.png' class='miniIcon' alt='Battery alert icon'/> vehicle battery level low<p></li>
	<li><img src='images/icon_alertPowerOff.png' class='miniIcon' alt='Power off icon'/> GPS unit power disconnected<p></li>
	</ul>
	Use <img src='images/page_previous.png' class='miniIcon' alt='Previous locations icon'/> and <img src='images/page_next.png' class='miniIcon' alt='Next locations icon'/> to see the loaded locations.<p>
	Use <img src='images/icon_locations.png' class='miniIcon' alt='Location icon'/> to see the Map Screen for that vehicle.<p>
	<h2>Map Screen</h2>
	This screen plots the selected location on a map.  You may interact with the map to zoom and pan as you would normally.<p>
	If you have only one vehicle on your account, you will be taken directly to the Map Screen after login.<p>
	Use <img src='images/page_previous.png' class='miniIcon' alt='Previous locations icon'/> and <img src='images/page_next.png' class='miniIcon' alt='Next locations icon'/> to plot the loaded locations.<p>
	<div style='position:relative; display:inline'><img src='images/icon_locations.png' class='miniIcon' alt='Location icon'/><img src='images/icon_refresh.png' style='width:20px; position:absolute; left:6px; top:-10px' alt='Refresh location icon'/></div> requests an updated location from the vehicle.<br/>
	NOTE: This may take some time to complete, but you can continue using the application while waiting.  If a response is not received after a period of time, you will be notified to try later.<p>
	<img src='images/btn_back.png' class='miniIcon' alt='Back button'/> will be visible if you have more than one vehicle on your account, and returns to the Vehicle Screen.<p>
	<h2>Help Screen</h2>
	Use <img src='images/btn_help.png' class='miniIcon' alt='Help button'/> to access this screen, and <img src='images/btn_back.png' class='miniIcon' alt='Back button'/> to close it.
</div>