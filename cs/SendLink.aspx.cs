using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LenderLink;
using System.Net.Mail;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Configuration;
using VehicleManagement.Data;

public partial class SendLink : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
//		document.getElementById('btnHome.visibility = "hidden";
    }

    protected void btnSubmit_Click(Object sender, EventArgs e)
    {
        //verify their email address exists.
        using (var db = new VehicleManagementDataContext())
        {
            String email = txtEmail.Text.Trim();

			MailMessage theMessage = new MailMessage("info@mysky-link.com", email);
            theMessage.Subject = "SkyLink account updated";
            theMessage.Priority = MailPriority.Normal;
            
            theMessage.Body = Environment.NewLine;
            theMessage.Body += String.Format("Please use the following direct link to access PROTECT.") + Environment.NewLine + Environment.NewLine;
            theMessage.Body += "https://www.MySky-Link.com/etd" + Environment.NewLine + Environment.NewLine + Environment.NewLine;
            theMessage.Body += String.Format("If you did not request this link, you may ignore this email.") + Environment.NewLine + Environment.NewLine;

            theMessage.Body += Environment.NewLine + Environment.NewLine;
            theMessage.Body += "Sincerely your SkyLink support team";

            string emailServer = ConfigurationManager.AppSettings["MailServer"];

            SmtpClient smtp = new SmtpClient(emailServer);

            smtp.Send(theMessage);

            errorContentArea.InnerHtml = "A new password has been sent to your email address.";
            txtEmail.Enabled = false;
            txtEmail.Text = "";
            btnSubmit.Enabled = false;
        }

    }
}
