using System;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;


using System.Data;
using System.Security.Cryptography;
using System.IO;

using LenderLink;
using SkyLink;
using VehicleManagement.Data;

/// <summary>
/// 1. Allows a support user to login to the system as an alternate user
/// </summary>
public partial class CurrentSubscriberLogin : System.Web.UI.Page
{
    #region Methods (1)

    protected void SetError(String message)
    {
        lblError.Text = message;
    }

    #endregion

    #region Event Handlers (3)

	/// <summary>
	/// v1.01 removed encryption
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {   
        if (!Page.IsPostBack)
        {
        	if (SkylinkUtil.AltUserID < 1) {
	        	// If this is not a support user (i.e. the session's AltUserID is not set), continue to standard landing page or...
        		if (SkylinkUtil.UserID > 0) {
					Response.Redirect("index.html");
				} else {				
    	    	// ...if there is no user ID at all in this session, to the login page.
					Response.Redirect("main.aspx");
				}
        	} else {
        		// Otherwise, continue to allow altUser login 
	    		// Start off by defaulting to current (support) user           
        		System.Web.HttpCookie tmpEmail = Request.Cookies["altUserName"];
    			if (tmpEmail != null) {
    				txtAltEmail.Text = Server.HtmlEncode(tmpEmail.Value);
    			}
    		}
        }
    }


    /// <summary>
    /// If the email address corresponds to a genuine user, logs the user in as that user.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void robLogin_Click(Object sender, EventArgs e)
    {
        String altEmail = txtAltEmail.Text.Trim();

        using (var db = new VehicleManagementDataContext())
        {       
            ConsumerUser slu = db.ConsumerUsers.FirstOrDefault(x => x.PrimaryEmail == altEmail);

            // Only allow login as a consumer.
            if (slu == null)
            {           
                SetError("Login as consumer "+altEmail+ " failed.");
            	Response.Cookies["altUserName"].Expires = DateTime.Now.AddDays(-1);
                return;
            }
            
            // Create cookie for alternate user email
            Response.Cookies["altUserName"].Value = altEmail;
			Response.Cookies["altUserName"].Expires = DateTime.Now.AddDays(365);
            
            // Set alternate user ID
            SkylinkUtil.AltUserID = slu.ID;

			Response.Redirect("index.html");        		
        }
    }

    #endregion
}
