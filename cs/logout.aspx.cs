using System;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;


using System.Data;
using System.Security.Cryptography;
using System.IO;

using LenderLink;
using SkyLink;
using VehicleManagement.Data;

/// <summary>
/// 1. Allows a support user to login to the system as an alternate user
/// </summary>
public partial class CurrentSubscriberLogin : System.Web.UI.Page
{
    #region Event Handlers (3)

	/// <summary>
	/// v1.01 removed encryption
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
    protected void Page_LoadComplete(object sender, EventArgs e)
    {   
        if (!Page.IsPostBack)
        {
        	// For non-support users, clear the password & "remember me" flag at logout.
			if (SkylinkUtil.AltUserID == 0) {
            	Response.Cookies["remember"].Expires = DateTime.Now.AddDays(-1);
            	Response.Cookies["userKey"].Expires = DateTime.Now.AddDays(-1);
	            SkylinkUtil.UserID = 0;
            }
            Response.Cookies["altUserName"].Expires = DateTime.Now.AddDays(-1);
            SkylinkUtil.AltUserID = SkylinkUtil.UserID;
            Response.Redirect("altLogin.aspx");        		
        }
    }
    
    #endregion
}