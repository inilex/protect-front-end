<script type="text/javascript" src="js/ctrlPanel.js"></script>
<link rel="stylesheet" type="text/css" href="css/ctrlPanel.css" />

<script type="text/javascript" src="js/cart.js"></script>

<div id="ctrlPanel" class="ctrlPanel">
	<div id="vSettings" class="subPanel">
	<img src="images/btn_close.png" class="closeBtnControl" alt="Close panel" onclick="hideCtrlPanel();"/>
		<div class="subSubPanel">
			<!--#include file="ctrlPanelMenu.inc"-->
		</div>
		<div id="ctrlPromo1"></div>
	</div>
	
	<!--#include file="inc/ctrlPanelBody.inc"-->
</div>


	
<script>
	$(function() {
//		$( "#geoOnOff0" ).buttonset();
//		$( "#geoOnOff1" ).buttonset();
//		$( "#geoOnOff2" ).buttonset();
		
//		$( "#geoTrigger0" ).buttonset();


		$( "#maxSpeedSlider" ).slider({
			range: "min",
			min: minSpeedThreshold,
			max: maxSpeedThreshold,
			step: speedThresholdStep,
			slide: function( event, ui ) {
				$( "#maxSpeed" ).val(ui.value );
			}
		});

		$( "#maxIdleSlider" ).slider({
			range: "min",
			min: minIdleTime,
			max: maxIdleTime,
			step: idleTimeStep,
			slide: function( event, ui ) {
				$( "#maxIdle" ).val(ui.value );
			}
		});

		$( "#geoRadiusSlider0" ).slider({
			range: "min",
			min: minRadius,
			max: maxRadius,
			step: radiusStep,
			slide: function( event, ui ) {
				$( "#geoRadius0" ).val(ui.value );
				vehicles.getCurrentVehicle().geofences.list[0].setRadius(ui.value);
			}
		});

		$( "#geoRadiusSlider1" ).slider({
			range: "min",
			min: minRadius,
			max: maxRadius,
			step: radiusStep,
			slide: function( event, ui ) {
				$( "#geoRadius1" ).val(ui.value );
				vehicles.getCurrentVehicle().geofences.list[1].setRadius(ui.value);
			}
		});


		$( "#geoRadiusSlider2" ).slider({
			range: "min",
			min: minRadius,
			max: maxRadius,
			step: radiusStep,
			slide: function( event, ui ) {
				$( "#geoRadius2" ).val(ui.value );
				vehicles.getCurrentVehicle().geofences.list[2].setRadius(ui.value);
			}
		});
	});

		
</script>